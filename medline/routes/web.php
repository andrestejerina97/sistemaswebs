<?php

use App\Http\Controllers\MedicalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/MedicalHome','MedicalController@index')->name('MedicalHome');
Route::get('/Profile','MedicalController@ProfileHome')->name('MedicalProfile');
Route::get('/Hola','MedicalController@ProfileHome')->name('Hola');
Route::get('/Recipe','MedicalController@RecipeHome')->name('MedicalRecipe');
Route::post('/UpdateMedical','MedicalController@Update')->name('UpdateMedical');
