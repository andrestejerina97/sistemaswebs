<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Safario Travel - Home</title>
    <link rel="icon" href="img/Fevicon.png" type="image/png">

    <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/flat-icon/font/flaticon.css">
    <link rel="stylesheet" href="vendors/nice-select/nice-select.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="css/style.css">
</head>

<body class="bg-shape">
    @include('public.header.Medical')

 <!--Current Recipes -->

    <div class="container">
        <div class="hero-banner ">
            <div class=" justify-content-center col-lg-6 col-xl-5  mb-5 mb-lg-0">
                <table class="table table-striped  table-responsive">
                    <thead class="thead-inverse">
                        <tr>
                            <th>aaa</th>
                            <th>aa</th>
                            <th>aa</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">asd</td>
                                <td>asd</td>
                                <td>asda</td>
                            </tr>
                            <tr>
                                <td scope="row">asd</td>
                                <td>asd</td>
                                <td>asd</td>
                            </tr>
                        </tbody>
                </table>



            </div>
            
     </div>

</div>
 <!--End Current Recipes -->

        @include('public.footer.Medical')





        <script src="vendors/jquery/jquery-3.2.1.min.js"></script>
        <script src="vendors/bootstrap/bootstrap.bundle.min.js"></script>
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="vendors/nice-select/jquery.nice-select.min.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/mail-script.js"></script>
        <script src="js/skrollr.min.js"></script>
        <script src="js/main.js"></script>
</body>

</html>