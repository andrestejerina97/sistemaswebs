<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald|Roboto:300,400,500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/estilos.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SIFD</title>
</head>
<body>
    @include('sidebar.navbar')
    

        <div class="container-fluid">
            <div class="row">
                @include('sidebar.sidebar')

                <main class="main col">
                    <div class="row mt-3">
                        <div class="col-12 col-md-3 mt-3 ml-3">
                            <div class="card-group">               
                                <div class="card">
                                    <img src="img/pruebasdif.png" style="height : 300px; widht: 300px;" class="card-image-top img-fluid" alt="">
                                    <div class="card-body">
                                        <h4 class="card-title">Resolucion por maternidad</h4>
                                        <p class="card-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eos, eligendi.</p>
                                        <button class="btn btn-primary btn-block">Descargar</button>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted"><em>Resoluciones internas.</em></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3 mt-3">
                            <div class="card-group">               
                                <div class="card">
                                    <img src="http://lorempixel.com/300/300/" class="card-image-top img-fluid" alt="">
                                    <div class="card-body">
                                        <h4 class="card-title">Resolucion por licencia</h4>
                                        <p class="card-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eos, eligendi.</p>
                                        <button class="btn btn-primary btn-block">Descargar</button>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted"><em>Resoluciones internas.</em></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
