<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald|Roboto:300,400,500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/estilos.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SIFD</title>
</head>
<body>
    @include('sidebar.navbar')
    

        <div class="container-fluid">
            <div class="row">
                @include('sidebar.sidebar')

                <main class="main col">
                </main>
            </div>
        </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
