<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald|Roboto:300,400,500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/estilos.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SIFD</title>
</head>
<body>
    @include('sidebar.navbar')
    

        <div class="container-fluid">
            <div class="row">
                @include('sidebar.sidebar')

                <main class="main col">
                    <div class="col mt-3">
                        <h2>Subir Resolucion Interna</h2>
                        <hr>
        
                        <form method="POST" action="{{route('save')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                            <div class="form-group row">                                                                
                                <div class="col-12 col-md-3">
                                    <label for="resolution">Nro resolución</label>
                                    <input class="form-control" type="text" name="resolution_number" id="resolution_number" placeholder="Nro resolucion">
                                </div>
                                /
                                <div class="col-12 col-md-3">
                                    <label for="year">Año</label>
                                    <input class="form-control" type="text" name="year" id="year" placeholder="Año">
                                </div>                                
                            </div>
                            <div class="form-group row">
                                <div class="col-12 col-md-6">
                                    <label for="asunto">Asunto de resolucion</label>
                                    <select name="subject" id="subject" class="custom-select mb-3"> 
                                        @foreach ($subjects as $subject)
                                            <option value="{!!$subject->id!!}">{!!$subject->subject_name!!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-check row mb-3 pl-0">
                                <div class="col-12 col-md-6">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="flag_urgent" id="flag_urgent" value="1"> Urgente <em>(Usar con precaución)</em>
                                    </label>
                                </div>
                            </div>

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  
                            <div class="form-group row">                               
                                <div class="col-md-6">
                                    <label class="control-label">Nuevo Archivo</label>
                                    <input type="file" class="form-control" name="file" >
                                </div>
                            </div>
                       
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4 pl-0">
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </main>
            </div>
        </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
