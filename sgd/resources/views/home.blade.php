<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald|Roboto:300,400,500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/estilos.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SIFD</title>
</head>
<body>
    @include('sidebar.navbar')
    

        <div class="container-fluid">
            <div class="row">
                @include('sidebar.sidebar')

                <main class="main col">
                    <div class="row mt-3">
                        <div class="col">
                            <ul class="nav nav-tabs">
                                <li class="nav-av-item">
                                    <a href="#tab1" class="nav-link active" data-toggle="tab" aria-selected="true">Resoluciones internas<span class="badge badge-dark ml-1">{!!$notificationsRI!!}</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#tab2" class="nav-link" data-toggle="tab" aria-selected="false">Resoluciones departamentales<span class="badge badge-dark ml-1">{!!$notificationsRD!!}</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#tab3" class="nav-link" data-toggle="tab">Urgentes<span class="badge badge-warning ml-1">{!!$notificationsUrgent!!}</span></a>
                                </li>

                            </ul>

                            <div class="tab-content ml-2 mt-2">
                                <div class="tab-pane active" id="tab1" role="tabpanel">
                                    @if ($resolutions->isEmpty())
                                        <div>No hay Resoluciones</div>
                                    @else
                                    <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Remitente</th>
                                                <th>Nombre del archivo</th>
                                                <th>Asunto de resolucion</th>
                                                <th>Año</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach($resolutions as $resolution)
                                                <tr class="tabla1">
                                                    <td><strong>{!! $resolution->name_user !!} {!! $resolution->surname !!}</strong></td>
                                                    <td> <a href="" class="text-dark"><strong>{!! $resolution->name !!}</strong></a> </td>
                                                    <td>{!! $resolution->subject_name !!}</td>
                                                    <td>{!! $resolution->year !!}</td>
                                                    <td><a href="{{route('read',$resolution->id)}}" target="_blank" class="btn btn-primary">Leer</a></td>
                                                    <td>
                                                        
                                                        @if (($role_id)==3 or ($role_id)==4)
                                                        <form action="{{route('signerror')}}" method="post">
                                                            @csrf
                                                        <input type="hidden" name="name" value="{{$resolution->name}}">
                                                        <input type="hidden" name="subject" value="{{$resolution->subject_name}}" >
                                                        <input type="hidden" name="idir_error" value="{{$resolution->id}}">
                                                        <input type="submit" class="btn btn-danger" value="Reenviar">
                                                        @else
                                                        
                                                        @endif
                                                        @if (($role_id)==2)
                                                        <form action="{{route('sign')}}" method="post">
                                                            @csrf
                                                        <input type="hidden" name="name" value="{{$resolution->name}}">
                                                        <input type="hidden" name="subject" value="{{$resolution->subject_name}}" >
                                                        <input type="hidden" name="idir" value="{{$resolution->id}}">
                                                        <input type="submit" class="btn btn-primary" value="Firmar">
                                                        @else
                                                        
                                                        @endif
                                                        </form>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                                <div class="tab-pane" id="tab2" role="tabpanel">
                                    <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Remitente</th>
                                                <th>Nombre del archivo</th>
                                                <th>Asunto de resolucion</th>
                                                <th>Año</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($resolutionsDR as $resolutionDR)
                                                <tr class="tabla1">
                                                    <td><strong>{!! $resolutionDR->name_user !!} {!! $resolutionDR->surname !!}</strong></td>
                                                    <td> <a href="" class="text-dark"><strong>{!! $resolutionDR->name !!}</strong></a> </td>
                                                    <td>{!! $resolutionDR->subject_name !!}</td>
                                                    <td>{!! $resolutionDR->year !!}</td>
                                                    <td><a href="{{route('readDr',$resolutionDR->id)}}" target="_blank" class="btn btn-primary">Leer</a></td>
                                                    <td>
                                                        
                                                        @if (($role_id)==3 or ($role_id)==4)
                                                        <form action="{{route('signDrerror')}}" method="post">
                                                            @csrf
                                                        <input type="hidden" name="name" value="{{$resolutionDR->name}}">
                                                        <input type="hidden" name="subject" value="{{$resolutionDR->subject_name}}" >
                                                        <input type="hidden" name="iddr_error" value="{{$resolutionDR->id}}">
                                                        <input type="submit" class="btn btn-danger" value="Incorrecto">
                                                        @else
                                                        
                                                        @endif
                                                        @if (($role_id)==2)
                                                        <form action="{{route('signDr')}}" method="post">
                                                            @csrf
                                                        <input type="hidden" name="name" value="{{$resolutionDR->name}}">
                                                        <input type="hidden" name="subject" value="{{$resolutionDR->subject_name}}" >
                                                        <input type="hidden" name="iddr" value="{{$resolutionDR->id}}">
                                                        <input type="submit" class="btn btn-primary" value="Firmar">
                                                        @else
                                                        
                                                        @endif
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab3" role="tabpanel">
                                    <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Remitente</th>
                                                <th>Nombre del archivo</th>
                                                <th>Tipo de resolucion</th>
                                                <th>Titulo de resolucion</th>
                                                <th>Año</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($resolutionsU as $resolutionU)
                                                <tr class="tabla1">
                                                    <td><strong>{!! $resolutionU->name_user !!} {!! $resolutionU->surname !!}</strong></td>
                                                    <td> <a href="" class="text-dark"><strong>{!! $resolutionU->name !!}</strong></a> </td>
                                                    <td>{!!$resolutionU->type!!}</td>
                                                    <td>{!! $resolutionU->subject_name !!}</td>
                                                    <td>{!! $resolutionU->year !!}</td>
                                                    <td><a href="{{route('read',$resolutionU->id)}}" target="_blank" class="btn btn-primary">Leer</a></td>
                                                    <td>
                                                        @if (($role_id)==3 or ($role_id)==4)
                                                        <form action="{{route('signerror')}}" method="post">
                                                            @csrf
                                                        <input type="hidden" name="name" value="{{$resolutionU->name}}">
                                                        <input type="hidden" name="subject" value="{{$resolutionU->subject_name}}" >
                                                        <input type="hidden" name="idir_error" value="{{$resolutionU->id}}">
                                                        <input type="submit" class="btn btn-danger" value="Reenviar">
                                                        @else
                                                        
                                                        @endif
                                                        @if (($role_id)==2)
                                                        <form action="{{route('sign')}}" method="post">
                                                            @csrf
                                                        <input type="hidden" name="name" value="{{$resolutionU->name}}">
                                                        <input type="hidden" name="subject" value="{{$resolutionU->subject_name}}" >
                                                        <input type="hidden" name="idir" value="{{$resolutionU->id}}">
                                                        <input type="submit" class="btn btn-primary" value="Firmar">
                                                        @else
                                                        
                                                        @endif
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @foreach($resolutionsDRU as $resolutionDRU)
                                            <tr class="tabla1">
                                                <td><strong>{!! $resolutionDRU->name_user !!} {!! $resolutionDRU->surname !!}</strong></td>
                                                <td> <a href="" class="text-dark"><strong>{!! $resolutionDRU->name !!}</strong></a> </td>
                                                <td>{!!$resolutionDRU->type!!}</td>
                                                <td>{!! $resolutionDRU->subject_name !!}</td>
                                                <td>{!! $resolutionDRU->year !!}</td>
                                                <td><a href="{{route('readDr',$resolutionDRU->id)}}" target="_blank" class="btn btn-primary">Leer</a></td>
                                                <td>
                                                    @if (($role_id)==3 or ($role_id)==4)
                                                    <form action="{{route('signDrerror')}}" method="post">
                                                        @csrf
                                                    <input type="hidden" name="name" value="{{$resolutionDRU->name}}">
                                                    <input type="hidden" name="subject" value="{{$resolutionDRU->subject_name}}" >
                                                    <input type="hidden" name="iddr_error" value="{{$resolutionDRU->id}}">
                                                    <input type="submit" class="btn btn-danger" value="Incorrecto">
                                                    @else
                                                    
                                                    @endif
                                                    @if (($role_id)==2)
                                                    <form action="{{route('signDr')}}" method="post">
                                                        @csrf
                                                    <input type="hidden" name="name" value="{{$resolutionDRU->name}}">
                                                    <input type="hidden" name="subject" value="{{$resolutionDRU->subject_name}}" >
                                                    <input type="hidden" name="iddr" value="{{$resolutionDRU->id}}">
                                                    <input type="submit" class="btn btn-primary" value="Firmar">
                                                    @else
                                                    
                                                    @endif
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>





 
