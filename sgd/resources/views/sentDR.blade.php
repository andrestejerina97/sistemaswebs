<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald|Roboto:300,400,500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/estilos.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SIFD</title>
</head>
<body>
    @include('sidebar.navbar')
    

        <div class="container-fluid">
            <div class="row">
                @include('sidebar.sidebar')

                <main class="main col">
                    <div class="container col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading mt-3">
                                <h2>Resoluciones</h2>
                            </div>
                            @if ($resolutions->isEmpty())
                                <div>No hay Resoluciones</div>
                            @else
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Filename</th>
                                            <th>Subject</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($resolutions as $resolution)
                                            <tr>
                                                <td>{!! $resolution->id !!}</td>
                                                <td>{!! $resolution->filename !!}</td>
                                                <td>{!! $resolution->subject ? 'Pendiente' : 'Respondido' !!}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </main>
            </div>
        </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
