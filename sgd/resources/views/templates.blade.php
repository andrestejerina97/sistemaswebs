<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald|Roboto:300,400,500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/estilos.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SIFD</title>
</head>
<body>
    @include('sidebar.navbar')
    

        <div class="container-fluid">
            <div class="row">
                @include('sidebar.sidebar')

                <main class="main col">
                    <div class="row my-3">
                        <div class="col">
                            <div class="card text-white" style="background: #020611f1;">
                                <div class="card-body text-center">
                                    <h3 class="card-title">Plantillas de Resoluciones Internas.</h3>
                                    <p class="card-text"> <em>Aqui están todos los modelos de plantillas.</em> </p>
                                    <a href="{{route('viewtemplate')}}" class="btn text-white" style="background: #0a1744f1;">Ver</a>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card text-white" style="background: #020611f1;">
                                <div class="card-body text-center">
                                    <h3 class="card-title">Plantilla de Resoluciones Departamentales.</h3>
                                    <p class="card-text"> <em>Aqui están todos los modelos de plantillas.</em> </p>
                                    <a href="{{route('uploadDR')}}" class="btn text-white" style="background: #0a1744f1;">Ver</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
