<div class="barra-lateral col-12 col-sm-auto">
    <nav class="menu d-flex d-sm-block justify-content-center flex-wrap">
        <a href="{{route('select')}}" class=""><i class="icon-pencil-neg"></i><span>Enviar resoluciones</span></a>
        <a href="{{route('template')}}"><i class="icon-inbox"></i><span>Plantillas</span></a>
        <a href="#"><i class="icon-inbox"></i><span>No firmados</span></a>
        <a href="#"><i class="icon-ok"></i><span>Firmados</span></a>
        <a href="{{route('sent')}}"><i class="icon-forward"></i><span>Enviados</span></a>
        <a href="#"><i class="icon-doc-inv"></i><span>Borradores</span></a>
        <a href="#"><i class="icon-help"></i><span>Ayuda</span></a>
        <a href="{{route('viewtemplate')}}"><i class="icon-help"></i><span>templates</span></a>
        <a href="{{route('logout')}}" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();"><i class="icon-logout"></i><span>Salir</span></a>
    </nav>
</div>
