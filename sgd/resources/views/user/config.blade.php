<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald|Roboto:300,400,500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/estilos.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SIFD</title>
</head>
<body>
    @include('sidebar.navbar')
    

        <div class="container-fluid">
            <div class="row">

                <main class="main col">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                
                            @if(session('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                            @endif
                
                            <div class="card mt-3">
                                <div class="card-header text-white" style="background: #020611f1;">Configuracion de la Cuenta</div>
                
                                <div class="card-body">
                                    <form method="POST" action="{{route('user.update')}}" aria-label="configuracion de la cuenta">
                                        @csrf
                
                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                
                                            <div class="col-md-6">
                                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ Auth::user()->name }}" required autofocus>
                
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                
                                        <div class="form-group row">
                                            <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('Surname') }}</label>
                
                                            <div class="col-md-6">
                                                <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ Auth::user()->surname }}" required autofocus>
                
                                                @if ($errors->has('surname'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('surname') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                
                                        <div class="form-group row">
                                            <label for="dni" class="col-md-4 col-form-label text-md-right">{{ __('DNI') }}</label>
                
                                            <div class="col-md-6">
                                                <input id="dni" type="text" class="form-control{{ $errors->has('dni') ? ' is-invalid' : '' }}" name="dni" value="{{ Auth::user()->dni }}" required autofocus>
                
                                                @if ($errors->has('dni'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('dni') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                
                
                                        <div class="form-group row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                
                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ Auth::user()->email }}" required>
                
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                
                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary text-white" style="background: #020611f1;">
                                                    Guardar Cambios
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>


<div class="container">
    
</div>
