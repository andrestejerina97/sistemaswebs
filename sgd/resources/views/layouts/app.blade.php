<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SIFD</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald|Roboto:300,400,500&display=swap"
        rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/estilos.css">
</head>

<body>
    <header class="cabecera row text-white">
        <div class="columna col d-inline-flex flex-row">
            <!--<nav class="menu d-flex d-sm-block justify-content-start flex-wrap pl-2">
                <div class="logo">
                    <img src="img/logo.png" alt="" style="width: 300px; height: 100px; padding: 0;">
                </div> 
            </nav>-->
            <h2>
                <i class="icon-folder-open"></i><span>SDIF</span>
            </h2>
            <div class="col align-self-center">
                <!--<form action="" class="form-inline my-2 my-lg-0">-->
                <div class="input-group" style="width: 500px; height: 37px;">
                    <input type="text" class="form-control" style="height: 37px;" placeholder="Buscar" aria-label="Buscar">
                    <div class="input-group-append" style="height: 37px;">
                        <button class="btn btn-secondary my-sm-0" type="submit"><i class="icon-search"></i></button>
                    </div>
                </div>
                <!--</form>-->
            </div>
        </div>

        <div class="links  cold-flex flex-row-reverse">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="">
                                    Perfil
                                </a>
                                <a class="dropdown-item" href="{{ route('config') }}">
                                    Configuracion
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </nav>
        </div>
    </header>


    <main class="">
        @yield('content')
    </main>
    </div>
</body>

</html>
