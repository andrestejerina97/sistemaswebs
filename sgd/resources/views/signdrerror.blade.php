<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald|Roboto:300,400,500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/estilos.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SIFD</title>
</head>
<body>
    @include('sidebar.navbar')
    

        <div class="container-fluid">
            <div class="row">
                @include('sidebar.sidebar')

                <main class="main col">
                    <div class="col mt-3">
                        <h2>Reenviar resolucion interna erronea</h2>
                        <hr>
                
                        <form method="POST" action="{{route('savesigndrerror')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                            <div class="form-group row">                                                                
                                <div class="col-12 col-md-6">            
                                    <label for="year"><h4>Datos de la resolución</h4></label>
                                </div>                                
                            </div>
                
                            <div class="card mb-3" style="width: 18rem;">
                                <div class="card-body">
                                <P> <strong>Nombre: {{$filename}}</strong> </P>
                                <P>Tipo: {{$subject}}</P>                               
                                </div>
                              </div>

                            <input type="hidden" name="iddr_error" value="{{$iddr_error}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  
                            <div class="form-group row">                               
                                <div class="col-md-6">
                                    <label class="control-label">Seleccione su archivo:</label>
                                    <input type="file" class="form-control" name="file" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 col-md-6">
                                    <label for="asunto">¿Que tipo de error presenta la resolución?</label>
                                    <select name="error" id="error" class="custom-select">                                         
                                        @foreach ($errors as $error)
                                            <option value="{!!$error->id!!}">{!!$error->error_name!!}</option>    
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 col-md-6">
                                    <label for="asunto">¿A quien quieres enviar este documento?</label>
                                    <select name="addresse" id="addresse" class="custom-select mb-3"> 
                                        
                                        @foreach ($users as $user)
                                        @if($user->id==1 or $user->id == Auth::user()->id)
                                       @else
                                            <option value="{!!$user->id!!}">{!!$user->name.' '.$user->surname!!}</option>
                                        
                                            @endif
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-check row mb-3 pl-0">
                                <div class="col-12 col-md-6">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="flag_urgent" id="flag_urgent" value="1"> Urgente <em>(Usar con precaución)</em>
                                    </label>
                                </div>
                            </div>
                       
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4 pl-0">
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </main>
            </div>
        </div>

    <script src="../js/jquery-3.4.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</body>
</html>
