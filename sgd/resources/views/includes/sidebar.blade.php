<div class="container-fluid">
    <div class="row">
        <div class="barra-lateral col-12 col-sm-auto">
            <nav class="menu d-flex d-sm-block justify-content-center flex-wrap">
                <a href="#"><i class="icon-pencil-neg"></i><span>Redactar nuevo</span></a>
                <a href="#" class=""><i class="icon-inbox"></i><span>No firmados</span></a>
                <a href="#"><i class="icon-ok"></i><span>Firmados</span></a>
                <a href="#"><i class="icon-forward"></i><span>Enviados</span></a>
                <a href="#"><i class="icon-doc-inv"></i><span>Borradores</span></a>
                <a href="#"><i class="icon-upload"></i><span>Subir Archivos</span></a>
                <a href="#"><i class="icon-help"></i><span>Ayuda</span></a>
                <a href="#"><i class="icon-logout"></i><span>Salir</span></a>
            </nav>
        </div>
    </div>
</div>
