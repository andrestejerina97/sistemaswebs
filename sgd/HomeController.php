<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\InternalResolution as InternalResolution;
use App\DepartamentalResolution as DepartamentalResolution;
use App\Subject;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $request->user()->authorizeRoles(['user', 'admin', 'Decano', 'Secretario Academico','Secretario Administrativo']);

        $resolutions = InternalResolution::getResolutionUnsigned(Auth::user()->id);
        
        $resolutionsDR = DepartamentalResolution::getResolutionUnsigned( Auth::user()->id);
        
        $notificationsRI = InternalResolution::getNotifications(Auth::user()->id);

        $notificationsRD = DepartamentalResolution::getNotifications(Auth::user()->id);
        
        $resolutionsU = InternalResolution::getResolutionUrgent(Auth::user()->id);

        $resolutionsDRU = DepartamentalResolution::getResolutionUrgent(Auth::user()->id);

        $notificationsU = InternalResolution::getNotificationsUrgent(Auth::user()->id);

        $notificationsRDU = DepartamentalResolution::getNotificationsUrgent(Auth::user()->id);

        $notificationsUrgent= $notificationsRDU + $notificationsU ;

        return view('home')
                ->with('resolutions',$resolutions)
                ->with('resolutionsDR',$resolutionsDR)
                ->with('resolutionsU',$resolutionsU)
                ->with('resolutionsDRU',$resolutionsDRU)
                ->with('notificationsRI',$notificationsRI)
                ->with('notificationsRD',$notificationsRD)
                ->with('notificationsUrgent',$notificationsUrgent);
        
    }

    public function hola(Request $request){
        //THIS IS THE PAPOTA
        $request->user()->authorizeRoles(['Decano', 'Secretario Academico']);  
        return view('hola');
    }

    public function upload(){
        $subjects = Subject::all();
        $users=User::all();
        return view('upload',compact('subjects','users'));
    }

    public function uploadDR(){
        $subjects = Subject::all();
        $users=User::all();
        return view('uploadDR',compact('subjects','users'));
    }

    public function write(){
        return view('write');
    }

    public function template(){
        return view('templates');
    }

    public function templateslist(){
        return view('templateslist');
    }

    /*
    public function uploadsign($id){
        $ir = InternalResolution::find($id);
        return view('uploadsign')->with('internalresolution',$ir);

    }
    */
}
