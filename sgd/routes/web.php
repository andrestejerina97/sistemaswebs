<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/hola', 'HomeController@hola')->name('hola');
Route::get('/configuracion', 'UserController@config')->name('config');
Route::post('/user/update', 'UserController@update')->name('user.update');
Route::get('/formRi','FormController@formRi')->name('formRi');
Route::get('/formRd','FormController@formRd')->name('formRd');
Route::post('/save', 'StorageController@save')->name('save');
Route::post('/saveDR', 'StorageController@saveDR')->name('saveDR');
Route::get('/selectResolution','FormController@selectResolution')->name('select');
Route::get('/template','TemplateController@template')->name('template'); //intermedio para elegir las plantillas
Route::get('/templateslist','TemplateController@templateslist')->name('templateslist'); //vista que no sirve para nada
Route::get('/sent','ToolController@sent')->name('sent');
Route::get('/template1','TemplateController@index')->name('viewtemplate');
Route::get('/temupload','TemplateController@create')->name('formtemplate');
Route::post('/temupload','TemplateController@store')->name('uploadtemplate');
Route::get('/temdownload/{id}','TemplateController@show')->name('downloadtemplate');
Route::get('/read/{id}','HomeController@buttonRead')->name('read');
Route::get('/readDr/{id}','HomeController@buttonReadDr')->name('readDr');
Route::post('/signir','HomeController@buttonSign')->name('sign');
Route::post('/signdr','HomeController@buttonSignDr')->name('signDr');
Route::post('/signirerror','HomeController@buttonSignError')->name('signerror');
Route::post('/signdrerror','HomeController@buttonSignDrError')->name('signDrerror');
Route::post('/savesignir','StorageController@savesignir')->name('savesignir');
Route::post('/savesigndr','StorageController@savesigndr')->name('savesigndr');
Route::post('/savesignirerror','StorageController@savesignirerror')->name('savesignirerror');
Route::post('/savesigndrerror','StorageController@savesigndrerror')->name('savesigndrerror');

/* TODAVIA NO SABEMOS PA QUE SIRVE!!
Route::put('post/{id}', function ($id) {
    //
})->middleware('auth', 'role:admin');
*/
