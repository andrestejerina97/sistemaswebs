<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternalResolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internal_resolutions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('receiver_id')->nullable();
            $table->integer('volume_id')->nullable();
            $table->string('filename');
            $table->string('name');
            $table->string('signed_filename')->nullable();
            $table->integer('sender_id')->nullable();
            $table->integer('addresse_id')->nullable();            
            $table->string('subject');
            $table->string('error')->nullable();
            $table->string('type');
            /* Los estados son los siguientes
            0 = pendiente de aproacion
            1 = firmado correctamente
            2 = firmado incorrectamente
            */
            $table->integer('status')->nullable();
            /* 
            La bandera es para identifcar los mensajes urgentes
            0 = normal
            1 = urgente
            */
            $table->integer('resolution_number');
            $table->year('year');
            $table->boolean('flag_urgent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internal_resolutions');
    }
}
