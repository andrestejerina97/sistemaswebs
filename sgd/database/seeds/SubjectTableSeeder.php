<?php

use Illuminate\Database\Seeder;
use App\Subject;

class SubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects= new Subject();
        $subjects->subject_name='Resolucion por maternidad';
        $subjects->save();

        $subjects= new Subject();
        $subjects->subject_name='Resolucion por Reemplazo';
        $subjects->save();
        $subjects= new Subject();
        $subjects->subject_name='Resolucion por licencia';
        $subjects->save();
    }
}
