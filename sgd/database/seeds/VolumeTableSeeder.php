<?php

use Illuminate\Database\Seeder;
use App\IrVolume;

class VolumeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $irvolume = new IrVolume();
        $irvolume->volume_number=1;
        $irvolume->year=2020;
        $irvolume->save();
    
    }
}
