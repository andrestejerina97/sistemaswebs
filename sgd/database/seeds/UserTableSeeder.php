<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_decano = Role::where('name', 'Decano')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $role_secadmin = Role::where('name', 'Secretario Administrativo')->first();
        $role_secacad = Role::where('name', 'Secretario Academico')->first();

        $user = new User();
        $user->name = 'Admin';
        $user->surname = 'admin';
        $user->email = 'admin@example.com';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'Marcelo';
        $user->surname = "Martinez";
        $user->email = 'decano.exactas@unlar.edu.ar';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_decano);   

        $user = new User();
        $user->name = 'Rosana';
        $user->surname = 'Diaz';
        $user->email = 'secretario.administrativo@unlar.edu.ar';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_secadmin);

        $user = new User();
        $user->name = 'Eugenio';
        $user->surname = 'Herrera';
        $user->email = 'secretario.academico@unlar.edu.ar';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_secacad);
    }
}
