<?php

use Illuminate\Database\Seeder;
use App\Error;

class ErrorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $errors= new Error();
        $errors->error_name='Resolucion erronea';
        $errors->save();

        $errors= new Error();
        $errors->error_name='Firma inexistente';
        $errors->save();

        $errors= new Error();
        $errors->error_name='Resolucion incompleta';
        $errors->save();
    }
}
