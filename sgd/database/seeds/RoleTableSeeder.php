<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->description = 'Administrator';
        $role->save(); 
               
        $role = new Role();
        $role->name = 'Decano';
        $role->description = 'Rol de Decano';
        $role->save();

        $role = new Role();
        $role->name = 'Secretario Administrativo';
        $role->description = 'Rol de Secretario Administrativo';
        $role->save();

        $role = new Role();
        $role->name = 'Secretario Academico';
        $role->description = 'Rol de Secretario Academico';
        $role->save();
    }
}
