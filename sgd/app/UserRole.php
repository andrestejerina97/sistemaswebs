<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userRole extends Model
{
    protected $table = 'role_user';
    //protected $fillable = ['subject_name',];
    public function users(){
        return $this
            ->hasMany('App\User');
            //->withtime
    }
    public function roles()
    {
        return $this
        ->belongsToMany('App\Role');
    }
}
