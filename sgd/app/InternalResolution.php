<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class InternalResolution extends Model
{
    protected $table = 'internal_resolutions';
    protected $fillable = ['filename','subject','addresse_id','resolution_number','name','signed_filename','year','user_id','volume_number','status','flag_urgent'];
    protected $guarded = ['id'];

    public function users(){
        return $this
            ->belongsTo('App\User');
    }
    public static function getResolutions($id_user,$status)
    {
        $list=InternalResolution::join('users', function($join){
            $join->on('users.id',"=","internal_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','internal_resolutions.subject')
                ->select('internal_resolutions.*','users.name as name_user','users.surname','subjects.subject_name')
                ->where('internal_resolutions.flag_urgent','=',0)
                ->where('internal_resolutions.status','=',$status)
                //->where('internal_resolutions.status','=',$status2)
                ->where('internal_resolutions.addresse_id','=',$id_user)
                ->get();
            return $list;
    }
    public static function getNotifications($id_user)
    {
        $number= InternalResolution::join('users', function($join){
            $join->on('users.id',"=","internal_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','internal_resolutions.subject')
                ->select('internal_resolutions.*','users.name as name_user','users.surname','subjects.subject_name')
                ->where('internal_resolutions.flag_urgent','=',0)
                ->where('internal_resolutions.status','=',0)
                ->where('internal_resolutions.addresse_id','=',$id_user)
                ->get()
                ->count();
                return $number;
    }
    public static function getResolutionUrgent($user_id,$status)
    {
      $list=InternalResolution::join('users', function($join){
            $join->on('users.id',"=","internal_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','internal_resolutions.subject')
                ->select('internal_resolutions.*','users.name as name_user','users.surname','subjects.subject_name')
                ->where('internal_resolutions.flag_urgent','=',1)
                ->where('internal_resolutions.status','=',$status)
                ->where('internal_resolutions.addresse_id','=', $user_id)
                ->get();
                return $list;
    }
    public static function getNotificationsUrgent($id_user){
        return InternalResolution::join('users', function($join){
            $join->on('users.id',"=","internal_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','internal_resolutions.subject')
                ->select('internal_resolutions.*','users.name as name_user','users.surname','subjects.subject_name')
                ->where('internal_resolutions.flag_urgent','=',1)
                ->where('internal_resolutions.status','=',0)
                ->where('internal_resolutions.addresse_id','=',$id_user)
                ->get()
                ->count();
    }
    static function getSend($id_user)
    {
        return InternalResolution::join('users', function($join){
            $join->on('users.id',"=","internal_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','internal_resolutions.subject')
                ->join('roles','roles.id','=','internal_resolutions.receiver_id')
                ->select('internal_resolutions.*','users.name as name_user','users.surname','subjects.subject_name','roles.name as rol_name')
                ->where('internal_resolutions.user_id','=', $id_user)
                ->get();
    }
    /*public static function getResolutionsSigned($id_user)
    {
        $list=InternalResolution::join('users', function($join){
            $join->on('users.id',"=","internal_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','internal_resolutions.subject')
                ->select('internal_resolutions.*','users.name as name_user','users.surname','subjects.subject_name')
                ->where('internal_resolutions.flag_urgent','=',0)
                ->where('internal_resolutions.status','=',1)
                ->where('internal_resolutions.addresse_id','=',$id_user)
                ->get();
            return $list;
    }*/
}
