<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IrVolume extends Model
{
    protected $table = 'ir_volumes';
    protected $fillable = ['volume_number','year'];
}
