<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartamentalResolution extends Model
{
    protected $table = 'departamental_resolutions';
    protected $fillable = ['filename','subject','addresse_id','signed_filename','resolution_number','name','year','user_id','volume_number','status','flag_urgent'];
    protected $guarded = ['id'];

    public function users(){
        return $this
            ->belongsTo('App\User');
    }
    public static function getResolutions($id_user,$status)
    {
       return DepartamentalResolution::join('users', function($join){
            $join->on('users.id',"=","departamental_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','departamental_resolutions.subject')
                ->select('departamental_resolutions.*','users.name as name_user','users.surname','subjects.subject_name')
                ->where('departamental_resolutions.flag_urgent','=',0)
                ->where('departamental_resolutions.status','=',$status)
                ->where('departamental_resolutions.addresse_id','=',$id_user)
                ->get();
    }
    public static function getNotifications($id_user)
    {
        return DepartamentalResolution::join('users', function($join){
            $join->on('users.id',"=","departamental_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','departamental_resolutions.subject')
                ->select('departamental_resolutions.*','users.name as name_user','users.surname','subjects.subject_name')
                ->where('departamental_resolutions.flag_urgent','=',0)
                ->where('departamental_resolutions.status','=',0)
                ->where('departamental_resolutions.addresse_id','=',$id_user)
                ->get()
                ->count();
                
    }

    public static function getResolutionUrgent($id_user,$status)
    {
        $list= DepartamentalResolution::join('users', function($join){
            $join->on('users.id',"=","departamental_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','departamental_resolutions.subject')
                ->select('departamental_resolutions.*','users.name as name_user','users.surname','subjects.subject_name')
                ->where('departamental_resolutions.flag_urgent','=',1)
                ->where('departamental_resolutions.status','=',$status)
                ->where('departamental_resolutions.addresse_id','=',$id_user)
                ->get();
                return $list;
    }
    public static function getNotificationsUrgent($id_user)
    {
        return DepartamentalResolution::join('users', function($join){
            $join->on('users.id',"=","departamental_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','departamental_resolutions.subject')
                ->select('departamental_resolutions.*','users.name as name_user','users.surname','subjects.subject_name')
                ->where('departamental_resolutions.flag_urgent','=',1)
                ->where('departamental_resolutions.status','=',0)
                ->where('departamental_resolutions.addresse_id','=', $id_user)
                ->get()
                ->count();
    }

    static function getSend($id_user)
    {
       return DepartamentalResolution::join('users', function($join){
            $join->on('users.id',"=","departamental_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','departamental_resolutions.subject')
                ->join('roles','roles.id','=','departamental_resolutions.receiver_id')
                ->select('departamental_resolutions.*','users.name as name_user','users.surname','subjects.subject_name','roles.name as rol_name')
                ->where('departamental_resolutions.user_id','=',$id_user)
                ->get();
    }
  /*  public static function getResolutionSigned($id_user)
    {
       return DepartamentalResolution::join('users', function($join){
            $join->on('users.id',"=","departamental_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','departamental_resolutions.subject')
                ->select('departamental_resolutions.*','users.name as name_user','users.surname','subjects.subject_name')
                ->where('departamental_resolutions.flag_urgent','=',0)
                ->where('departamental_resolutions.status','=',1)
                ->where('departamental_resolutions.addresse_id','=',$id_user)
                ->get();
    }*/
}
