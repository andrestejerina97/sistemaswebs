<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\InternalResolution as InternalResolution;
use App\DepartamentalResolution as DepartamentalResolution;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


   public function config(){
       return view('user.config');
   }

   public function update(Request $request){

    //Conseguir el usuario identificado
    $user = \Auth::user();

    $id = $user->id;

    //Validacion del formulario
    $validate = $this->validate($request, [
        'name' => 'required|regex:/^[\pL\s]+$/u',
        'surname' => 'required|regex:/^[\pL\s]+$/u',
        'dni' => 'required|integer|digits_between:7,8',
        'email' => 'required|string|email|max:255|unique:users,email,'.$id
    ]);
     
    //Recoger datos del formulario
    $name = $request->input('name');
    $surname = $request->input('surname');
    $dni = $request->input('dni');
    $email = $request->input('email');

    //Asignar nuevos valores al objeto del usuario
    $user->name = $name;
    $user->surname = $surname;
    $user->dni =  $dni;
    $user->email =  $email;

    //Ejecutar consulta y cambios en la BD
    $user->update();

    Return redirect()->route('config')->with(['message'=>'Usuario actualizado correctamente.']);

   }   
   
  

   
}
