<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SendController extends Controller
{
    public function sent(){
        //$resolutions = InternalResolution::all();
        //$resolutionsdr = DepartamentalResolution::all();
        //return view('sent', compact('resolutions'));


        $resolutions = InternalResolution::join('users', function($join){
            $join->on('users.id',"=","internal_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','internal_resolutions.subject')
                ->join('roles','roles.id','=','internal_resolutions.receiver_id')
                ->select('internal_resolutions.*','users.name as name_user','users.surname','subjects.subject_name','roles.name as rol_name')
                ->where('internal_resolutions.user_id','=', Auth::user()->id)
                //->where('internal_resolutions.flag_urgent','=',0)
                //->where('internal_resolutions.status','=',0)
                //->where('internal_resolutions.addresse_id','=', Auth::user()->id)
                ->get();
        
        $resolutionsdr = DepartamentalResolution::join('users', function($join){
            $join->on('users.id',"=","departamental_resolutions.user_id");})  
                ->join('subjects','subjects.id','=','departamental_resolutions.subject')
                ->join('roles','roles.id','=','departamental_resolutions.receiver_id')
                ->select('departamental_resolutions.*','users.name as name_user','users.surname','subjects.subject_name','roles.name as rol_name')
                ->where('departamental_resolutions.user_id','=', Auth::user()->id)
                //->where('departamental_resolutions.flag_urgent','=',0)
                //->where('departamental_resolutions.status','=',0)
                //->where('departamental_resolutions.addresse_id','=', Auth::user()->id)
                ->get();
        return view('sent')
                            ->with('resolutions',$resolutions)
                            ->with('resolutionsdr',$resolutionsdr);
   }
}
