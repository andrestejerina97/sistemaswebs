<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template;
use App\InternalResolution;
use App\DepartamentalResolution;
use App\User;
class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $templates = Template::orderBy('created_at','DESC')->paginate(30);
       return view('temindex', ['templates' => $templates]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('temupload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'template' => 'required|file|max:20000'
        ]);
        $upload = $request->file('template');
        $path = $upload->store('public/storage');
        $file = Template::create([
            'title' => $upload->getClientOriginalName(),
            'description' => '',
            'path' => $path
        ]);
        return redirect('/template1')->with('success','Correct');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dl = Template::find($id);
        return  response()->download(public_path('storage/'.$dl->path),$dl->title);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }   


    public function template(){
        return view('templates');
    }

    public function templateslist(){
        return view('templateslist');
    }
}
