<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\InternalResolution as InternalResolution;
use App\DepartamentalResolution as DepartamentalResolution;
use App\Role;
use App\UserRole;
use App\User;
use App\Error;

use PharIo\Manifest\Author;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $request->user()->authorizeRoles(['user', 'admin', 'Decano', 'Secretario Academico','Secretario Administrativo']);
        $role_id= userRole::select('role_id')->where('user_id','=',Auth::user()->id)->pluck('role_id')[0];

        switch ($role_id){
            case '1'://admin
      


                 
                break;
            case '2': //decano
                $resolutions = InternalResolution::getResolutions(Auth::user()->id,0);
               
        
                $resolutionsDR = DepartamentalResolution::getResolutions(Auth::user()->id,0);
              
        
                $notificationsRI = InternalResolution::getNotifications(Auth::user()->id);

                $notificationsRD = DepartamentalResolution::getNotifications(Auth::user()->id);
        
                $resolutionsU = InternalResolution::getResolutionUrgent(Auth::user()->id,0);
             

                $resolutionsDRU = DepartamentalResolution::getResolutionUrgent(Auth::user()->id,0);
             

                $notificationsU = InternalResolution::getNotificationsUrgent(Auth::user()->id);

                $notificationsRDU = DepartamentalResolution::getNotificationsUrgent(Auth::user()->id);

                $notificationsUrgent= $notificationsRDU + $notificationsU ;

                /*$resolutions = InternalResolution::getResolutions(Auth::user()->id,0);
                $resolutions2 = InternalResolution::getResolutions(Auth::user()->id,2);
        
                $resolutionsDR = DepartamentalResolution::getResolutions(Auth::user()->id,0);
                $resolutionsDR2 = DepartamentalResolution::getResolutions(Auth::user()->id,2);
        
                $notificationsRI = InternalResolution::getNotifications(Auth::user()->id);

                $notificationsRD = DepartamentalResolution::getNotifications(Auth::user()->id);
        
                $resolutionsU = InternalResolution::getResolutionUrgent(Auth::user()->id,0);
                $resolutionsU2 = InternalResolution::getResolutionUrgent(Auth::user()->id,2);

                $resolutionsDRU = DepartamentalResolution::getResolutionUrgent(Auth::user()->id,0);
                $resolutionsDRU2 = DepartamentalResolution::getResolutionUrgent(Auth::user()->id,2);

                $notificationsU = InternalResolution::getNotificationsUrgent(Auth::user()->id);

                $notificationsRDU = DepartamentalResolution::getNotificationsUrgent(Auth::user()->id);

                $notificationsUrgent= $notificationsRDU + $notificationsU ;*/
                
                
                break;
            case '3':// sec admin
                $resolutions = InternalResolution::getResolutions(Auth::user()->id,1);
        
                $resolutionsDR = DepartamentalResolution::getResolutions( Auth::user()->id,1);
                        
                $notificationsRI = InternalResolution::getResolutions(Auth::user()->id,1)->count();
                
                $notificationsRD = DepartamentalResolution::getResolutions( Auth::user()->id,1)->count();
                        
                $resolutionsU = InternalResolution::getResolutionUrgent(Auth::user()->id,1);
        
                $resolutionsDRU = DepartamentalResolution::getResolutionUrgent(Auth::user()->id,1);
        
                $notificationsU = InternalResolution::getResolutionUrgent(Auth::user()->id,1)->count();
        
                $notificationsRDU = DepartamentalResolution::getResolutionUrgent(Auth::user()->id,1)->count();
        
                $notificationsUrgent= $notificationsRDU + $notificationsU ;
                
                break;
            case '4': //sec academico 
                $resolutions = InternalResolution::getResolutions(Auth::user()->id,1);
        
                $resolutionsDR = DepartamentalResolution::getResolutions( Auth::user()->id,1);
                        
                $notificationsRI = InternalResolution::getResolutions(Auth::user()->id,1)->count();
                
                $notificationsRD = DepartamentalResolution::getResolutions( Auth::user()->id,1)->count();
                        
                $resolutionsU = InternalResolution::getResolutionUrgent(Auth::user()->id,1);
        
                $resolutionsDRU = DepartamentalResolution::getResolutionUrgent(Auth::user()->id,1);
        
                $notificationsU = InternalResolution::getResolutionUrgent(Auth::user()->id,1)->count();
        
                $notificationsRDU = DepartamentalResolution::getResolutionUrgent(Auth::user()->id,1)->count();
        
                $notificationsUrgent= $notificationsRDU + $notificationsU ;
                break;
            default:
                
                break;
        }
       // Auth::
       
        

        return view('home')
                ->with('resolutions',$resolutions)                
                ->with('resolutionsDR',$resolutionsDR)                
                ->with('resolutionsU',$resolutionsU)                
                ->with('resolutionsDRU',$resolutionsDRU)
                ->with('notificationsRI',$notificationsRI)
                ->with('notificationsRD',$notificationsRD)
                ->with('notificationsUrgent',$notificationsUrgent)
                ->with('role_id',$role_id);
        
    }

    public function hola(Request $request){
        //THIS IS THE PAPOTA
        $request->user()->authorizeRoles(['Decano', 'Secretario Academico']);  
        return view('hola');
    }
    public function buttonRead($id){
        $dl = InternalResolution::find($id);
        //$path = public_path('resolucionesInternas');
        return  response()->file(public_path('storage\resolucionesInternas/').$dl->filename);
    }

    public function buttonReadDr($id){
        $dl = DepartamentalResolution::find($id);
        //$path = public_path('resolucionesInternas');
        return  response()->file(public_path('storage\resolucionesDepartamentales/').$dl->filename);
    }
    public function buttonSign(Request $request){

        $users=User::all();
        return view('signir',compact('users'))
        ->with('filename',$request->input('name'))
        ->with('subject',$request->input('subject'))
        ->with('idir',$request->input('idir'));
    }

    public function buttonSignError(Request $request){

        $users=User::all();
        $errors= Error::all();
        return view('signirerror',compact('users','errors'))
        ->with('filename',$request->input('name'))
        ->with('subject',$request->input('subject'))
        ->with('idir_error',$request->input('idir_error'));
    }

    public function buttonSignDr(Request $request){
       
        $users= User::all();
        return view('signdr',compact('users'))
        ->with('filename',$request->input('name'))
        ->with('subject',$request->input('subject'))
        ->with('iddr',$request->input('iddr'));
    }

    public function buttonSignDrError(Request $request){
       
        $users= User::all();
        $errors = Error::all();
        return view('signdrerror',compact('users','errors'))
        ->with('filename',$request->input('name'))
        ->with('subject',$request->input('subject'))
        ->with('iddr_error',$request->input('iddr_error'));
    }

 


}
