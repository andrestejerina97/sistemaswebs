<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\InternalResolution as InternalResolution;
use App\DepartamentalResolution as DepartamentalResolution;
use App\Subject;
use App\User;

class FormController extends Controller
{
    public function formRi(){
        $subjects = Subject::all();
        $users=User::all();
        return view('formRi',compact('subjects','users'));
    }

    public function formRd(){
        $subjects = Subject::all();
        $users=User::all();
        return view('formRd',compact('subjects','users'));
    }

    public function selectResolution(){
        return view('selectResolution');
    }
}
