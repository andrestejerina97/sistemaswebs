<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InternalResolution as InternalResolution;
use App\DepartamentalResolution as DepartamentalResolution;
use App\User;
use App\Error;
use App\IrVolume;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StorageController extends Controller
{
	
/**
* muestra el formulario para guardar archivos
*
* @return Response
*/
public function index()
   {
      return \View::make('new');
   }

      
   /**
   * guarda un archivo en nuestro directorio local.
   *
   * @return Response
   */
public function save(Request $request)
   {
      $file = $request->file('file');
      
         $currentUser = Auth::user();
         $resolution = new InternalResolution();
         $resolution->filename = $file->getClientOriginalName();
         $resolution->name = 'RI '.$request->input('resolution_number').'/'.$request->input('year');
         $resolution->subject = $request->input('subject','no encontrado');
         if($request->input('flag_urgent')==1){
            $resolution->flag_urgent=1;
         }else{
            $resolution->flag_urgent=0;
         }        
         $resolution->status = 0;
         $resolution->resolution_number = $request->input('resolution_number','no encontrado');
         $resolution->year = $request->input('year','no encontrado');
         $resolution->user_id=$currentUser->id;
         $resolution->sender_id=$currentUser->id;
         $resolution->volume_id= $this->GetVolumeNumber();
         $resolution->addresse_id = $request->input('addresse');
         $resolution->receiver_id = $request->input('addresse');
         $resolution->type= 'Interna';
         $resolution->save();
         //obtenemos el campo file definido en el formulario
        

        //obtenemos el nombre del archivo
        $nombre = $file->getClientOriginalName();
    
        //indicamos que queremos guardar un nuevo archivo en el disco local
        //Storage::disk('local')->put($nombre,  \File::get($file));
        $path = $file->storeAs('resolucionesInternas',$nombre);

        $resolutions = InternalResolution::all();
       // $users = User::join("roles","users.roles_id","=","roles.id")
         //->where('users.estado','=',1)
         //->get();
        
        return redirect('/')->with('resolutions',$resolutions);
   }

public function GetVolumeNumber(){
      $ultimateVolume= DB::table('ir_volumes')->max('volume_number'); //Obtiene el ultimo volumen creado
      $idVolumeUltimate=DB::table('ir_volumes')->where('volume_number','=',$ultimateVolume)->get()->first(); // Obtiene el ID del último volumen creado en la tabla ir_volumes
      $resolutionNumberForVolume= DB::table('internal_resolutions')->where('volume_id','=',$idVolumeUltimate->id)->get()->count();//Cuenta cuantas resoluciones existen para el último volumen existente 
      
      if($resolutionNumberForVolume >= 5)
      { // pregunto si la cantidad de resoluciones supera el limite
         /* a partir de aqui agrego un nuevo volumen  usando el modelo*/
         $irVolume= new IrVolume();
         $ultimateVolume++;  // incremento el volumen
         $irVolume->volume_number=$ultimateVolume;
         $irVolume->year=date("Y");
         $irVolume->save();
         /* fin agregar nuevo volumen */
         $idVolumeUltimate->id=$irVolume->id; // reemplazo por el id del volumen creado
            
      }
      return $idVolumeUltimate->id; // devuelvo el id del volumen que se le asginará a la nueva resolución creada
      //DB::table('internal_resolutions')->where()
}

public function saveDR(Request $request)
   {
      $file = $request->file('file');
      
         $currentUser = Auth::user();
         $resolutionDR = new DepartamentalResolution();
         $resolutionDR->filename = $file->getClientOriginalName();
         $resolutionDR->name = 'RD '.$request->input('resolution_number').'/'.$request->input('year');
         $resolutionDR->subject = $request->input('subject','no encontrado');
         if($request->input('flag_urgent')==1){
            $resolutionDR->flag_urgent=1;
         }else{
            $resolutionDR->flag_urgent=0;
         }        
         $resolutionDR->status = 0;
         $resolutionDR->resolution_number = $request->input('resolution_number','no encontrado');
         $resolutionDR->year = $request->input('year','no encontrado');
         $resolutionDR->user_id=$currentUser->id;
         $resolutionDR->sender_id=$currentUser->id;
         $resolutionDR->volume_id= $this->GetVolumeNumber();
         $resolutionDR->addresse_id = $request->input('addresse');
         $resolutionDR->receiver_id = $request->input('addresse');
         $resolutionDR->type= 'Departamental';
         $resolutionDR->save();
         //obtenemos el campo file definido en el formulario
        
    
        //obtenemos el nombre del archivo
        $nombre = $file->getClientOriginalName();
    
        //indicamos que queremos guardar un nuevo archivo en el disco local
        //\Storage::disk('local')->put($nombre,  \File::get($file));
        //$path = $file->store('resolucionesDepartamentales');
        $path = $file->storeAs('resolucionesDepartamentales',$nombre);

        $resolutionsDR = DepartamentalResolution::all();
       // $users = User::join("roles","users.roles_id","=","roles.id")
         //->where('users.estado','=',1)
         //->get();
        
        return redirect('/')->with('resolutionsDR',$resolutionsDR);
   }

public function savesignir(Request $request){

      $file = $request->file('file');
      //obtenemos el nombre del archivo
      $nombre = $file->getClientOriginalName();    
      //indicamos que queremos guardar un nuevo archivo en el disco local
      //Storage::disk('local')->put($nombre,  \File::get($file));
      $path = $file->storeAs('resolucionesInternas/signed',$nombre);


      $ir = InternalResolution::find($request->input('idir'));
      $ir->signed_filename = 'Signed_'.$nombre;
      $ir->sender_id = Auth::user()->id;
      $ir->addresse_id = $request->input('addresse');
      $ir->status=1;
      $ir->save();

      $resolutions = InternalResolution::all();
      // $users = User::join("roles","users.roles_id","=","roles.id")
        //->where('users.estado','=',1)
        //->get();
       
       return redirect('/')->with('resolutions',$resolutions);     
}

public function savesignirerror(Request $request){

   $file = $request->file('file');
   //obtenemos el nombre del archivo
   $nombre = $file->getClientOriginalName();    
   //indicamos que queremos guardar un nuevo archivo en el disco local
   //Storage::disk('local')->put($nombre,  \File::get($file));
   $path = $file->storeAs('resolucionesInternas/signed',$nombre);


   $ir = InternalResolution::find($request->input('idir_error'));
   $ir->signed_filename = 'Signed_'.$nombre;
   $ir->sender_id = Auth::user()->id;
   if($request->input('flag_urgent')==1){
      $ir->flag_urgent=1;
   }else{
      $ir->flag_urgent=0;
   }    
   $ir->addresse_id = $request->input('addresse');
   $ir->error = $request->input('error');
   $ir->status=2;
   $ir->save();

   $resolutions = InternalResolution::all();
   // $users = User::join("roles","users.roles_id","=","roles.id")
     //->where('users.estado','=',1)
     //->get();
    
    return redirect('/')->with('resolutions',$resolutions);     
}


public function savesigndr(Request $request){

   $file = $request->file('file');
   //obtenemos el nombre del archivo
   $nombre = $file->getClientOriginalName();    
   //indicamos que queremos guardar un nuevo archivo en el disco local
   //Storage::disk('local')->put($nombre,  \File::get($file));
   $path = $file->storeAs('resolucionesDepartamentales/signed',$nombre);


   $dr = DepartamentalResolution::find($request->input('iddr'));
   $dr->signed_filename = 'Signed_'.$nombre;
   $dr->sender_id = Auth::user()->id;
   $dr->addresse_id = $request->input('addresse');
   $dr->status=1;
   $dr->save();

   $resolutionsDR = DepartamentalResolution::all();
   // $users = User::join("roles","users.roles_id","=","roles.id")
     //->where('users.estado','=',1)
     //->get();
    
    return redirect('/')->with('resolutionsDR',$resolutionsDR);     
}


public function savesigndrerror(Request $request){

   $file = $request->file('file');
   //obtenemos el nombre del archivo
   $nombre = $file->getClientOriginalName();    
   //indicamos que queremos guardar un nuevo archivo en el disco local
   //Storage::disk('local')->put($nombre,  \File::get($file));
   $path = $file->storeAs('resolucionesDepartamentales/signed',$nombre);


   $dr = DepartamentalResolution::find($request->input('iddr_error'));
   $dr->signed_filename = 'Signed_'.$nombre;
   $dr->sender_id = Auth::user()->id;
   if($request->input('flag_urgent')==1){
      $dr->flag_urgent=1;
   }else{
      $dr->flag_urgent=0;
   }    
   $dr->addresse_id = $request->input('addresse');
   $dr->error = $request->input('error');
   $dr->status=2;
   $dr->save();

   $resolutionsDR = DepartamentalResolution::all();
   // $users = User::join("roles","users.roles_id","=","roles.id")
     //->where('users.estado','=',1)
     //->get();
    
    return redirect('/')->with('resolutionsDR',$resolutionsDR);     
}
}

