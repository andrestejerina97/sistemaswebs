<?php

namespace App\Http\Controllers;

use App\DepartamentalResolution;
use App\InternalResolution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ToolController extends Controller
{
    public function sent(){
        //$resolutions = InternalResolution::all();
        //$resolutionsdr = DepartamentalResolution::all();
        //return view('sent', compact('resolutions'));


        $resolutions = InternalResolution::getSend(Auth::user()->id);
        
        $resolutionsdr = DepartamentalResolution::getSend(Auth::user()->id);
        return view('sent')
                            ->with('resolutions',$resolutions)
                            ->with('resolutionsdr',$resolutionsdr);
   }
}
