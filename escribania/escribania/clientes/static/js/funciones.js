

  obtenerRegistros();

toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": true,
  "onclick": null,
  "showDuration": "100",
  "hideDuration": "1000",
  "timeOut": "2000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "show",
  "hideMethod": "hide"
};







$( "form" ).submit(function( event ) {
  event.preventDefault();
  var form = $(this);
  $.ajax({
    url: form.attr("action"),
    data: form.serialize(),
    type: form.attr("method"),
    success: function (Persona) {
      toastr.success('Cliente guardado!');
      obtenerRegistros();
     
      $("#formularioIngreso")[0].reset();
      
    },
    error:function(Persona) {
        console.log("No se ha podido obtener la información");
    }
  });
});

function obtenerRegistros () {
  $.ajax({
  url:"/motrarPersonas/",
  data: {csrfmiddlewaretoken: csrftoken },
  type: "POST",
  success: function (data) {
    var bodyTabla="";
    var date=new Date();
      for (var index = 0; index < data.length; index++) {
            var fechaDeNacimiento=new Date(data[index].fields.fechaNacimiento);
            var edad=date.getFullYear()-fechaDeNacimiento.getFullYear();
            bodyTabla+=`<tr>
                  <th>`+data[index].fields.nombre+`</th>
                  <th>`+data[index].fields.apellido+`</th>
                  <th>`+data[index].fields.dni+`</th>
                  <th>`+data[index].fields.domicilio+`</th>
                  <th>`+data[index].fields.celular+`</th>
                  <th>`+data[index].fields.correo+`</th>
                  <th>`+data[index].fields.telefono+`</th>
                 
                  <th><button class="btn btn-success" data-toggle="modal" data-target="#modalPersona" onclick="editarPersona(`+data[index].pk+`);">Editar</button>
                    <button class="btn btn-danger" onclick="eliminarPersona(`+data[index].pk+`);">Borrar</button></th>
                </tr>`;
  }
  $("#insertarPersonaTabla").html(bodyTabla);
  },
  error:function(data) {
      console.log("No se ha podido obtener la información");
  }
});
}

function editarPersona(idPersona) {
  $.ajax({
  url:"/obtenerPersona/",
  data: {'id':idPersona,csrfmiddlewaretoken: csrftoken },
  type: "POST",
  success: function (Persona) {
    $("#idPersona").val(Persona[0].pk);
    $("#editarNombre").val(Persona[0].fields.nombre);
    $("#editarApellido").val(Persona[0].fields.apellido);
    $("#editarDni").val(Persona[0].fields.dni);
    $("#editarDomicilio").val(Persona[0].fields.domicilio);
    $("#editarTelefono").val(Persona[0].fields.telefono);
    $("#editarCelular").val(Persona[0].fields.celular);
    $("#editarEmail").val(Persona[0].fields.correo);
    $("#editarFechaNacimiento").val(Persona[0].fields.fechaNacimiento);
    $('.ui.modal')
      .modal('show')
    ;
  },
  error:function(data) {
      console.log("No se ha podido obtener la información");
  }
});

}



function guardarCambios() {
  var formulario= $("#formularioEditar");
  $.ajax({
  url:formulario.attr('action'),
  data:formulario.serialize(),
  type: formulario.attr('method'),
  success: function (Persona) {
    toastr.success('Cliente Modificado!');
  obtenerRegistros();
  },
  error:function(Persona) {
      console.log("No se ha podido obtener la información");
  }
});

}


function eliminarPersona(idPersona) {

  bootbox.confirm({
    title: "Confirmación",
    message: "Esta seguro de eliminar el cliente?",
    buttons: {
        cancel: {
            label: '<i class="fa fa-times"></i> NO',
            className: 'btn-danger'
        },
        confirm: {
            label: '<i class="fa fa-check"></i> SI',
            className: 'btn-info'
        }
    },
    callback: function (result) {
        console.log('This was logged in the callback: ' + result);



if (result==true) {


  $.ajax({
    url:"/eliminarDatosPersonales/",
    data: {'id':idPersona,csrfmiddlewaretoken: csrftoken },
    type: "POST",
    success: function (Persona) {
  
      toastr.error('Cliente eliminado!');
      obtenerRegistros();
   
  
    
    },
    error:function(Persona) {
        console.log("No se ha podido obtener la información");
    }
    });



  
}
    }
});



}



