from django import forms
from.models import Clientes
class formularioPersona (forms.ModelForm):
    class Meta:
        model=Clientes
        fields=[
            'nombre',
            'apellido',
            'dni',
            'domicilio',
            'telefono',
            'celular',
            'correo',
            'fechaNacimiento',
        ]
        widgets={
            'nombre':forms.TextInput(attrs={'class':'form-control' ,'id':'nombre'}),
            'apellido':forms.TextInput(attrs={'class':'form-control'}),
            'correo':forms.TextInput(attrs={'class':'form-control' }),
            'dni':forms.TextInput(attrs={'class':'form-control' }),
            'domicilio':forms.TextInput(attrs={'class':'form-control' }),
            'telefono':forms.TextInput(attrs={'class':'form-control' }),
            'celular':forms.TextInput(attrs={'class':'form-control' }),
            'fechaNacimiento':forms.DateInput(attrs={'class':'form-control' }),
            
        }