from django.urls import path
from.views import clientes,cargarPersona,listaCompleta,obtenerPersona,actualizarDatosPersona,eliminarPersona

urlpatterns = [
    path('clientes/',clientes,name="clientes"),
    path('cargarDatosPersonales/', cargarPersona, name='cargaDatosPersona'),#CREATE
    path('motrarPersonas/', listaCompleta, name='DatosPersonas'),#READ
    path('obtenerPersona/', obtenerPersona, name='DatoPersona'),
    path('actualizarPersona/', actualizarDatosPersona, name='actualizarDatosPersona'),#UPDATE
    path('eliminarDatosPersonales/', eliminarPersona, name='eliminarDatosPersona'),#DELETE
]