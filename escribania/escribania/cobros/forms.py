from django import forms
from.models import Cobros
class formularioCobros (forms.ModelForm):
    class Meta:
        model=Cobros
        fields=[
            'concepto',
            'monto',
            'observacion',
        ]
        