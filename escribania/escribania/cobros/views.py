from django.shortcuts import render
from .models import Cobros
from .forms import formularioCobros

# Create your views here.
def cobros(request):
    return render(request,'cobros.html')
    
def cargarCobro(request):
    if request.method == 'POST':
        form=formularioCobros(request.POST)
        if form.is_valid():  
            try:  
                form.save()  
                return redirect('/cobros/')  
            except:  
                pass  
        else:  
            form = formularioCobros
    
    return render(request,'cobros.html')  
       
    