from django.db import models
from datetime import date
# Create your models here.
class Pagos(models.Model):
    concepto=models.CharField(max_length=80,blank=True,null=True)
    monto=models.FloatField(blank=True,null=True)
    observacion=models.CharField(max_length=80,blank=True,null=True)
    created_at = models.DateField(default=date.today)