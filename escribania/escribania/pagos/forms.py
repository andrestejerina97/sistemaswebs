from django import forms
from.models import Pagos
class formularioPagos (forms.ModelForm):
    class Meta:
        model=Pagos
        fields=[
            'concepto',
            'monto',
            'observacion',
        ]
        