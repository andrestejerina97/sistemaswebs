from django.shortcuts import render, HttpResponse
from .models import Pagos
from .forms import formularioPagos
# Create your views here.
def pagos(request):
    return render(request,'pagos.html')
def cargarPago(request):
    if request.method == 'POST':
        form=formularioPagos(request.POST)
        if form.is_valid():  
            try:  
                form.save()  
                return redirect('/pagos/')  
            except:  
                pass  
        else:  
            form = formularioPagos
    
    return render(request,'pagos.html')  
       
    

