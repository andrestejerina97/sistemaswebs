from django.shortcuts import render,HttpResponse
from clientes.models import Clientes
############ Librerias PDF###############
from io import BytesIO
from django.template.loader import get_template
from xhtml2pdf import pisa
from jinja2 import Environment, FileSystemLoader
# Create your views here.

def formularios(request):
    return render(request,'formulario.html')


def otros(request):
    return render(request,'otros.html')


def generaPdf2(request):
  print(request.POST)
  env=Environment(loader=FileSystemLoader("templates"))
  template= env.get_template('minutas.html')
  result = BytesIO()
  datos={
    'nombre':request.POST.get('nombre'),
    'apellido':request.POST.get('apellido')
  }
  print(datos)
  html= template.render(datos)
  pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)

  return HttpResponse(result.getvalue(), content_type='application/pdf')


def generaPdf(request):
  #print(request.POST)
  valor=request.POST['dni'] 
  datosCliente=Clientes.objects.filter(dni=valor).values('id','nombre','apellido','telefono','domicilio','fechaNacimiento')
  datos={}
  for i in datosCliente:
    datos=i
    #print(i)
  #print(datos)
  env=Environment(loader=FileSystemLoader("templates"))
  template= env.get_template('minutas.html')
  result = BytesIO()
  print(datos)
  html= template.render(datos)
  pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)

  return HttpResponse(result.getvalue(), content_type='application/pdf')