from django.urls import path
from.views import formularios,otros,generaPdf

urlpatterns = [
    path('minutas/',formularios,name="formulario"),
    path('otros/',otros,name="otros"),
    path('generarPdf/',generaPdf,name="pdf"),
]