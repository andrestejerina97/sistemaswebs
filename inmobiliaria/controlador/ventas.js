function buscar_cliente() {
    var dni=$("#buscarDni").val();
    $(document).ready(function() {  //esto sirve para controlar todo el dom desde que carga 
      //aqui empieza la peticion ajax  
         $.ajax({
           beforeSend: function(){
  
            },
           url: 'modelo/buscar_clientes.php',
           type: 'POST',
           method:'POST',
          
           data: 'dni='+dni,
          
           success: function(datos){
      
             
             if(datos==0){
               
               alert("NO HAY CLIENTES CON ESE DNI REGISTRADOS,PUEDE REGISTRAR UNO NUEVO");
         
  
  
              }else{
  $("#tabla_clientes").html(datos);
  $("#seccionLotes").css({display: "block"});
  comboLotes();
              }
  
             }
             ,
             /**************************/
           error: function(jqXHR,estado,error){
            alert("cuidado:"+ estado+" "+ error);
             }
          }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos
  
       });   
}
function comboLotes() {
  $(document).ready(function() {
  $.ajax({
    beforeSend: function(){

     },
    url: 'modelo/combo_lotes.php',
    type: 'POST',
    success: function(datos){
      if(datos==0){


      }else{

        $("#comboLotes").html(datos);
      


      }


      }
      ,
      /**************************/
    error: function(jqXHR,estado,error){
     alert("cuidado:"+ estado+" "+ error);
      }
   });
  });   
}
function comboTerrenos(value) {
var id=value;


  $.ajax({
  beforeSend: function(){

     },  
    url: 'modelo/combo_terrenos.php',
    type: 'POST',
   data: 'id='+id,
    success: function(datos){
      if(datos==0){


      }else{

        $("#comboTerrenosCompleto").html(datos);
            }


      }
      ,
      /**************************/
    error: function(jqXHR,estado,error){
     alert("cuidado:"+ estado+" "+ error);
      }
   });

  
}
function habilitarVenta() {

  $("#seccionVenta").css({display: "block"});

}
function calcularVenta() {
  var precio=parseInt($("#precio").cleanVal());
  var entrega=parseInt($("#entrega").cleanVal());
  var monto= precio - entrega;
var cuotas=parseInt($("#cuotas").val());
var interes=parseInt($("#interes").val());
if(monto && interes && cuotas){
total= Math.round(interes*monto/100);
totalf=Math.round(monto + total);
totalc=Math.round(totalf/cuotas);
$("#saldo").text("Saldo: $"+monto)

$("#resultadoVenta").text(" TOTAL: "+ cuotas +" CUOTAS DE $"+ totalc )
}
}

function pulsar(q) {
  if (q.keyCode === 13 && !q.shiftKey) {
      q.preventDefault();
      buscar_cliente();
   
  }
}
$(document).ready(function(){

  $('.money').mask('000.000.000.000.000', {reverse: true});
  $('.dni').mask('0000000000');

});

function procesa_venta() {
var precio=parseInt($("#precio").cleanVal());
var entrega=parseInt($("#entrega").cleanVal());
var saldo= precio - entrega;

var cuotas=parseInt($("#cuotas").val());
var interes=parseInt($("#interes").val());
var lote=$("#comboLotesCompleto option:selected").val();
var terreno=$("#comboTerrenosCompleto option:selected").val();
var id=parseInt($("#idcliente").val());
total= Math.round(interes*saldo/100);
totalf=Math.round(saldo +total);
totalc=Math.round(totalf/cuotas);
var data= { 
  precio: precio,
  entrega: entrega,
  saldo:saldo,
  cuotas:cuotas,
  interes:interes,
  lote:lote,
  terreno:terreno,
  id:id,
  cuotaValor:totalc,

};
$.ajax({
  url: 'modelo/procesa_venta.php',
  type: 'POST',
  data:  data,
  success: function(datos){
    if(datos==0){
    
    }else{
   alert("venta exitosa");
$("#precio").val("");
$("#entrega").val("");
$("#cuotas").val("");
$("#interes").val("");

$("#idcliente").val("");

$("#buscarDni").val("");
$("#modal_confirmar_venta").modal("toggle");
location.reload();
$("#buscarDni").focus();
    }


      }
      ,
      /**************************/
    error: function(jqXHR,estado,error){
     alert("cuidado:"+ estado+" "+ error);
      }
   });





}