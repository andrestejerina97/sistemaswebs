<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Inicio- inmobiliaria</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

  <?php include 'sidebar.php' ?>



    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

<?php include 'navbar.php' ?>
   
      <section >
      <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="row justify-content-center">
          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Cobros</h1>
      </div>
    
        <div class="row justify-content-center">
         <form  >
                   
                     <div class='input-group'>
                    <input class="form-control align-self-center" type="text" onchange='busca_articulo_cambio();'  placeholder='Ingresar DNI del cliente'> 
                    <button type='button' class='btn btn-success btn-sm' onclick='busca_articulo_cambio();' id='btn-buscar-cambio'><i class='fa fa-search'></i>Continuar</button>
                   
                    </div>
                   

                </form>
</div>
</section>
<div class="container-fluid">

          <div class="row justify-content-center">
          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Cobros actuales disponibles</h1>
      </div>
    
        <div class="row justify-content-center">
<table class="table-hover">
  
<div class="">
 <th>cuotas</th>
 <th>cliente</th>
  <tr>
  <td>13323</td>
  </tr>
</div>  
</table>

</div>

<section>
  

</section>
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
 <?php include 'footer.php' ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

 


  <!-- Scroll to Top Button-->

  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
