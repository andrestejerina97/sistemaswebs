<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Simulador de ventas</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

  <?php include 'sidebar.php' ?>


    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

<?php include 'navbar.php' ?>
   

<section>       <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="row justify-content-center">
          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">VENTAS</h1>
      </div>
      <section >
        <div class="row justify-content-center">
         <form >
             
                     <div class='input-group'>
                    <input class="form-control dni" id="buscarDni" onkeypress="pulsar(event);"    placeholder='Ingresar DNI del cliente'> 
                    <button type='button' class='btn btn-success btn-sm' onclick='buscar_cliente();' id='btn-buscar-cambio'><i class='fa fa-search'></i>Buscar...</button>
                   
                  
                
                    
                    </div>
                   

                </form>
               
         
</div>
<div id="tabla_clientes"></div>
</section>
<section id="seccionLotes" style='display:none'>
                
          <div class="container col-md-6">
          <form onchange="habilitarVenta();">
            <div id="comboLotes" class="form-group">
           <label for="">Seleccione Loteo:</label>
        
           </div> 
           <div class="form-group">
           <label for="">Seleccione terreno:</label>
           <select class="form-control" id="comboTerrenosCompleto">
             
           </select>
           </div> 
           <div class="form-group">
           <label for="">Precio $:</label>
           <input class="form-control money" id="precio"  placeholder='Ingresar precio...' required> 
           </div> 
         
           </form>
            </div>
          
</section>
<br><br>
<section id="seccionVenta" class="row" style="display:none">
               
         <div class="container col-md-6">
         <form onchange="calcularVenta();">
           <div class="form-group">
          <label for="">Entrega $:</label>
          <input id="entrega" class="form-control money">
          </div>
         
          <div class="form-group">
           <label for="">Cantidad de cuotas:</label>
          <input type="number" id="cuotas" class="form-control ">
           </div> 
           <div class="form-group">
           <label for="">Tasa de interés(%):</label>
         <input id="interes" class="form-control">
           </div> 
      
           <div class="card ">
  <div class="card-body">
    <h5 class="card-title" id="saldo"></h5>
    <h5 class="card-subtitle mb-2 text-muted" id="resultadoVenta"></h5>

  </div>
</div>
     
<br>
          <div class="btn-group">
        
            <button type='button'data-target="#modal_confirmar_venta" data-toggle="modal" class='btn btn-raised btn-primary btn-lg'>Confirmar</button>
          </div>
          </form>
            </div>
           
           
</section>


        </div>
        <!-- /.container-fluid -->
</section>

      </div>
      <!-- End of Main Content -->


      <!-- Footer -->
 <?php include 'footer.php' ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->










<!-- Logout Modal-->
  <div class="modal fade" id="modal_confirmar_venta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea confirmar la compra?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
        <button class="btn btn-primary" onclick="procesa_venta();" type="submit">Confirmar </button>

        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
        
        </div>
      </div>
    </div>
  </div>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script src="plugins/inputMask/dist/jquery.mask.min.js"> </script>
<script src="controlador/ventas.js"></script>

</body>

</html>
