-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-06-2019 a las 04:36:19
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inmobiliaria_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `idclientes` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `telefono` int(11) NOT NULL,
  `email` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`idclientes`, `dni`, `apellido`, `nombre`, `telefono`, `email`) VALUES
(1, 0, '', '', 0, ''),
(2, 39989093, 'tejerina', 'josÃ© andrÃ©s', 2147483647, 'andresteje@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobranzas`
--

CREATE TABLE `cobranzas` (
  `idcobranzas` int(11) NOT NULL,
  `monto_cobro` float NOT NULL,
  `fecha` date NOT NULL,
  `tipo_de_pago` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuotas`
--

CREATE TABLE `cuotas` (
  `idcuotas` int(11) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `numero_de_cuota` int(11) NOT NULL,
  `precio` float NOT NULL,
  `ventas_idventas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuotas`
--

INSERT INTO `cuotas` (`idcuotas`, `estado`, `numero_de_cuota`, `precio`, `ventas_idventas`) VALUES
(41, 'En mora', 1, 49500, 20),
(42, 'En mora', 2, 49500, 20),
(43, 'En mora', 1, 4950, 21),
(44, 'En mora', 2, 4950, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `loteos`
--

CREATE TABLE `loteos` (
  `idloteos` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `ubicación` varchar(45) NOT NULL,
  `cantidad_de_terrenoss` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `loteos`
--

INSERT INTO `loteos` (`idloteos`, `nombre`, `ubicación`, `cantidad_de_terrenoss`) VALUES
(1, 'los perales', 'zona norte', 100),
(2, 'los alisos', 'sur', 233);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `terrenos`
--

CREATE TABLE `terrenos` (
  `idterrenos` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `ubicacion` varchar(45) NOT NULL,
  `medidas` varchar(45) NOT NULL,
  `precio` float NOT NULL,
  `observaciones` varchar(45) NOT NULL,
  `loteos_idloteos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `terrenos`
--

INSERT INTO `terrenos` (`idterrenos`, `numero`, `ubicacion`, `medidas`, `precio`, `observaciones`, `loteos_idloteos`) VALUES
(1, 1, 'sur', '10x25', 1000000, '', 2),
(2, 1, 'zona norte', '25x35', 900000, '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `idventas` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `monto` float NOT NULL,
  `cuotas` int(11) NOT NULL,
  `interes` float NOT NULL,
  `clientes_idclientes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`idventas`, `fecha`, `monto`, `cuotas`, `interes`, `clientes_idclientes`) VALUES
(1, '0000-00-00', 30000, 12, 25, 2),
(2, '0000-00-00', 30000, 12, 25, 2),
(3, '0000-00-00', 30000, 12, 25, 2),
(4, '0000-00-00', 4998, 2, 10, 2),
(5, '0000-00-00', 4998, 2, 10, 2),
(6, '0000-00-00', 30000, 12, 25, 2),
(7, '0000-00-00', 30000, 12, 25, 2),
(8, '0000-00-00', 420000, 4, 4, 2),
(9, '0000-00-00', 5550, 5, 5, 2),
(10, '0000-00-00', 45450, 4, 4, 2),
(11, '0000-00-00', 41000, 45, 45, 2),
(12, '0000-00-00', 20181, 3, 23, 2),
(13, '0000-00-00', 4440, 4, 4, 2),
(14, '0000-00-00', 10000, 3, 10, 2),
(15, '0000-00-00', 44955, 5, 25, 2),
(16, '2019-06-16', 10000, 5, 10, 2),
(17, '2019-06-16', 100000, 2, 15, 2),
(18, '2019-06-16', 500000, 5, 15, 2),
(20, '2019-06-16', 100000, 2, 0, 2),
(21, '2019-06-16', 10000, 2, 0, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`idclientes`);

--
-- Indices de la tabla `cobranzas`
--
ALTER TABLE `cobranzas`
  ADD PRIMARY KEY (`idcobranzas`);

--
-- Indices de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD PRIMARY KEY (`idcuotas`),
  ADD KEY `fk_cuotas_ventas1_idx` (`ventas_idventas`);

--
-- Indices de la tabla `loteos`
--
ALTER TABLE `loteos`
  ADD PRIMARY KEY (`idloteos`);

--
-- Indices de la tabla `terrenos`
--
ALTER TABLE `terrenos`
  ADD PRIMARY KEY (`idterrenos`),
  ADD KEY `fk_terrenos_loteos_idx` (`loteos_idloteos`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuarios`),
  ADD UNIQUE KEY `usuario_UNIQUE` (`usuario`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`idventas`),
  ADD KEY `fk_ventas_clientes1_idx` (`clientes_idclientes`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `idclientes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cobranzas`
--
ALTER TABLE `cobranzas`
  MODIFY `idcobranzas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  MODIFY `idcuotas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `loteos`
--
ALTER TABLE `loteos`
  MODIFY `idloteos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `terrenos`
--
ALTER TABLE `terrenos`
  MODIFY `idterrenos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `idventas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD CONSTRAINT `fk_cuotas_ventas1` FOREIGN KEY (`ventas_idventas`) REFERENCES `ventas` (`idventas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `terrenos`
--
ALTER TABLE `terrenos`
  ADD CONSTRAINT `fk_terrenos_loteos` FOREIGN KEY (`loteos_idloteos`) REFERENCES `loteos` (`idloteos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_ventas_clientes1` FOREIGN KEY (`clientes_idclientes`) REFERENCES `clientes` (`idclientes`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
