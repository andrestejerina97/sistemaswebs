<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Inicio- inmobiliaria</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

  <?php include 'sidebar.php' ?>
 <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
<?php include 'navbar.php' ?>
   
     <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
         <div class="row justify-content-center">
          <h1 class="h3 mb-4 text-gray-800">CARGA DE CLIENTES</h1>
</div>
          

   <div class="container col-9">      
                    <form >
                   <div class="row">
                     <div class='input-group'>
                    <input class="form-control" type="text" id="buscarDni" placeholder='Ingresar DNI del cliente'> 
                    <button type='button' class='btn btn-success btn-sm' onclick='buscar_cliente();' id='btn-buscar-cambio'><i class='fa fa-search'></i>Buscar...</button>
                   <div class="col-sm-4">
                    <button type='button' class='btn btn-primary 'data-target="#modal_nuevo_cliente" data-toggle="modal" id='btn-buscar-cambio'><i class=' fas fa-man'></i>Nuevo cliente</button>
                    </div>
                    </div>
                    </div>
                 </form>

     
        </div>
  <br>
        <div id="tabla_clientes"></div>
      
     
 <div class="modal fade" id ="modal_nuevo_cliente" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">

              <h4 class="modal-title">Ingrese nuevo cliente:</h4>       
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
           
          </div>
          <div class="modal-body">
           <div class="row justify-content-center">
     
           </div>
            <form action="">
           <div class="row">
              <div class="form-group col-md">
                <label for="">Apellido:</label>
                <input type="text" id="apellido" class="form-control" placeholder="ingrese apellido">
                          </div>

              <div class="form-group col-md">
                <label class="" for="">Nombres:</label>
                <input type="text" id="nombres" class="form-control" placeholder="ingrese nombres completos">
                          </div>
</div>
<div class="row">
              <div class="form-group col-md">
                <label for="">DNI:</label>
                <input type="text" id="dni" class="form-control" placeholder="ingrese DNI sin puntos..">
                          </div>
                                        <div class="form-group col-md">
                <label for="">Domicilio:</label>
                <input type="text" id="domicilio" class="form-control" placeholder="ingrese Domicilio">
                          </div>
</div>

<div class="row">
                <div class="form-group col-md">
                <label for="">Teléfono:</label>
                <input type="text" id="telefono" class="form-control" placeholder="ingrese Teléfono">
                          </div>
                                        <div class="form-group col-md">
                <label for="">Mail:</label>
                <input type="text" id="email" class="form-control" placeholder="ingrese Mail">
                          </div>

</div>
<div class="row justify-content-center">
<button type="button" class="btn btn-success" onclick="alta_cliente();" data-dismiss="modal">Guardar cliente</button>
     </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->





        </div>  
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
 <?php include 'footer.php' ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

 


  <!-- Scroll to Top Button-->

  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script src="controlador/clientes.js"></script>

</body>

</html>
      





















