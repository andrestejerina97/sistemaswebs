-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-07-2019 a las 02:21:54
-- Versión del servidor: 5.7.14
-- Versión de PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistema_lihue`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `idclientes` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `direccion` varchar(85) DEFAULT NULL,
  `dni` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`idclientes`, `nombre`, `apellido`, `direccion`, `dni`) VALUES
(1, 'general', NULL, 'desconocido', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `idcompras` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `dineroCompra` decimal(10,0) DEFAULT NULL,
  `usuarios_idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precios`
--

CREATE TABLE `precios` (
  `idprecios` int(11) NOT NULL,
  `PrecioDecompra` decimal(10,0) DEFAULT NULL,
  `PrecioVentaContado` decimal(10,0) DEFAULT NULL,
  `PrecioVentaTarjeta` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `precios`
--

INSERT INTO `precios` (`idprecios`, `PrecioDecompra`, `PrecioVentaContado`, `PrecioVentaTarjeta`) VALUES
(108, '270', '350', '450'),
(111, '450', '750', '940'),
(112, '300', '350', '400'),
(113, '300', '350', '400'),
(114, '300', '350', '400'),
(115, '300', '350', '400'),
(116, '300', '350', '400'),
(117, '300', '350', '400'),
(118, '300', '350', '400'),
(119, '300', '350', '400'),
(120, '300', '350', '400'),
(121, '300', '350', '400'),
(122, '300', '350', '400'),
(123, '300', '350', '400'),
(124, '300', '350', '400'),
(125, '300', '350', '400'),
(126, '232', '343', '2323'),
(127, '232', '343', '2323'),
(128, '300', '233', '2323'),
(129, '300', '233', '2323'),
(130, '300', '233', '2323'),
(131, '300', '233', '2323'),
(132, '300', '233', '2323'),
(133, '230', '450', '350'),
(134, '230', '500', '600'),
(135, '230', '500', '600');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idproductos` int(11) NOT NULL,
  `nombre` varchar(85) DEFAULT NULL,
  `descripcion` varchar(85) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `talle` varchar(45) DEFAULT NULL,
  `marca` varchar(45) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `ganancia` decimal(10,0) DEFAULT NULL,
  `porcentajeGanancia` decimal(10,0) DEFAULT NULL,
  `codigoProducto` varchar(45) NOT NULL,
  `rubros_idrubros` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idproductos`, `nombre`, `descripcion`, `color`, `talle`, `marca`, `cantidad`, `ganancia`, `porcentajeGanancia`, `codigoProducto`, `rubros_idrubros`) VALUES
(123, 'REMERA', '', '', '', '', 2, '80', '30', '00000138', 1),
(126, 'PANTALON JEANS TOLANO', 'THALASSO ELASTIZADO', '/', '', '/', 10, '300', '67', '141', 1),
(127, 'PANTALON', '', '', '', '', 1, '50', '17', '142', 1),
(128, 'PANTALON', '', '', '', '', 4, '50', '17', '143', 1),
(129, 'PANTALON', '', '', '', '', 4, '50', '17', '144', 1),
(130, 'PANTALON', '', '', '', '', 4, '50', '17', '145', 1),
(131, 'PANTALON', '', '', '', '', 4, '50', '17', '146', 1),
(132, 'PANTALON', '', '', '', '', 4, '50', '17', '147', 1),
(133, 'PANTALON', '', '', '', '', 4, '50', '17', '148', 1),
(134, 'PANTALON', '', '/', '', '/', 4, '50', '17', '149', 1),
(135, 'PANTALON', '', '', '', '', 4, '50', '17', '150', 1),
(136, 'PANTALON', '', '', '', '', 4, '50', '17', '151', 1),
(137, 'PANTALON', '', '', '', '', 4, '50', '17', '152', 1),
(138, 'PANTALON', '', '', '', '', 4, '50', '17', '153', 1),
(139, 'PANTALON', '', '', '', '', 4, '50', '17', '154', 1),
(140, 'PANTALON', '', '', '', '', 4, '50', '17', '155', 1),
(141, 'BUSO', '', '', '', '', 3, '111', '48', '156', 1),
(142, 'BUSO', '', '', '', '', 3, '111', '48', '157', 1),
(143, 'PANTALON', '', '', '', '', 4, '-67', '-22', '158', 1),
(144, 'PANTALON', '', '', '', '', 4, '-67', '-22', '159', 1),
(145, 'PANTALON', '', '', '', '', 4, '-67', '-22', '160', 1),
(146, 'PANTALON', '', '', '', '', 4, '-67', '-22', '161', 1),
(147, 'PANTALON', '', '', '', '', 4, '-67', '-22', '162', 1),
(148, 'JSAHDJA', '', '', '', '', 4, '220', '96', '163', 1),
(149, 'PRUEBA FINAÃ±', '', '', '', '', 3, '270', '117', '164', 1),
(150, 'PRUEBA FINAÃ±', '', '', '', '', 3, '270', '117', '165', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_has_compras`
--

CREATE TABLE `productos_has_compras` (
  `productos_idproductos` int(11) NOT NULL,
  `compras_idcompras` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_has_precios`
--

CREATE TABLE `productos_has_precios` (
  `productos_idproductos` int(11) NOT NULL,
  `precios_idprecios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos_has_precios`
--

INSERT INTO `productos_has_precios` (`productos_idproductos`, `precios_idprecios`) VALUES
(123, 108),
(126, 111),
(127, 112),
(128, 113),
(129, 114),
(130, 115),
(131, 116),
(132, 117),
(133, 118),
(134, 119),
(135, 120),
(136, 121),
(137, 122),
(138, 123),
(139, 124),
(140, 125),
(141, 126),
(142, 127),
(143, 128),
(144, 129),
(145, 130),
(146, 131),
(147, 132),
(148, 133),
(149, 134),
(150, 135);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_has_proveedores`
--

CREATE TABLE `productos_has_proveedores` (
  `productos_idproductos` int(11) NOT NULL,
  `proveedores_idproveedores` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `idproveedores` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores_has_compras`
--

CREATE TABLE `proveedores_has_compras` (
  `proveedores_idproveedores` int(11) NOT NULL,
  `compras_idcompras` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rubros`
--

CREATE TABLE `rubros` (
  `idrubros` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `fechaInicio` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rubros`
--

INSERT INTO `rubros` (`idrubros`, `nombre`, `fechaInicio`) VALUES
(1, 'ropa', '2018-12-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `talle_producto`
--

CREATE TABLE `talle_producto` (
  `id` int(11) NOT NULL,
  `talle` varchar(45) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `productos_idproductos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarjetas`
--

CREATE TABLE `tarjetas` (
  `idtarjetas` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tarjetas`
--

INSERT INTO `tarjetas` (`idtarjetas`, `nombre`, `numero`, `tipo`) VALUES
(1, 'generico', '123', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonos-proveedores`
--

CREATE TABLE `telefonos-proveedores` (
  `idtelefonos-proveedores` int(11) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `proveedores_idproveedores` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `tipo` varchar(65) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `nombre`, `tipo`, `usuario`, `password`) VALUES
(1, 'carlos', '1', 'admin', 'administrador'),
(2, 'ANDRES', 'admi', 'ANDY', 'andres');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `idventas` int(11) NOT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `pago` varchar(45) DEFAULT NULL,
  `fecha` date NOT NULL,
  `tarjetas_idtarjetas` int(11) NOT NULL,
  `clientes_idclientes` int(11) NOT NULL,
  `usuarios_idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas_detalles`
--

CREATE TABLE `ventas_detalles` (
  `ventas_detalles_idproductos` int(11) NOT NULL,
  `ventas_detalles_idventas` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `descuento` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`idclientes`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`idcompras`),
  ADD KEY `fk_compras_usuarios1_idx` (`usuarios_idusuarios`);

--
-- Indices de la tabla `precios`
--
ALTER TABLE `precios`
  ADD PRIMARY KEY (`idprecios`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idproductos`),
  ADD UNIQUE KEY `codigoProducto_UNIQUE` (`codigoProducto`),
  ADD KEY `fk_productos_rubros1_idx` (`rubros_idrubros`);

--
-- Indices de la tabla `productos_has_compras`
--
ALTER TABLE `productos_has_compras`
  ADD PRIMARY KEY (`productos_idproductos`,`compras_idcompras`),
  ADD KEY `fk_productos_has_compras_compras1_idx` (`compras_idcompras`),
  ADD KEY `fk_productos_has_compras_productos1_idx` (`productos_idproductos`);

--
-- Indices de la tabla `productos_has_precios`
--
ALTER TABLE `productos_has_precios`
  ADD PRIMARY KEY (`productos_idproductos`,`precios_idprecios`),
  ADD KEY `fk_productos_has_precios_precios1_idx` (`precios_idprecios`),
  ADD KEY `fk_productos_has_precios_productos1_idx` (`productos_idproductos`);

--
-- Indices de la tabla `productos_has_proveedores`
--
ALTER TABLE `productos_has_proveedores`
  ADD PRIMARY KEY (`productos_idproductos`,`proveedores_idproveedores`),
  ADD KEY `fk_productos_has_proveedores_proveedores1_idx` (`proveedores_idproveedores`),
  ADD KEY `fk_productos_has_proveedores_productos1_idx` (`productos_idproductos`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`idproveedores`);

--
-- Indices de la tabla `proveedores_has_compras`
--
ALTER TABLE `proveedores_has_compras`
  ADD PRIMARY KEY (`proveedores_idproveedores`,`compras_idcompras`),
  ADD KEY `fk_proveedores_has_compras_compras1_idx` (`compras_idcompras`),
  ADD KEY `fk_proveedores_has_compras_proveedores1_idx` (`proveedores_idproveedores`);

--
-- Indices de la tabla `rubros`
--
ALTER TABLE `rubros`
  ADD PRIMARY KEY (`idrubros`);

--
-- Indices de la tabla `talle_producto`
--
ALTER TABLE `talle_producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_talle_producto_productos1_idx` (`productos_idproductos`);

--
-- Indices de la tabla `tarjetas`
--
ALTER TABLE `tarjetas`
  ADD PRIMARY KEY (`idtarjetas`);

--
-- Indices de la tabla `telefonos-proveedores`
--
ALTER TABLE `telefonos-proveedores`
  ADD PRIMARY KEY (`idtelefonos-proveedores`,`proveedores_idproveedores`),
  ADD KEY `fk_telefonos-proveedores_proveedores1_idx` (`proveedores_idproveedores`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuarios`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`idventas`) USING BTREE,
  ADD KEY `fk_ventas_tarjetas1_idx` (`tarjetas_idtarjetas`),
  ADD KEY `fk_ventas_clientes1_idx` (`clientes_idclientes`),
  ADD KEY `fk_ventas_usuarios1_idx` (`usuarios_idusuarios`);

--
-- Indices de la tabla `ventas_detalles`
--
ALTER TABLE `ventas_detalles`
  ADD PRIMARY KEY (`ventas_detalles_idproductos`,`ventas_detalles_idventas`),
  ADD KEY `ventas_detalles_idventas` (`ventas_detalles_idventas`),
  ADD KEY `ventas_detalles_idventas_2` (`ventas_detalles_idventas`),
  ADD KEY `ventas_detalles_idproductos` (`ventas_detalles_idproductos`),
  ADD KEY `ventas_detalles_idventas_3` (`ventas_detalles_idventas`),
  ADD KEY `ventas_detalles_idventas_4` (`ventas_detalles_idventas`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `idclientes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `idcompras` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `precios`
--
ALTER TABLE `precios`
  MODIFY `idprecios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idproductos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `idproveedores` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `rubros`
--
ALTER TABLE `rubros`
  MODIFY `idrubros` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `talle_producto`
--
ALTER TABLE `talle_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tarjetas`
--
ALTER TABLE `tarjetas`
  MODIFY `idtarjetas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `telefonos-proveedores`
--
ALTER TABLE `telefonos-proveedores`
  MODIFY `idtelefonos-proveedores` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `idventas` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `fk_compras_usuarios1` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fk_productos_rubros1` FOREIGN KEY (`rubros_idrubros`) REFERENCES `rubros` (`idrubros`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos_has_compras`
--
ALTER TABLE `productos_has_compras`
  ADD CONSTRAINT `fk_productos_has_compras_productos1` FOREIGN KEY (`productos_idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos_has_precios`
--
ALTER TABLE `productos_has_precios`
  ADD CONSTRAINT `fk_productos_has_precios_precios1` FOREIGN KEY (`precios_idprecios`) REFERENCES `precios` (`idprecios`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_productos_has_precios_productos1` FOREIGN KEY (`productos_idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos_has_proveedores`
--
ALTER TABLE `productos_has_proveedores`
  ADD CONSTRAINT `fk_productos_has_proveedores_productos1` FOREIGN KEY (`productos_idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_productos_has_proveedores_proveedores1` FOREIGN KEY (`proveedores_idproveedores`) REFERENCES `proveedores` (`idproveedores`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proveedores_has_compras`
--
ALTER TABLE `proveedores_has_compras`
  ADD CONSTRAINT `fk_proveedores_has_compras_proveedores1` FOREIGN KEY (`proveedores_idproveedores`) REFERENCES `proveedores` (`idproveedores`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `talle_producto`
--
ALTER TABLE `talle_producto`
  ADD CONSTRAINT `fk_talle_producto_productos1` FOREIGN KEY (`productos_idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `telefonos-proveedores`
--
ALTER TABLE `telefonos-proveedores`
  ADD CONSTRAINT `fk_telefonos-proveedores_proveedores1` FOREIGN KEY (`proveedores_idproveedores`) REFERENCES `proveedores` (`idproveedores`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_ventas_clientes1` FOREIGN KEY (`clientes_idclientes`) REFERENCES `clientes` (`idclientes`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ventas_tarjetas1` FOREIGN KEY (`tarjetas_idtarjetas`) REFERENCES `tarjetas` (`idtarjetas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ventas_usuarios1` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ventas_detalles`
--
ALTER TABLE `ventas_detalles`
  ADD CONSTRAINT `claveforanea1` FOREIGN KEY (`ventas_detalles_idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `claveforanea2` FOREIGN KEY (`ventas_detalles_idventas`) REFERENCES `ventas` (`idventas`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
