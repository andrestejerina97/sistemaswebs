<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Lihue Novedades|Inicio</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <?php include"class_lib/links_two.php"; ?>


</head>

<body class="hold-transition skin-blue sidebar-mini" onLoad="obtener_codigo();lista_articulos();">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

<?php include"class_lib/nav_header.php"; ?>
  </header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar"> 
    <!-- /.sidebar -->
    <?php include"class_lib/sidebar.php"; ?>
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Lihue novedades
        <small>Adminstrá tu negocio de la mejor manera</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>nivel</a></li>
        <li class="active">aquí</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

          <!-- Your Page Content Here -->
          <div class='row'>
           <div class='col-md-12'>
             <div class='nav-tabs-custom'>
                  <ul class="nav nav-tabs pull-right">
                  <li><a href="#cambios" data-toggle="tab">Cambios</a></li>
                  <li><a href="#bajas" data-toggle="tab">Baja</a></li>
                  <li class="active"><a href="#altas" data-toggle="tab">Alta</a></li>

                  <li class="pull-left header"><i class="fa fa-file-text"></i> Administración de productos.</li>
                </ul>
               <div class="tab-content">
                  <div class="tab-pane active" id="altas">
                    <form class="form-horizontal">

<div class="form-group">
  

                    

                    <label for="nombre" class="col-sm-2 control-label">Nombre:</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control" id='nombre' tabindex="1" placeholder='Nombre del articulo...'>
                    </div>
                    </div>
                    <div class='form-group'>
                    <label for="descripcion" class="col-sm-2 control-label">Descripción:</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control" id='descripcion' placeholder='Descripción del articulo'>
                    </div>
                    </div>

                    <div class='form-group'>
                    <label for="color" class="col-sm-2 control-label">Color:</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control" id='color' placeholder='Color del articulo...'>
                    </div>
                    </div>

                      <div class='form-group'>
                        <label for="talle" class="col-sm-2 control-label">Talle:</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control" id='talle' placeholder='Talle del producto'>
                        </div>
                      </div>


                    <div class='form-group'>
                    <label for="marca" class="col-sm-2 control-label">Marca:</label>
                    <div class="col-sm-2">
                    <input type="text" class="form-control cantidades" id='marca' placeholder='Marca...'>
                    </div>
                    </div>

                    <div class='form-group'>
                    <label for="cantidad" class="col-sm-2 control-label">Cantidad:</label>
                    <div class="col-sm-2">
                    <input type="text" class="form-control cantidades" tabindex="2" id='cantidad' placeholder='Cantidad disponible...'
                     data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'">
                    </div>
                    </div>

                 
                     <div class='form-group'>
                    <label for="costo" class="col-sm-2 control-label">Costo original:</label>
                    <div class="col-sm-2">
                    <input type="text"  tabindex="3" class="form-control cantidades" id='costo' placeholder='Costo de compra'
                     data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'">
                    </div>
                    </div>
                     <div class='form-group'>
                    <label for="precioCont" class="col-sm-2 control-label">Precio de contado:</label>
                    <div class="col-sm-2">
                    <input type="text" class="form-control cantidades" tabindex="4" id='precioContado' placeholder='Precio de contado al público...'
                     data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'">
                    </div>
                    </div>
                    <div class='form-group'>
                    <label for="PrecioTarjeta" class="col-sm-2 control-label">Precio Tarjeta:</label>
                    <div class="col-sm-2">
                    <input type="text" class="form-control cantidades" tabindex="5" id='precioTarjeta' placeholder='Precio con Tarjeta al público...'
                     data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'">
                    </div>
                    </div>
                     <div class="form-row">
                    <div class='form-group'>
                     
                    <label for="Codigo  " class="col-sm-2 control-label">Código:</label>
                    <div class="col-sm-2">
                    <input type="text" class="form-control cantidades" id='codigo'  
                     >
                     
                        <canvas id="barcode" class="col-sm-2" ></canvas>
</div>
                                  </div>
                    </div>

                   


                    <br>
                    <div class="btn-group">
                    <button type='button' tabindex="6" class='btn btn-raised btn-primary btn-lg' onclick='alta_articulo();' id='btn-altas'><i class='fa fa-check-circle'></i> Registrar el articulo.</button>
                    <button type='button' class='btn btn-danger btn-raised btn-lg' onclick='cancela_alta_articulo();' id='btn-alta-cancela'><i class='fa fa-times'></i> Cancelar.</button>
                    </div>
                    </form>
                  </div><!-- /.tab-pane -->

                  <div class="tab-pane" id="bajas">
                    <form class='form-horizontal' onkeypress="return anular(event)">
                    <div class='form-group'>
                    <label for="codigo_busqueda" class="col-sm-2 control-label">Codigo:</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control" id='codigo_busqueda' onchange="busca_articulo();" placeholder='Codigo del articulo...'>
                    </div>
                    </div>
                        
                    <div id='info_articulo'></div>
                    <br>
                    <div class="btn-group">
                    <button type='button' class='btn btn-primary btn-lg' onclick='busca_articulo();' id='btn-buscar'><i class='fa  fa-search'></i> Buscar...</button>
                    <button type='button' class='btn btn-success btn-lg' onclick='elimina_articulo();' id='btn-procede-baja' disabled><i class='fa   fa-times'></i> Eliminar...</button>
                    <button type='button' class='btn btn-danger btn-lg' onclick='cancela_eliminacion();' id='btn-cancela-baja' disabled><i class='fa  fa-recycle'></i> Cancelar...</button>
                    </div>

                    </form>
                  </div><!-- /.tab-pane -->


                  <div class="tab-pane" id="cambios">
                    <form class='form-horizontal' onkeypress="return anular(event)">
                    <div class='form-group'>
                    <label for="codigo_busqueda_cambio" class="col-sm-2 control-label">Codigo:</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control" id='codigo_busqueda_cambio'  placeholder='Codigo del articulo...'>
                    </div>
                    </div>

                    <div id='info_articulo_cambio'></div>
                    <br>
                    <div class="btn-group">
                    <button type='button' class='btn btn-primary btn-lg' onclick='busca_articulo_cambio();' id='btn-buscar-cambio'><i class='fa fa-search'></i> Buscar...</button>
                    <button type='button' class='btn btn-success btn-lg' onclick='procede_cambio();' id='btn-procede-cambio' disabled><i class='fa fa-check-circle'></i> Actualizar...</button>
                    <button type='button' class='btn btn-danger btn-lg' onclick='cancela_cambios();' id='btn-cancela-cambio' disabled><i class='fa fa-recycle'></i> Cancelar...</button>
                    </div>

                    </form>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->

             </div>
           </div>

           <div class='col-md-12'>
           <div id='lista_articulos'>
           </div>
           </div>
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <?php include"class_lib/main_fotter.php"; ?> 
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <?php include"class_lib/control_sidebar.php"; ?> 
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->








<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->

     <?php include "./class_lib/scripts.php"; ?>
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/noty/packaged/jquery.noty.packaged.min.js"></script>
    <script src="plugins/number/jquery.inputmask.bundle.js"></script>
    <script src="plugins/barcode-jquery-master/jquery-barcode.min.js"></script>
     <script src="plugins/barcode-jquery-master/JsBarcode.all.min.js"></script>
    <script src="plugins/noty/packaged/jquery.noty.packaged.min.js"></script>
    <script src="plugins/uploadify/jquery.uploadify.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <script src="plugins/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="plugins/datepicker/locales/bootstrap-datepicker.es.min.js"></script>
    <script src="dist/js/source_articles.js"></script>
    <script>
      $(document).ready(function(){
      $(".cantidades").inputmask();
      });

      $("#fecha_caducidad").datepicker({
        language: "es",
        format: "yyyy-mm-dd"
      });
    </script>
</body>
</html>