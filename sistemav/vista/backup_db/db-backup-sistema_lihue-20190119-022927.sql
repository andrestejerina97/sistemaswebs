CREATE DATABASE IF NOT EXISTS sistema_lihue;

USE sistema_lihue;

DROP TABLE IF EXISTS clientes;

CREATE TABLE `clientes` (
  `idclientes` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `direccion` varchar(85) DEFAULT NULL,
  PRIMARY KEY (`idclientes`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO clientes VALUES("1","general","","desconocido");



DROP TABLE IF EXISTS compras;

CREATE TABLE `compras` (
  `idcompras` int(11) NOT NULL AUTO_INCREMENT,
  `fechaCompra` date DEFAULT NULL,
  `dineroCompra` decimal(10,0) DEFAULT NULL,
  `usuarios_idusuarios` int(11) NOT NULL,
  `fecha_idfecha` int(11) NOT NULL,
  PRIMARY KEY (`idcompras`,`fecha_idfecha`),
  KEY `fk_compras_usuarios1_idx` (`usuarios_idusuarios`),
  KEY `fk_compras_fecha1_idx` (`fecha_idfecha`),
  CONSTRAINT `fk_compras_fecha1` FOREIGN KEY (`fecha_idfecha`) REFERENCES `fecha` (`idfecha`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_compras_usuarios1` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS fecha;

CREATE TABLE `fecha` (
  `idfecha` int(11) NOT NULL AUTO_INCREMENT,
  `dia` int(11) DEFAULT NULL,
  `mes` int(11) DEFAULT NULL,
  `año` int(11) DEFAULT NULL,
  PRIMARY KEY (`idfecha`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO fecha VALUES("1","24","3","2019");



DROP TABLE IF EXISTS precios;

CREATE TABLE `precios` (
  `idprecios` int(11) NOT NULL AUTO_INCREMENT,
  `PrecioDecompra` decimal(10,0) DEFAULT NULL,
  `PrecioVentaContado` decimal(10,0) DEFAULT NULL,
  `PrecioVentaTarjeta` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`idprecios`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;

INSERT INTO precios VALUES("108","270","350","450");
INSERT INTO precios VALUES("111","450","750","940");
INSERT INTO precios VALUES("112","300","350","400");
INSERT INTO precios VALUES("113","300","350","400");
INSERT INTO precios VALUES("114","300","350","400");
INSERT INTO precios VALUES("115","300","350","400");
INSERT INTO precios VALUES("116","300","350","400");
INSERT INTO precios VALUES("117","300","350","400");
INSERT INTO precios VALUES("118","300","350","400");
INSERT INTO precios VALUES("119","300","350","400");
INSERT INTO precios VALUES("120","300","350","400");
INSERT INTO precios VALUES("121","300","350","400");
INSERT INTO precios VALUES("122","300","350","400");
INSERT INTO precios VALUES("123","300","350","400");
INSERT INTO precios VALUES("124","300","350","400");
INSERT INTO precios VALUES("125","300","350","400");
INSERT INTO precios VALUES("126","232","343","2323");
INSERT INTO precios VALUES("127","232","343","2323");
INSERT INTO precios VALUES("128","300","233","2323");
INSERT INTO precios VALUES("129","300","233","2323");
INSERT INTO precios VALUES("130","300","233","2323");
INSERT INTO precios VALUES("131","300","233","2323");
INSERT INTO precios VALUES("132","300","233","2323");



DROP TABLE IF EXISTS productos;

CREATE TABLE `productos` (
  `idproductos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(85) DEFAULT NULL,
  `descripcion` varchar(85) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `talle` varchar(45) DEFAULT NULL,
  `marca` varchar(45) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `ganancia` decimal(10,0) DEFAULT NULL,
  `porcentajeGanancia` decimal(10,0) DEFAULT NULL,
  `codigoProducto` varchar(45) NOT NULL,
  `rubros_idrubros` int(11) NOT NULL,
  PRIMARY KEY (`idproductos`),
  UNIQUE KEY `codigoProducto_UNIQUE` (`codigoProducto`),
  KEY `fk_productos_rubros1_idx` (`rubros_idrubros`),
  CONSTRAINT `fk_productos_rubros1` FOREIGN KEY (`rubros_idrubros`) REFERENCES `rubros` (`idrubros`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

INSERT INTO productos VALUES("123","REMERA","","","","","2","80","30","00000138","1");
INSERT INTO productos VALUES("126","PANTALON JEANS TOLANO","THALASSO ELASTIZADO","/","","/","3","300","67","141","1");
INSERT INTO productos VALUES("127","PANTALON","","","","","3","50","17","142","1");
INSERT INTO productos VALUES("128","PANTALON","","","","","4","50","17","143","1");
INSERT INTO productos VALUES("129","PANTALON","","","","","4","50","17","144","1");
INSERT INTO productos VALUES("130","PANTALON","","","","","4","50","17","145","1");
INSERT INTO productos VALUES("131","PANTALON","","","","","4","50","17","146","1");
INSERT INTO productos VALUES("132","PANTALON","","","","","4","50","17","147","1");
INSERT INTO productos VALUES("133","PANTALON","","","","","4","50","17","148","1");
INSERT INTO productos VALUES("134","PANTALON","","","","","4","50","17","149","1");
INSERT INTO productos VALUES("135","PANTALON","","","","","4","50","17","150","1");
INSERT INTO productos VALUES("136","PANTALON","","","","","4","50","17","151","1");
INSERT INTO productos VALUES("137","PANTALON","","","","","4","50","17","152","1");
INSERT INTO productos VALUES("138","PANTALON","","","","","4","50","17","153","1");
INSERT INTO productos VALUES("139","PANTALON","","","","","4","50","17","154","1");
INSERT INTO productos VALUES("140","PANTALON","","","","","4","50","17","155","1");
INSERT INTO productos VALUES("141","BUSO","","","","","3","111","48","156","1");
INSERT INTO productos VALUES("142","BUSO","","","","","3","111","48","157","1");
INSERT INTO productos VALUES("143","PANTALON","","","","","4","-67","-22","158","1");
INSERT INTO productos VALUES("144","PANTALON","","","","","4","-67","-22","159","1");
INSERT INTO productos VALUES("145","PANTALON","","","","","4","-67","-22","160","1");
INSERT INTO productos VALUES("146","PANTALON","","","","","4","-67","-22","161","1");
INSERT INTO productos VALUES("147","PANTALON","","","","","4","-67","-22","162","1");



DROP TABLE IF EXISTS productos_has_compras;

CREATE TABLE `productos_has_compras` (
  `productos_idproductos` int(11) NOT NULL,
  `compras_idcompras` int(11) NOT NULL,
  PRIMARY KEY (`productos_idproductos`,`compras_idcompras`),
  KEY `fk_productos_has_compras_compras1_idx` (`compras_idcompras`),
  KEY `fk_productos_has_compras_productos1_idx` (`productos_idproductos`),
  CONSTRAINT `fk_productos_has_compras_compras1` FOREIGN KEY (`compras_idcompras`) REFERENCES `compras` (`idcompras`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_has_compras_productos1` FOREIGN KEY (`productos_idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS productos_has_precios;

CREATE TABLE `productos_has_precios` (
  `productos_idproductos` int(11) NOT NULL,
  `precios_idprecios` int(11) NOT NULL,
  PRIMARY KEY (`productos_idproductos`,`precios_idprecios`),
  KEY `fk_productos_has_precios_precios1_idx` (`precios_idprecios`),
  KEY `fk_productos_has_precios_productos1_idx` (`productos_idproductos`),
  CONSTRAINT `fk_productos_has_precios_precios1` FOREIGN KEY (`precios_idprecios`) REFERENCES `precios` (`idprecios`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_productos_has_precios_productos1` FOREIGN KEY (`productos_idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO productos_has_precios VALUES("123","108");
INSERT INTO productos_has_precios VALUES("126","111");
INSERT INTO productos_has_precios VALUES("127","112");
INSERT INTO productos_has_precios VALUES("128","113");
INSERT INTO productos_has_precios VALUES("129","114");
INSERT INTO productos_has_precios VALUES("130","115");
INSERT INTO productos_has_precios VALUES("131","116");
INSERT INTO productos_has_precios VALUES("132","117");
INSERT INTO productos_has_precios VALUES("133","118");
INSERT INTO productos_has_precios VALUES("134","119");
INSERT INTO productos_has_precios VALUES("135","120");
INSERT INTO productos_has_precios VALUES("136","121");
INSERT INTO productos_has_precios VALUES("137","122");
INSERT INTO productos_has_precios VALUES("138","123");
INSERT INTO productos_has_precios VALUES("139","124");
INSERT INTO productos_has_precios VALUES("140","125");
INSERT INTO productos_has_precios VALUES("141","126");
INSERT INTO productos_has_precios VALUES("142","127");
INSERT INTO productos_has_precios VALUES("143","128");
INSERT INTO productos_has_precios VALUES("144","129");
INSERT INTO productos_has_precios VALUES("145","130");
INSERT INTO productos_has_precios VALUES("146","131");
INSERT INTO productos_has_precios VALUES("147","132");



DROP TABLE IF EXISTS productos_has_proveedores;

CREATE TABLE `productos_has_proveedores` (
  `productos_idproductos` int(11) NOT NULL,
  `proveedores_idproveedores` int(11) NOT NULL,
  PRIMARY KEY (`productos_idproductos`,`proveedores_idproveedores`),
  KEY `fk_productos_has_proveedores_proveedores1_idx` (`proveedores_idproveedores`),
  KEY `fk_productos_has_proveedores_productos1_idx` (`productos_idproductos`),
  CONSTRAINT `fk_productos_has_proveedores_productos1` FOREIGN KEY (`productos_idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_productos_has_proveedores_proveedores1` FOREIGN KEY (`proveedores_idproveedores`) REFERENCES `proveedores` (`idproveedores`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS proveedores;

CREATE TABLE `proveedores` (
  `idproveedores` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idproveedores`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS proveedores_has_compras;

CREATE TABLE `proveedores_has_compras` (
  `proveedores_idproveedores` int(11) NOT NULL,
  `compras_idcompras` int(11) NOT NULL,
  `compras_fecha_idfecha` int(11) NOT NULL,
  PRIMARY KEY (`proveedores_idproveedores`,`compras_idcompras`,`compras_fecha_idfecha`),
  KEY `fk_proveedores_has_compras_compras1_idx` (`compras_idcompras`,`compras_fecha_idfecha`),
  KEY `fk_proveedores_has_compras_proveedores1_idx` (`proveedores_idproveedores`),
  CONSTRAINT `fk_proveedores_has_compras_compras1` FOREIGN KEY (`compras_idcompras`, `compras_fecha_idfecha`) REFERENCES `compras` (`idcompras`, `fecha_idfecha`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proveedores_has_compras_proveedores1` FOREIGN KEY (`proveedores_idproveedores`) REFERENCES `proveedores` (`idproveedores`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS rubros;

CREATE TABLE `rubros` (
  `idrubros` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `fechaInicio` date DEFAULT NULL,
  PRIMARY KEY (`idrubros`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO rubros VALUES("1","ropa","2018-12-03");



DROP TABLE IF EXISTS talle_producto;

CREATE TABLE `talle_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `talle` varchar(45) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `productos_idproductos` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_talle_producto_productos1_idx` (`productos_idproductos`),
  CONSTRAINT `fk_talle_producto_productos1` FOREIGN KEY (`productos_idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS tarjetas;

CREATE TABLE `tarjetas` (
  `idtarjetas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtarjetas`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO tarjetas VALUES("1","generico","123","");



DROP TABLE IF EXISTS telefonos-proveedores;

;




DROP TABLE IF EXISTS usuarios;

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `tipo` varchar(65) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`idusuarios`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO usuarios VALUES("1","carlos","administrador","admin","administrador");
INSERT INTO usuarios VALUES("2","ANDRES","admi","ANDY","andres");



DROP TABLE IF EXISTS ventas;

CREATE TABLE `ventas` (
  `idventas` int(11) NOT NULL AUTO_INCREMENT,
  `total` decimal(20,0) DEFAULT NULL,
  `pago` varchar(45) DEFAULT NULL,
  `tarjetas_idtarjetas` int(11) NOT NULL,
  `clientes_idclientes` int(11) NOT NULL,
  `usuarios_idusuarios` int(11) NOT NULL,
  `fecha_idfecha` int(11) NOT NULL,
  PRIMARY KEY (`idventas`,`fecha_idfecha`),
  KEY `fk_ventas_tarjetas1_idx` (`tarjetas_idtarjetas`),
  KEY `fk_ventas_clientes1_idx` (`clientes_idclientes`),
  KEY `fk_ventas_usuarios1_idx` (`usuarios_idusuarios`),
  KEY `fk_ventas_fecha1_idx` (`fecha_idfecha`),
  CONSTRAINT `fk_ventas_clientes1` FOREIGN KEY (`clientes_idclientes`) REFERENCES `clientes` (`idclientes`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ventas_fecha1` FOREIGN KEY (`fecha_idfecha`) REFERENCES `fecha` (`idfecha`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ventas_tarjetas1` FOREIGN KEY (`tarjetas_idtarjetas`) REFERENCES `tarjetas` (`idtarjetas`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ventas_usuarios1` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

INSERT INTO ventas VALUES("27","1100","credito","1","1","1","1");



DROP TABLE IF EXISTS ventas_detalles;

CREATE TABLE `ventas_detalles` (
  `ventas_detalles_idproductos` int(11) NOT NULL,
  `ventas_detalles_idventas` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `descuento` decimal(10,0) NOT NULL,
  PRIMARY KEY (`ventas_detalles_idproductos`,`ventas_detalles_idventas`),
  KEY `ventas_detalles_idventas` (`ventas_detalles_idventas`),
  KEY `ventas_detalles_idventas_2` (`ventas_detalles_idventas`),
  KEY `ventas_detalles_idproductos` (`ventas_detalles_idproductos`),
  KEY `ventas_detalles_idventas_3` (`ventas_detalles_idventas`),
  KEY `ventas_detalles_idventas_4` (`ventas_detalles_idventas`),
  CONSTRAINT `claveforanea1` FOREIGN KEY (`ventas_detalles_idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `claveforanea2` FOREIGN KEY (`ventas_detalles_idventas`) REFERENCES `ventas` (`idventas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO ventas_detalles VALUES("126","27","1","0");
INSERT INTO ventas_detalles VALUES("127","27","1","0");



