   <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Carlos Tejerina</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->

     
 <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="treeview">
              <a href="#" ><i class="fa fa-users"></i> <span>Ventas</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="punto_venta.php"><i class="fa fa-object-group"></i>Insertar.</a></li>
                <li><a href="punto_venta.php"><i class="fa fa-film"></i> Modificar.</a></li>
                <li><a href="punto_venta.php"><i class="fa fa-film"></i> Listar.</a></li>
              </ul>
            </li>

 <li class="treeview">
              <a href="#" ><i class="fa fa-users"></i> <span>Productos</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="abc_articulos.php"><i class="fa fa-object-group"></i> Agregar.</a></li>
                <li><a href=""><i class="fa fa-film"></i> Modificar.</a></li>
                <li><a href="vista/listar_articulos.php"><i class="fa fa-film"></i> Listar.</a></li>
              </ul>
            </li>

             <li class="treeview">
              <a href="#" ><i class="fa fa-users"></i> <span>Compras</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="abc_articulos.php"><i class="fa fa-object-group"></i> Registar.</a></li>
                <li><a href=""><i class="fa fa-film"></i> Modificar.</a></li>
                <li><a href=""><i class="fa fa-film"></i> Listar.</a></li>
              </ul>
            </li>
                         <li class="treeview">
              <a href="#" ><i class="fa fa-users"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="rev_ventas.php"><i class="fa fa-object-group"></i> Ventas.</a></li>
                <li><a href=""><i class="fa fa-film"></i> Modificar.</a></li>
                <li><a href=""><i class="fa fa-film"></i> Listar.</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#" ><i class="fa fa-users"></i> <span>Configuración</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="util_usr.php"><i class="fa fa-object-group"></i>Usuarios.</a></li>
                <li><a href="util_backup.php"><i class="fa fa-film"></i>Respaldo.</a></li>
                <li><a href=""><i class="fa fa-film"></i> Listar.</a></li>
              </ul>
            </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>