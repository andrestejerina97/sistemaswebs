<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Starter</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <?php include"class_lib/links_two.php"; ?>


</head>

<body class="hold-transition skin-blue sidebar-mini" onload='pone_foco_ini();'>
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

<?php include"class_lib/nav_header.php"; ?>
  </header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar"> 
    <!-- /.sidebar -->
    <?php include"class_lib/sidebar.php"; ?>
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Lihue novedades
        <small>Adminstrá tu negocio de la mejor manera</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>nivel</a></li>
        <li class="active">aquí</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

          <!-- Your Page Content Here -->
   <div class='row'>
          <div class='col-md-4'>
          <div class='box box-primary'>
          <div class='box-header with-border'><h3 class='box-title'>Ingresa el Codigo del Articulo:</h3></div>
          <div class='box-body'>
          <div class='input-group'>
          <div class='input-group-btn'>
          <button type='button' class='btn btn-success' onclick='buscar_articulo();'><i class='fa fa-search'></i></button>
          </div>
          <input type='text' id='codigo' class='form-control' placeholder='Codigo...' style="font-size:20px; text-align:center; color:blue; font-weight: bold;">

          </div>
          <br>
          
          
          <button class='btn btn-danger btn-lg'  onclick='cancela_venta();' id='btn-cancel' ><i class='fa fa-times'></i> Cancelar</button>
          </div>

          </div>
          </div>

          

          <div class="col-md-8">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><div id='totales'></div></h3>
                  <p>Total</p>
                </div>
                <div class="icon">
                  <i class="fa fa-shopping-cart"></i>
                </div>
                <a href="#" class="small-box-footer">
                  <div id='num_ticket'></div>
                </a>
                <a href="#" class="small-box-footer">
                  <div id='total_articulos'></div>
                </a>
                <a href="#" class="small-box-footer">
                  <div id='tipo_de_venta'>Venta de Contado.</div>
                </a>
              </div>
              <div class='btn-group'>
              <button class='btn  btn-success btn-lg'id='btn-procesa' onclick='prepara_venta();'><i class='fa fa-money'></i> Pagar</button>
             
              
              </div>
            </div><!-- ./col -->


          </div>

          <div class='row'>
          <div class='col-md-12'>
          <div class='box box-primary'>
          <div class='box-header'>
          <h3 class='box-title'>Lista de Articulos</h3>
          </div>
          <div class='box-body table-responsive'>
          <table id='tabla_articulos' class='table table-hover'>
           <thead>
           <tr>
           <th class='center'>Codigo</th><th class='center'>Nombre</th><th class='center' >Cantidad</th><th class='center' >Precio U.</th><th class='center'>Monto</th><th class='center'>Operacion</th>
           </tr>
           </thead>
           <tbody>

           </tbody>
          </table>
          </div>
          </div>
          </div>
          </div>
        </section><!-- /.content -->
         </div><!-- /.content-wrapper -->


           <div class="modal fade" id ="modal_tabla_clientes" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Selecciona el Cliente:</h4>
          </div>
          <div class="modal-body">
            <div id='lista_clientes'></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal fade" id ="modal_prepara_venta" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">RESUMEN</h4>
          </div>
          <div class="modal-body">

          <div class='input-group input-group-lg'>
          <span class='input-group-addon bg-blue'><b>Total de la Venta:</b></span>
          <input type='text' id='total_de_venta' class='form-control' style="font-size:30px; text-align:center; color:red; font-weight: bold;" disabled>
          </div>
          <br>
          <div class='input-group input-group-lg'>
          <span class='input-group-addon bg-blue'><b>Su Pago:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
          <input type='text' id='paga_con' class='form-control cantidades' style="font-size:30px; text-align:center; color:red; font-weight: bold;" onkeyup="calcula_cambio();"
          data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'">
          </div>
          <br>
          <div class='input-group input-group-lg'>
          <span class='input-group-addon bg-blue'><b>Cambio:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
          <input type='text' id='el_cambio' class='form-control' style="font-size:30px; text-align:center; color:red; font-weight: bold;" disabled>
          </div>

          </div>
          <div class="modal-footer">
              <button class='btn btn-success btn-lg print_ticket' id='btn-termina' onclick='' disabled><i class='fa fa-print'></i> Ticket</button>
              <button class='btn btn-success btn-lg' id='btn-termina' onclick='procesa_venta();'><i class='fa fa-shopping-cart'></i> Contado</button>
              <button class='btn  btn-warning btn-lg' onclick='procesa_credito();' id='btn_cre'><i class='fa fa-user-plus'></i>Crédito</button>
              <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><i class='fa fa-times'></i> Cerrar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
 <div class="modal fade" id ="modal_cambio_producto" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Modificación de Producto a vender:</h4>
          </div>
          <div class="modal-body">
          <div class='input-group'>
          <span class='input-group-addon bg-blue'><b>Producto:</b></span>
          <input type='text' id='nombre_cambio' class='form-control' onkeyup="busca();" placeholder='Descripcion del articulo...'>
          
          <br>
            
          </div>
          <div class='input-group'>
          <span class='input-group-addon bg-blue'><b>Precio Final:</b></span>
          <input type='text' id='precio_cambio' class='form-control'" placeholder='Descripcion del articulo...'>
          
          <br>
            
          </div>
          <div class='input-group'>
          <span class='input-group-addon bg-blue'><b>Cantidad:</b></span>
          <input type='text' id='cantidad_cambio' class='form-control'  placeholder='Descripcion del articulo...'>
          <br>
          </div>
          <br>
          
          </div>
          
                    <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="guardar_cambio();" data-dismiss="modal">Guardar</button>
                    
            <button type="button" class="btn btn-default" onclick="cancela_cambio();" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal fade" id ="modal_busqueda_arts" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Busqueda de Articulos:</h4>
          </div>
          <div class="modal-body">
          <div class='input-group'>
          <span class='input-group-addon bg-blue'><b>Articulo:</b></span>
          <input type='text' id='articulo_buscar' class='form-control' onkeyup="busca();" placeholder='Descripcion del articulo...'>
          
          <br>
            <div id='lista_articulos'></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

      <input type='hidden' id='idcliente_credito' value="">
      <input type='hidden' id='total_venta' value="">

      <div id='impresion_de_ticket' class='print'></div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <?php include"class_lib/main_fotter.php"; ?> 
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <?php include"class_lib/control_sidebar.php"; ?> 
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->








<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->

     <?php include "./class_lib/scripts.php"; ?>
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/noty/packaged/jquery.noty.packaged.min.js"></script>
    <script src="plugins/number/jquery.inputmask.bundle.js"></script>
    <script src="plugins/noty/packaged/jquery.noty.packaged.min.js"></script>
    <script src="plugins/uploadify/jquery.uploadify.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <script src="plugins/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="plugins/datepicker/locales/bootstrap-datepicker.es.min.js"></script>
    <script src="dist/js/source_point_sales.js"></script>
    <script>
      $(document).ready(function(){
      $(".cantidades").inputmask();
      });

      $("#fecha_caducidad").datepicker({
        language: "es",
        format: "yyyy-mm-dd"
      });
    </script>
</body>
</html>