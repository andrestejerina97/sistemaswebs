/***********************************************************/
 function pone_provs(){
         $(document).ready(function() {
          $.ajax({
          beforeSend: function(){
            $("#pone_provs").html("Recuperando lista de proveedores...");
           },
          url: 'pone_provs.php',
          type: 'POST',
          data: null,
          success: function(x){
            $("#pone_provs").html(x);
            $(".select2").select2();
           },
           error: function(jqXHR,estado,error){
           }
           });
          });
     }
/************************************************************/
function pone_lineas(){
        $( document ).ready(function() {
          $.ajax({
          beforeSend: function(){
            $("#pone_lineas").html("Recuperando lista de lineas...");
           },
          url: 'pone_lineas.php',
          type: 'POST',
          data: null,
          success: function(x){
            $("#pone_lineas").html(x);
            $(".select2").select2();
           },
           error: function(jqXHR,estado,error){
           }
           });
          });
         }
/***************************************************************/
function pone_grupos(){
         $(document).ready(function() {
          $.ajax({
          beforeSend: function(){
            $("#pone_grupos").html("Recuperando lista de grupos...");
           },
          url: 'pone_grupos.php',
          type: 'POST',
          data: 'linea='+$("#linea").val(),
          success: function(x){
            $("#pone_grupos").html(x);
            $(".select2").select2();
           },
           error: function(jqXHR,estado,error){
           }
           });
          });
         }
/***************************************************************/
function generar_codigo_de_barras(dato){
  
  var elem=document.getElementById("barcode");
//  image.src = elem.toDataURL("image/png");
  //alert("wdw");
  //return image;

JsBarcode(elem,dato);
var d=elem.toDataURL("image/png");

//d=d.replace("image/jpeg", "image/octet-stream");
//document.location.href =d;

$.ajax({
        url: "generar_codigo_barras.php",
        method: 'post',
        data: {imagen:d,codigo:dato}
    }).done(function(retorno) {
        //alert(retorno);
        //$( '#imagen' ).show();
    });

//var d= $("#barcode").toDataURL("image/png");

//$(document).location.href= image ; 
//);
//});
}

function generar_pdf(dato){
  
 $.ajax({
        url: "generar_pdf.php",
        method: 'post',
        data: {codigo:dato}
    }).done(function(retorno) {
        //alert(retorno);
        //$( '#imagen' ).show();
    });
//);
//});
}


/***************************************************************/
function alta_articulo(){
         
          var nombre=$("#nombre").val();
          var descripcion=$("#descripcion").val();
          var color=$("#color").val();
          var talle=$("#talle").val();
          var marca=$("#marca").val();
          var cantidad=$("#cantidad").val();
          var costo=$("#costo").val();
          var precioContado=$("#precioContado").val();
          var precioTarjeta=$("#precioTarjeta").val();
          var codigo=$("#codigo").val();
          var ganancia= (precioContado - costo);
          var pganancia= ((ganancia * 100)/ costo); 
          if(nombre==""||cantidad==""||costo==""||precioContado==""||cantidad=="0"){

            var n = noty({
                 text: "Completa la informacion del articulo...!",
                 theme: 'relax',
                 layout: 'center',
                 type: 'information',
                 timeout: 2000,
                 });
            return false;
            }
             $(document).ready(function() {
            $.ajax({
              beforeSend: function(){
 
               },
              url: 'save_articulo.php',
              type: 'POST',
              dataType: "json",
              data: '&pganancia='+pganancia+'&nombre='+nombre+'&descripcion='+descripcion+'&costo='+costo+'&precioContado='+precioContado+'&precioTarjeta='+precioTarjeta+'&color='+color+'&talle='+talle+'&cantidad='+cantidad+'&codigo='+codigo+'&marca='+marca+'&ganancia='+ganancia,
             
              success: function(datos){
                $("#btn-altas").prop('disabled', true);
                
                if(datos.result =="exito"){
                  
                 generar_codigo_de_barras(datos.codigo);
                  generar_pdf(datos.codigo);

                 $("#codigo").prop('disabled', true);

                                                 
                  $("#btn-altas").prop('disabled', false);
                 var n = noty({
                  text: "Se registro el articulo correctamente",
                  theme: 'relax',
                  layout: 'center',
                  type: 'information',
                  timeout: 1000,        
                  
                  });
                 //lista_articulos();
                 obtener_codigo();

                 }
                 if(datos.result=="error"){
                 var n = noty({
                 text: "No se registro el articulo, verifique los campos...!",
                 theme: 'relax',
                 layout: 'center',
                 type: 'information',
                 timeout: 2000,
                 });
                 $("#btn-altas").prop('disabled', false);
                }
                }
                ,
                /**************************/
              error: function(jqXHR,estado,error){
                var n = noty({
                 text: "Ocurrio un error al registrar el articulo, consulte a Soporte...!",
                 theme: 'relax',
                 layout: 'center',
                 type: 'information',
                 });
                }
             });
          });
         }
/*******************************************************************************************/
function busca_articulo(){
         $(document).ready(function() {
          $.ajax({
          beforeSend: function(){
             $("#btn-buscar").prop('disabled', true);
           },
          url: 'busca_articulo.php',
          type: 'POST',
          data: 'codigo='+$("#codigo_busqueda").val(),
          success: function(x){

              if(x==0){
               alert("El codigo del articulo, no existe...");
                $("#btn-buscar").prop('disabled', false);
                $("#codigo_busqueda").select();
                $("#codigo_busqueda").focus();
              }else{
              $("#info_articulo").html(x);
              $("#btn-procede-baja").prop('disabled', false);
              $("#btn-cancela-baja").prop("disabled", false);
              }
           },
           error: function(jqXHR,estado,error){
           }
           });
          });
         }
/*******************************************************************************************/
function obtener_codigo(){
$(document).ready(function() {
$.ajax({
beforeSend: function(){

},
url:'obtener_codigo.php',
success: function(x){
if(x > 0){
  $("#codigo").attr('value',x);
  $("#codigo").attr('pleaceholder',x);
}
},
error: function(jqXHR,estado,error){
           }
         });
});
}
/*******************************************************************************************/
function elimina_articulo(){
           var n = noty({
                  text: "Seguro que desea eliminar el articulo...?",
                  theme: 'relax',
                  layout: 'center',
                  type: 'information',
                  buttons     : [
                    {addClass: 'btn btn-primary',
                     text    : 'Si',
                     onClick : function ($noty){
                          $noty.close();
                        $.ajax({
          beforeSend: function(){

           },
          url: 'delete_articulo.php',
          type: 'POST',
          data: 'codigo='+$("#codigo_busqueda").val(),
          success: function(x){
            if (x==1) {
             var n = noty({
              text: "Se elimino la informacion del articulo...!",
              theme: 'relax',
              layout: 'center',
              type: 'information',
              timeout: 2000,
            });
           }
          if (x==2) {
             var n = noty({
              text: "Hubo un error al eliminar el articulo!",
              theme: 'relax',
              layout: 'center',
              type: 'information',
              timeout: 2000,
            });
           }
             cancela_eliminacion();
             lista_articulos();
           },
           error: function(jqXHR,estado,error){
           }
           });
                      }
                   },
                   {addClass: 'btn btn-danger',
                    text    : 'No',
                    onClick : function ($noty){
                       $noty.close();

                    }
                    }
                  ]
                  });
         }
/*************************************************************************************/
function cancela_eliminacion(){
           $("#info_articulo").empty();
           $("#codigo_busqueda").val('');
           $("#btn-cancela-baja").prop('disabled', true);
           $("#btn-procede-baja").prop('disabled', true);
           $("#btn-buscar").prop('disabled', false);
           $("#codigo_busqueda").focus();
         }
/****************************************************************************************/
function cancela_alta_articulo(){
    $("#codigo").val("");
    $("#codigostock").val("");
    $("#descripcion").val("");
    $("#costo").val("0");
    $("#precioContado").val("0");
    $("#codigo").focus();
}
/************************************************************************************/
function busca_articulo_cambio(){
         $(document).ready(function(){
          $.ajax({
          beforeSend: function(){
             $("#btn-buscar-cambio").prop('disabled', true);
           },
          url: 'busca_articulo_cambio.php',
          type: 'POST',
          data: 'codigo='+$("#codigo_busqueda_cambio").val(),
          success: function(x){
              if(x==0){
               alert("El codigo del articulo, no existe...");
                $("#btn-buscar-cambio").prop('disabled', false);
              }else{
              $("#codigo_busqueda_cambio").prop('disabled', true);
              $("#info_articulo_cambio").html(x);
              $("#btn-procede-cambio").prop('disabled', false);
              $("#btn-cancela-cambio").prop('disabled', false);                          
              }
            },
           error: function(jqXHR,estado,error){
           }
           });
          });
         }
/**************************************************************************************/
function cambia_grupos(){
         $(document).ready(function() {
          $.ajax({
          beforeSend: function(){
           },
          url: 'cambia_grupos.php',
          type: 'POST',
          data: 'linea='+$("#linea_cambio").val(),
          success: function(x){
             if(x==0){

             }else{
               $("#btn-procede-cambio").prop('disabled', false);
               $("#btn-cancela-cambio").prop("disabled", false);
               $("#grupo_para_cambiar").html(x);
               $(".select2").select2();


               }
           },
           error: function(jqXHR,estado,error){
           }
           });
          });
         }
/************************************************************************************/
function procede_cambio(){
          var nombre=$("#nombre_cambio").val();
          var descripcion=$("#descripcion_cambio").val();
          var color=$("#color_cambio").val();
          var talle=$("#talle_cambio").val();
          var marca=$("#marca_cambio").val();
          var cantidad=$("#cantidad_cambio").val();
          var codigo=$("#codigo_busqueda_cambio").val();
          var costo=$("#costo_cambio").val();
          var precioContado=$("#precio_contado_cambio").val();
          var precioTarjeta=$("#precio_tarjeta_cambio").val();
          var ganancia= (precioContado - costo);
          var pganancia= ((ganancia * 100)/ costo); 


         $(document).ready(function() {
          $.ajax({
          beforeSend: function(){
             $("#btn-procede-cambio").prop('disabled', true);
           },
          url: 'procede_cambio.php',
          type: 'POST',
          data: '&nombre='+nombre+'&descripcion='+descripcion+'&costo='+costo+'&marca='+marca+'&precioContado='+precioContado+'&precioTarjeta='+precioTarjeta+'&color='+color+'&talle='+talle+'&cantidad='+cantidad+'&ganancia='+ganancia+'&pganancia='+pganancia+'&codigo='+codigo,
          success: function(x){
          
            if(x=='error'){
               var n = noty({
                                text: "Verifique los campos, no se realizo el cambio!",
                                theme: 'relax',
                                layout: 'center',
                                type: 'information',
                                timeout: 2000,
                                });
                                $("#btn-procede-cambio").prop('disabled', false);
               }else{
               
               var n = noty({
                                text: "Se actualizo la informacion del articulo...!",
                                theme: 'relax',
                                layout: 'center',
                                type: 'information',
                                timeout: 2000,
                                });
               cancela_cambios();
               lista_articulos();
               }
           },
           error: function(jqXHR,estado,error){
           }
           });
          });
         }
/*****************************************************************************/
function cancela_cambios(){
           $("#info_articulo_cambio").empty();
           $("#codigo_busqueda_cambio").val('');
           $("#codigo_busqueda_cambio").prop('disabled', false);
           $("#btn-cancela-cambio").prop('disabled', true);
           $("#btn-procede-cambio").prop('disabled', true);
           $("#btn-buscar-cambio").prop('disabled', false);
           $("#codigo_busqueda_cambio").focus();
         }
/********************************************************************************/
     function anular(e) {
          tecla = (document.all) ? e.keyCode : e.which;
          return (tecla != 13);
     }
 /*****************************************************************************/
 function lista_articulos(){
    $(document).ready(function() {
          $.ajax({
          beforeSend: function(){
             $("#lista_articulos").html('<b>Actualizando lista de articulos...</b>');
           },
          url: 'lista_articulos.php',
          type: 'POST',
          data: null,
          success: function(x){
            $("#lista_articulos").html(x);
            $("#tabla_articulos").dataTable();
           },
           error: function(jqXHR,estado,error){
           }
           });
          });
 }
 /**********************************************************************************/