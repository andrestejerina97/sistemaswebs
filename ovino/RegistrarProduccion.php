<!DOCTYPE html>
<html lang="en">
<?php  
session_start();
if($_SESSION['autorizado']<>1){
    header("Location: index.php");
}
?>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="frontend/img/favicon1.ico">
  <title>Registro de producción de leche</title>

  <!-- Bootstrap core CSS -->
   <link href="libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/menuinicial.css" rel="stylesheet">
  <link href="fontawesome-pro-5.7.2-web/css/all.css" rel="stylesheet">
  <link href=" custom/css/style.css" rel="stylesheet">
</head>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
 <?php include 'frontend/sidebar.php'; ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

<?php include 'frontend/nav_bar.php'; ?>
      <div class="container-fluid">
        <h1 class="mt-4"></h1>

        
        <div class="row justify-content-center">
          <div class="col-11 col-md-5">
            <p class="titular-principal">FORMULARIO DE REGISTRO DE PRODUCCIÓN</p>
          </div>
        </div>
        
        <form action="insertar.php" method="POST">
          <section>
            <div class="container">
              <div class="row">
                <div class="col-12">
                    <div class="subtitular subrayado">
                      DATOS GENERALES
                    </div>
                </div>
              </div>
              <!--FIN SUBTITULAR-->
            </div>
            <!--FIN CONTAINER SUBTITULAR-->
      
            <div class="container">
              <div class="row espacio-superior">
                <div class="col-auto col-lg-2 align-self-center">
                  <span class="nombres-campos">Numero de Finca</span>
                </div>
                <div class="col-12 col-lg-3">
                  <input type="text" name="finca" id="finca" class="form-control" placeholder="Ingrese el número finca" required>
                </div>
              </div>
              <!--FIN FILA CEDULA DE IDENTIDAD-->

              <div class="row espacio-superior">
                <div class="col-auto col-lg-2 align-self-center">
                  <span class="nombres-campos">Litros de Leche</span>
                </div>
                <div class="col-12 col-lg-3">
                  <input type="text" name="leche" id="leche" class="form-control" placeholder="Ingrese los litros de leche" required>
                </div>
              </div>

              <!--fin de input litros de leche -->
              <div class="row espacio-superior">
                <div class="col-auto col-lg-2 align-self-center nombres-campos">
                  <span class="nombres-campos">Socios:</span>
                </div>
                <div class="col-12 col-lg-3">
                  <div class="input-group">
                      <input type="text" name="cedula" id="cedula" class="form-control" placeholder="Ingrese la cédula del socio " required>
                      <div class="input-group-append">
                          <button type='button' class='btn' style="background: #c6df45" onclick='registrar_socio();' title="Registrar nuevo socio"><i class='fas fa-edit'></i></button>
                      </div>
                  </div>
                </div>
              </div>
              <!-- fin de input socios --> 
              <div class="row espacio-superior">
                <div id="datos_socio">
                </div>
                <div class="col-12 col-md-4">
                  <!-- <input type="submit" value="Guardar" onclick="alta_socios();" class="btn btn-warning" id="btn_altas"> -->
                  <button type='button' tabindex="6" class='btn btn-warning' onclick='buscar_socio();' id='btn-altas'><i class='fas fa-check-circle'></i> Registrar .</button>
                </div>
              </div>
               <hr>
            </div>
            <!--FIN BOTON ENVIAR-->
          </section>
        </form>
        
        <footer class="fixed-bottom">
          <div class="float-right">2019</div>
          <strong>Parroquia La Matriz (de prueba)</strong>  
        </footer>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
    <script src="libraries/jquery-3.3.1.min.js"></script>
  <script src="libraries/bootstrap/js/bootstrap.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
    <script type="text/javascript" src="custom/js/ocultar.js"></script> 
    <script type="text/javascript" src="backend/funciones/scrips/ingresos.js"></script>

    <script src="libraries/noty/packaged/jquery.noty.packaged.min.js"></script>  

</body>

</html>
