<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="stylesheet" href="../frontend/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/font-awesome.min.css">
  <link rel="shortcut icon" href="../frontend/img/favicon1.ico"/>
  
  <link rel="stylesheet" type="text/css" href="../custom/css/style.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
  
  <title>Complete Bootstrap 4 Website Layout</title>
</head>
<body>
  <div id="linea-superior"></div>
    <header>
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-8 col-md-3"><img src="../frontend/img/logo4M.png" class="img-fluid"></div>
        </div>
        <div class="row justify-content-center">
          <div class="col-11 col-md-5">
            <p class="titular-principal">GRACIAS POR REGISTRARTE</p>
          </div>
        </div>
      </div>
    </header>

    <div class="container">
      <div class="row">
        <div class="col-12 separacion-gracias">
            <section>
                <div class="texto-normal" align="center">Tu registro fue exitoso.</div>
                <div id="capa-extra" class="texto-normal" align="center">Si tienes alguna duda o quieres enviarnos un comentario, puedes hacerlo a </div>
                <div align="center">
                  <a href="mailto:info_4Mguamote@gmail.com" class="link-gracias">
                  info_4Mguamote@gmail.com</a>
                </div>
            </section>
        </div>
      </div>
    </div>
   
    <div class="container">
        <div class="row">
          <div class="col-12 separacion-gracias">
            <?php 
            require_once ('../frontend/funciones/conexiones.php');

  $ced                  =  $_POST['cedula'];
  $ape1                 =  $_POST['p_apellido'];
  $ape2                 =  $_POST['s_apellido'];
  $nombres              =  $_POST['nombres'];
  $estado_civil         =  $_POST['estado_civil'];
  $fnac                 =  $_POST['f_nacimiento'];             
  $genero               =  $_POST['genero'];
  $o_genero             =  $_POST['esp_genero'];
  $trabajo              =  $_POST['trabajo'];
  $nacionalidad         =  $_POST['nacionalidad'];
  $pro                  =  $_POST['provincia'];
  $can                  =  $_POST['canton'];
  $prq                  =  $_POST['parroquia'];
  $barrio               =  $_POST['barrio'];
  $c_principal          =  $_POST['calle_principal'];
  $nun_casa             =  $_POST['numero_casa'];
  $c_secundaria         =  $_POST['calle_secundaria'];
  $ref                  =  $_POST['referencia'];
  $tel                  =  $_POST['telefono'];
  $cel                  =  $_POST['movil'];
  $tlf_adicional        =  $_POST['tlf_adicional'];
  $email                =  $_POST['email'];
  $m_contacto           =  $_POST['medio_contacto'];

  $fecha=date('Y-m-d'); 
  $hora=date('H:i:s');


  $sql = "INSERT INTO Datos_Principales(
  CEDULA, 
  P_APELLIDO, 
  S_APELLIDO,
  NOMBRES,  
  ESTADO_CIVIL, 
  F_NACIMIENTO,
  GENERO,
  O_GENERO,
  TRABAJO,
  NACIONALIDAD,
  PROVINCIA,
  CANTON,
  PARROQUIA,
  BARRIO,
  CALLE_PRINCIPAL,
  NUMERO_CASA,
  CALLE_SECUNDARIA,
  REFERENCIA,
  TELEFONO,
  MOVIL,
  TEL_ADICIONAL,
  EMAIL,
  MEDIO_CONTACTO,
  F_REGISTRO,
  HORA)  
   

  VALUES (
  '$ced', 
  '$ape1', 
  '$ape2', 
  '$nombres', 
  '$estado_civil', 
  '$fnac',
  '$genero',
  '$o_genero',
  '$trabajo',
  '$nacionalidad',
  '$pro',
  '$can',
  '$prq',
  '$barrio',
  '$c_principal',
  '$nun_casa',
  '$c_secundaria',
  '$ref',
  '$tel',
  '$cel',
  '$tlf_adicional',
  '$email',
  '$m_contacto',
  '$fecha',
  '$hora')";

  $q = mysqli_query( $con, $sql);

  if(!$q)
  {
   
    echo "<center><h3>SE HA PRODUCIDO UN ERROR</h3></center>";
  }
  else
  {
    
    echo "<center><h3> DATOS ALMACENADOS CON EXITOS</h3></center>";
  }

 ?>  
          </div>
        </div>
      </div> 
<meta http-equiv='refresh' content='7; url=../Template/F_personal.php'/>   
</body>
</html>