
 <!DOCTYPE html>
<html>
  <head>
  <link rel="icon" href="frontend/img/favicon1.ico">
  <title>Ovinos-Sistema de acopio-Iniciar Sesión</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/menuinicial.css" rel="stylesheet">
  <link href="fontawesome-pro-5.7.2-web/css/all.css" rel="stylesheet">

  </head>
  <body>
    <form action="backend/funciones/valida_entrada.php" method="post" class="AjaxForms MainLogin" data-type-form="login" autocomplete="off">
        <p class="text-center text-muted lead text-uppercase">login</p>
       <?php
echo "<div class='form-group'>";
         echo "<label class='control-label' for='UserName'>Usuario</label>";
        echo" <input class='form-control' name='usuario' id='usuario' type='text' required=''>";
        echo"</div>";
	?>
        <div class="form-group">
          <label class="control-label" for="Pass">Contraseña</label>
          <input class="form-control" name="password" id="password" type="password" required="">
        </div>
        <p class="text-center">
            <button type="submit" class="btn btn-primary btn-block">Ingresar</button>        
        </p>
    </form>
    <div class="MsjAjaxForm"></div>
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   <script src="libraries/noty/packaged/jquery.noty.packaged.min.js"></script>    <script src="libraries/jquery-3.3.1.min.js"></script>
    <script src="libraries/sweetalert.min.js"></script>
  <script src="libraries/AjaxForms.js"></script>
  </body>
</html>
