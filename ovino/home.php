<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="frontend/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="frontend/fontawesome/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="frontend/normalize.css">
	<link rel="stylesheet" type="text/css" href="custom/css/what_wrapper.css">
	<link rel="stylesheet" type="text/css" href="custom/css/home.css">	
	
	<title>Productores de Ovino 4M | La Matriz </title>
	<link rel="shortcut icon" href="frontend/img/favicon1.ico">
</head>
<body>
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-10 col-lg-3">
				<img src="frontend/img/logo4M.png" alt="Impulso Joven" class="img-fluid">
			</div>
		</div>

			
	</div> <!--- /container-->

	<!--- what_wrapper-->
	<div  class="section" id="what_wrapper">
        <div id="what">
            <div class="left_box_wrapper " >
                <div class="left_box clearfix">
                    <div class="left_box_inner">
                        <div class="what1 animated zoomIn visible" data-animation="zoomIn" data-animation-delay="100">
                            <div class="ic1"><i class="fa fa-edit"></i></div>
                            <div class="txt1">WEB Design</div>
                            <div class="txt2">Aprende a crear cualquier sitio web adaptable a dispositivos móviles con Boostrap 4, el mejor framework de diseño web.
                            </div>
                            <div class="txt3"><a href="" class="btn-default btn1">Learn more</a></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="right_box_wrapper">
                <div class="right_box clearfix">
                    <div class="title3 animated fadeInUp visible " data-animation="fadeInUp"
                     data-animation-delay="200"> Módulos del Sistema.
                    </div>

                    <div class="right_box_inner">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="what2 clearfix animated fadeInRight visible" data-animation="fadeInRight" data-animation-delay="300">
                                    <a href="">
                                        <div class="ic1"><i class="fa fa-box-open"></i></div>
                                        <div class="txt1">applications</div>
                                        <div class="txt2">Mobile Application</div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="what2 clearfix animated fadeInRight visible" data-animation="fadeInRight" data-animation-delay="400">
                                    <a href="index.html">
                                        <div class="ic1"><i class="fa fa-edit"></i></div>
                                        <div class="txt1">web Design</div>
                                        <div class="txt2">Unique Graphic Design</div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="what2 clearfix animated fadeInRight visible" data-animation="fadeInRight" data-animation-delay="500">
                                    <a href="">
                                        <div class="ic1"><i class="fa fa-chart-bar"></i></div>
                                        <div class="txt1">Developement</div>
                                        <div class="txt2">Web Development</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="what2 clearfix animated fadeInRight visible" data-animation="fadeInRight" data-animation-delay="600">
                                    <a href="">
                                        <div class="ic1"><i class="fa fa-database"></i></div>
                                        <div class="txt1">programming</div>
                                        <div class="txt2">Java &amp; PHP Programming</div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="what2 clearfix animated fadeInRight visible" data-animation="fadeInRight" data-animation-delay="700">
                                    <a href="">
                                        <div class="ic1"><i class="fa fa-server"></i></div>
                                        <div class="txt1">cloud hosting</div>
                                        <div class="txt2">Secure Cloud Hosting</div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="what2 clearfix animated fadeInRight visible" data-animation="fadeInRight" data-animation-delay="800">
                                    <a href="">
                                        <div class="ic1"><i class="fa fa-life-ring"></i></div>
                                        <div class="txt1">24/7 Support</div>
                                        <div class="txt2">Fast &amp; Friendly Team</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--- /what_wrapper-->


    
</body>
</html>