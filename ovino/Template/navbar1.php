
<!-- Navigation -->  
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <a class="navbar-brand" href="">Web Disigner</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="menu">
    <ul class="navbar-nav">

      <li class="nav-item">
        <a class="nav-link" href=""><i class=" icono fa fa-calculator"></i> Facturas</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href=""><i class=" icono fa fa-cart-arrow-down"></i> Productos</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href=""><i class=" icono fa fa-user"></i> Personas</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href=""><i class=" icono fa fa-users"></i> Usuarios</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href=""><i class=" icono fa fa-cog"></i> Configuración</a>
      </li> 
    </ul>

    <ul class=" navbar-nav ml-auto ">
      <li class="nav-item">
        <a class="nav-link" href=""><i class=" icono fa fa-life-ring"></i> Soporte</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href=""><i class=" icono fa fa-times-circle"></i> Salir</a>
      </li>
      
    </ul>
  </div>  
</nav>