<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../frontend/img/favicon1.ico">
    <title>Template example for Bootstrap</title>
    <!-- Bootstrap core CSS -->
    <link href="../frontend/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href=" ../custom/css/style.css" rel="stylesheet">
  </head>

  <body>

    
    <div id="linea-superior">&nbsp;</div>
    <header>
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-8 col-md-3"><img src="../frontend/img/logo4M.png" class="img-fluid"></div>
        </div>
        <div class="row justify-content-center">
          <div class="col-11 col-md-5">
            <p class="titular-principal">FORMULARIO DE REGISTRO</p>
          </div>
        </div>
      </div>
    </header>

    <form action="../php_action/registrar_personal.php" method="POST" >
    <section>
      <div class="container">
        <div class="row">
          <div class="col-12">
              <div class="subtitular subrayado">
                DATOS GENERALES
              </div>
          </div>
        </div>
        <!--FIN SUBTITULAR-->
      </div>
      <!--FIN CONTAINER SUBTITULAR-->

      <div class="container">
        <div class="row espacio-superior">
          <div class="col-auto align-self-center">
            <span class="nombres-campos">Cédula de identidad</span>
          </div>
          <div class="col-12 col-md-3">
            <input type="number" max="9999999999" name="cedula" id="cedula" class="form-control" placeholder="Ingrese su número de cédula" title="Por favor ingresar 10 dígitos" required>
          </div>
        </div>
        <!--FIN FILA CEDULA DE IDENTIDAD-->
        <div class="row espacio-superior">
          <div class="col-auto align-self-center">
            <span class="nombres-campos">Nombres completos</span>
          </div>
        </div>
        <!--FIN FILA TITULAR NOMBRES COMPLETOS-->
        <div class="row">
          <div class="col-12 col-md-4">
            <input type="text" id="p_apellido" name="p_apellido" class="form-control" placeholder="Ingrese su primer apellido" required>
          </div>
          <div class="col-12 col-md-4 espacio-superior-dm">
            <input type="text" id="s_apellido" name="s_apellido"  class="form-control" placeholder="Ingrese su segundo apellido" required>
          </div>
          <div class="col-12 col-md-4 espacio-superior-dm">
            <input type="text" id="nombres" name="nombres"  class="form-control" placeholder="Ingrese sus nombres" required>
          </div>
        </div>
        <!--FIN FILA NOMBRES-->
        <div class="row espacio-superior">
          <div class="col-12 col-md-auto nombres-campos align-self-center">Estado civil:</div>
          <div class="col-12 col-md-auto" >
            <select class="custom-select form-control" name="estado_civil" id="estado_civil">
              <option selected value="Soltero/a">Soltero/a</option>
              <option value="Casado/a">Casado/a</option>
              <option value="Divorciado/a">Divorciado/a</option>
              <option value="Viudo/a">Viudo/a</option>
              <option value="Unión de hecho">Unión de hecho</option>
            </select>
          </div>
        </div>
        <!--FIN FILA ESTADO CIVIL-->
        <div class="row espacio-superior">
          <div class="col-12 col-md-auto nombres-campos align-self-center">Fecha de nacimiento:</div>
          <div class="col-12 col-md-4">
            <input type="date" name="f_nacimiento" class="form-control" max="2000-01-01" id="f_nacimiento" required>
          </div>
        </div>
        <!--FIN FILA FECHA DE NACIMIENTO-->
        <div class="row espacio-superior">
          <div class="col-12 col-md-auto nombres-campos align-self-center">Género:</div>
          <div class="col-12 col-md-4">
            <select class="custom-select form-control" name="genero" id="genero" onChange="Generos(this)">
              <option selected value="Masculino" class="genero">Masculino</option>
              <option value="Femenino" class="genero">Femenino</option>
              <option value="Otro-Genero" class="genero" >Otro</option>
            </select>
          </div>
        </div>
        <!--FIN FILA GENERO-->
        <div class="row espacio-superior" id="o-genero"  style="display: none;">
          <div class="col-12 col-md-6 align-self-center">
            <input class="form-control"  type="text" placeholder="Especifique el género con el que se identifica" name="esp_genero">
          </div>
        </div>
        <!--FIN ESPECIFICAR GENERO-->

        <div class="row espacio-superior">
          <div class="col-12 col-md-auto nombres-campos align-self-center"> Ocupación:</div>
          <div class="col-12 col-md-4">
            <input type="text" name="trabajo" id="trabajo" placeholder="Especifique el trabajo que desempeña" class="form-control" required>
          </div>
        </div>
        <!--FIN FILA OCUPACIÓN-->

        <div class="row espacio-superior">
          <div class="col-12 col-md-auto nombres-campos align-self-center">Nacionalidad:</div>
          <div class="col-12 col-md-4">
            <input type="text" name="nacionalidad" id="nacionalidad" class="form-control" required>
          </div>
        </div>
        <!--FIN FILA NACIONALIDAD-->
         <div class="row espacio-superior">
          <div class="col-auto align-self-center">
            <span class="nombres-campos">Domicilio</span>
          </div>
        </div>
        <!--FIN FILA TITULAR DOMICILIO-->
        <div class="row">
          <div class="col-12 col-md-3">
            <input type="text" id="provincia" name="provincia" class="form-control" placeholder="Ingrese su provincia" required>
          </div>    
          <div class="col-12 col-md-3">
            <input type="text" id="canton" name="canton" class="form-control" placeholder="Ingrese su cantón" required>
          </div>
          <div class="col-12 col-md-3">
            <input type="text" id="parroquia" name="parroquia" class="form-control" placeholder="Ingrese su parroquia" required>
          </div>
          <div class="col-12 col-md-3">
            <input type="text" id="barrio" name="barrio" class="form-control" placeholder="Ingrese su barrio" required>
          </div>       
        </div>
        <!--FIN FILA DOMICILIO-->
        <div class="row espacio-superior">
          <div class="col-auto align-self-center">
            <span class="nombres-campos">Dirección</span>
          </div>
        </div>
        <!--FIN FILA TITULAR DIRECCION-->
        <div class="row">
          <div class="col-12 col-md-5">
            <input type="text" id="calle_principal" name="calle_principal" class="form-control" placeholder="Ingrese la calle principal" required>
          </div>
          <div class="col-12 col-md-3">
            <input type="text" id="numero_casa" name="numero_casa" class="form-control" placeholder="Numeración" required>
          </div>
          <div class="col-12 col-md-4">
            <input type="text" id="calle_secundaria" name="calle_secundaria" class="form-control" placeholder="Ingrese la calle secundaria" >
          </div>
        </div>
        <div class="row espacio-superior">
          <div class="col-12 col-md-5">
            <textarea id="referencia" name="referencia" class="form-control" placeholder="Ingrese una referencia"  rows="3"></textarea>     
          </div>        
        </div>
        <!--FIN FILA DIRECCION-->
        <div class="row espacio-superior">
          <div class="col-auto align-self-center">
            <span class="nombres-campos">Teléfonos de contacto</span>
          </div>
        </div>
        <!--FIN FILA TITULAR TELEFONOS-->
        <div class="row">
          <div class="col-12 col-md-4">
            <input type="number" id="telefono" name="telefono" class="form-control" placeholder="Teléfono de domicilio" required>
          </div>
          <div class="col-12 col-md-4">
            <input type="number" id="movil" name="movil" class="form-control" placeholder="Teléfono móvil (celular)" required>
          </div>
          <div class="col-12 col-md-4">
            <input type="text" id="tlf_adicional" name="tlf_adicional" class="form-control" placeholder="Teléfono adicional" >
          </div>
        </div>
        <!--FIN FILA TELEFONOS-->
        <div class="row espacio-superior">
          <div class="col-12 col-md-auto nombres-campos align-self-center">Correo electrónico:</div>
          <div class="col-12 col-md-auto" >
           <input type="email" id="email" name="email" placeholder="Ingrese su email" class="form-control" required>
          </div>
        </div>  
        <!--FIN FILA CORREO ELECTRONICO-->
        <div class="row espacio-superior">
          <div class="col-auto align-self-center">
            <span class="nombres-campos">¿Por cuál de los siguientes medios preferiría ser contactado?</span>
          </div>
        </div>
        <!--FIN FILA TITULAR MEDIO DE COMUNICACION PREFERIDO-->
        <div class="row espacio-superior">
          <div class="col-12 col-md-auto">
            <div class="custom-control custom-radio">
              <input type="radio" id="tlf_convencional" name="medio_contacto" class="custom-control-input" value="Teléfono convencional">
              <label class="custom-control-label" for="tlf_convencional">Teléfono convencional</label>
            </div>
           </div>
          <div class="col-12 col-md-auto">
            <div class="custom-control custom-radio">
              <input type="radio" id="tlf_celular" name="medio_contacto" class="custom-control-input" value="Teléfono celular">
              <label class="custom-control-label" for="tlf_celular">Teléfono celular</label>
            </div>
           </div>
           <div class="col-12 col-md-auto">
            <div class="custom-control custom-radio">
              <input type="radio" id="email_contacto" name="medio_contacto" class="custom-control-input" value="Correo electrónico" checked="checked">
              <label class="custom-control-label" for="email_contacto">Correo electrónico</label>
            </div>
           </div>
           <div class="col-12 col-md-auto">
            <div class="custom-control custom-radio">
              <input type="radio" id="whatsapp" name="medio_contacto" class="custom-control-input" value="Whatsapp">
              <label class="custom-control-label" for="whatsapp">Whatsapp</label>
            </div>
           </div>
        </div>
        <!--FIN FILA MEDIO DE COMUNICACION PREFERIDO-->
      </div>
      <!--FIN CONTENEDOR DATOS GENERALES-->
     
      <div class="container">
        <div class="row espacio-superior">
          <div class="col-12 col-md-1">
            <input type="submit" value="Guardar" class="btn btn-warning" id="enviar">
          </div>
        </div>
        <hr>
      </div>
      <!--FIN BOTON ENVIAR-->
    </section>
    </form>
    <br>

    
    <script type="text/javascript" src="../custom/js/ocultar.js"></script> 
  </body>
</html>
