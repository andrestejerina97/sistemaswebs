<?php 
session_start();
	require_once ('funciones.php');
require_once('fpdf/fpdf.php');
class PDF extends FPDF 
{ 
function header() 
{ 
$this->Image('img/logo4M.png',16,8,33); 
$this->Image('img/logo4M.png',245,8,33);  		
$this->SetFont('Arial','B',20); 
$this->SetTextColor(0,0,0);
$this->Cell(50); 
$this->Cell(160,5,'INFORME GENERAL DE PRODUCCION POR SECTOR.',0,0,'C'); 
$this->SetFont('Arial','',14); 
$this->Ln(10); 
$this->setx(55);
$this->Cell(122,5,'Creado por: '.$_SESSION['nombre_de_usuario']." ".$_SESSION['apellido_de_usuario'],0,0,'L');
$this->Cell(62,5,date('d/m/Y'),0,0,'C');
$this->Ln(7); 
 $this->setx(55);
$this->Cell(129,5,'Autorizado por: '.$_SESSION['autorizador'],0,0,'L');
$this->Cell(43,5,date('H:i:s'),0,0,'C');
$this->Ln(20); 
$this->Cell(195,5,utf8_decode('Lista de todos los sectores junto a la producción acumulada de cada uno'),0,0,'C'); 
$this->Ln(10); 

} 
function Footer() 
{ 
$this->SetY(-15); 

$this->SetTextColor(0,0,0);
$this->SetFont('Arial','',10); 
$this->Cell(283,6,'Pagina '.$this->PageNo().'/{nb}',0,0,'C'); 
$this->Ln(); 

} 
} 
$db= new FuncionesMysql();
$db->ConexionMySql();
$sql="SELECT s.sector,SUM(i.litros) AS total_litros FROM socios s INNER JOIN ingresos i ON i.socios_idsocios=s.idsocios GROUP BY s.sector ORDER BY s.sector DESC";

$exe=$db->consulta($sql);

$pdf=new PDF('L');
$pdf->SetFont('Arial','B',12); 
$pdf->AliasNbPages(); 
$pdf->AddPage(); 
$pdf->SetDrawColor(0,0,0);


$pdf->setX(40);
if ($db->numero_de_registros($exe)) {
//$pdf->Cell(1,150,"CEDULA",0,0,'L');
//$pdf->Cell(48,6,"APELLIDOS",1,0,'C'); 
//$pdf->Cell(34,6,"NOMBRES",1,0,'C'); 
//$pdf->Cell(25,6,"CEDULA",1,0,'C'); 
//$pdf->Cell(75,6,"DIRECCION",1,0,'C'); 
$pdf->Cell(105,9,"SECTOR",1,0,'C'); 
$pdf->Cell(105,9,"LITROS DE LECHE ",1,0,'C'); 
 
$pdf->ln();
while ($consulta=$db->buscar_array($exe)) {
	//echo $each(array)['apellido_uno'];
$pdf->setX(40);

$pdf->SetFont('Arial','B',11); 
//$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(0,0,0);
//$pdf->SetFillColor(227,142,21);

//$pdf->Cell(48,6,$consulta["apellido_uno"]." ".$consulta["apellido_dos"],1,0,'C','true'); 
//$pdf->Cell(34,6,$consulta["nombres"],1,0,'C','true'); 
//$pdf->Cell(25,6,$consulta["cedula"],1,0,'C','true');  
//$pdf->Cell(75,6,$consulta["barrio"]." ,".$consulta["calle_principal"],1,0,'C','true'); 
$pdf->Cell(105,8,$consulta["sector"],1,0,'C'); 
$pdf->Cell(105,8,$consulta["total_litros"],1,0,'C'); 
//$pdf->Cell(51,6,$consulta["IdDepartamento"],1,0,'C','true'); 
//$pdf->Cell(39,6,$consulta["FechaCambio"],1,0,'C','true'); 
//$pdf->Cell(39,6,$consulta["HoraCambio"],1,0,'C','true');
$pdf->Ln();  
}	
}
//$nombre=$_POST['codigo'];
//$path='images/productos/'.$nombre.'.png';
//$destino='images/'.$nombre.'.pdf'

//$pdf->Image('images/icon.png',10,8,200);

//$pdf->Image($path,60,75,80);

$pdf->Output();

 ?>