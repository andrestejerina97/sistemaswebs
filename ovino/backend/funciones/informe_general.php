<?php
//session_start();
//if($_SESSION['autorizado']<>1){
  //  header("Location: index.php");
//}
//error_reporting(0);
require_once('funciones.php');

$db= new FuncionesMysql();
$db->ConexionMySql();
$cadena="SELECT s.apellido_uno,s.apellido_dos,s.cedula,s.nombres,s.sector,d.barrio,d.calle_principal,d.provincia,SUM(i.litros) AS total_litros FROM socios s INNER JOIN ingresos i ON i.socios_idsocios=s.idsocios INNER JOIN direccion_socio d ON d.socios_idsocios=s.idsocios GROUP BY s.cedula,s.nombres,s.apellido_uno,s.apellido_dos,s.sector,d.barrio,d.calle_principal,d.provincia  ORDER BY s.apellido_uno  ";

$exe=$db->consulta($cadena);


if($db->numero_de_registros($exe)>0){
  
echo "<div class='table-responsive '>";
 echo "<table id='tabla_informe_general' class='table table-striped table-bordered dt-responsive nowrap '  cellspacing='0' width='100%'>";

 echo "<thead>";
 echo "<tr>";
 echo "<th>Apellidos</th><th>Nombres</th><th>Cedula</th><th>Barrio</th><th>Calle principal</th><th>Provincia</th><th>Litros totales</th>";
 echo "</tr>";
 echo "</thead>";
 echo "<tbody>";
 while($e=$db->buscar_array($exe)){
   echo "<tr>";
   echo "<td style='text-align: center;'>".$e['apellido_uno']." ".$e[apellido_dos]."</td>";
   echo "<td style='text-align: center;'>".utf8_encode($e['nombres'])."</td>";
   echo "<td style='text-align: center;'>$e[cedula]</td>";
   echo "<td style='text-align: center;'>$e[barrio]</td>";
   echo "<td style='text-align: center;'>".utf8_encode($e['calle_principal'])."</td>";
   echo "<td style='text-align: center;'>$e[provincia]</td>";

   echo "<td style='text-align: center;'>$e[total_litros]</td>";
      echo "</tr>";
 }
 echo "</tbody>";
 echo "</table>";
echo "</div>";
}else{
 echo "<b>Actualmente no hay articulos registrados...</b>";
}
?>