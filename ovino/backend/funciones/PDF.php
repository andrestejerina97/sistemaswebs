<?php 
include_once('fpdf/fpdf.php'); 
session_start();
//sheader("Content-Type: text/html;charset=utf-8");
class PDF extends FPDF 
{ 
function header() 
{ 
$this->Image('img/logo4M.png',16,8,33); 
$this->Image('img/logo4M.png',245,8,33);  		
$this->SetFont('Arial','B',20); 
$this->SetTextColor(0,0,0);
$this->Cell(50); 
$this->Cell(160,5,'INFORME GENERAL DE PRODUCCION POR SOCIOS.',0,0,'C'); 
$this->SetFont('Arial','',14); 
$this->Ln(10); 
$this->setx(55);
$this->Cell(122,5,'Creado por: '.$_SESSION['nombre_de_usuario']." ".$_SESSION['apellido_de_usuario'],0,0,'L');
$this->Cell(62,5,date('d/m/Y'),0,0,'C');
$this->Ln(7); 
 $this->setx(55);
$this->Cell(129,5,'Autorizado por: '.$_SESSION['autorizador'],0,0,'L');
$this->Cell(43,5,date('H:i:s'),0,0,'C');
$this->Ln(20); 
$this->Cell(195,5,utf8_decode('Lista de todos los socios junto a la producción acumulada de cada uno'),0,0,'C'); 
$this->Ln(10); 

} 
function Footer() 
{ 
$this->SetY(-15); 

$this->SetTextColor(0,0,0);
$this->SetFont('Arial','',10); 
$this->Cell(283,6,utf8_decode('Página').$this->PageNo().'/{nb}',0,0,'C'); 
$this->Ln(); 

} 
} 
 ?>