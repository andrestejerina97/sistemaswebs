function informe_general(){
$(document).ready(function() {
          $.ajax({
          beforeSend: function(){
             $("#lista_socios").html('<b>Actualizando lista de articulos...</b>');
           },
          url: 'backend/funciones/informe_general.php',
          type: 'POST',
          data: null,
          success: function(x){
            $("#listado_informe_general").html(x);

            $("#tabla_informe_general").DataTable({

"language": {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    }
    } );
           },
           error: function(jqXHR,estado,error){
           }
           });
          });





}
function informe_general_pdf(){
$(document).ready(function() {
          $.ajax({
          beforeSend: function(){
             $("#lista_socios").html('<b>Actualizando lista de articulos...</b>');
           },
          url: 'backend/funciones/pdf_general.php',
          type: 'POST',
          data: null,
          success: function(x){
            
           },
           error: function(jqXHR,estado,error){
           }
           });
          });







}
function informe_por_sector(){
$(document).ready(function() {
          $.ajax({
          beforeSend: function(){
             $("#lista_socios").html('<b>Actualizando lista de articulos...</b>');
           },
          url: 'backend/funciones/informe_por_sector.php',
          type: 'POST',
          data: null,
          success: function(x){
            $("#listado_informe_por_sector").html(x);

            $("#tabla_informe_por_sector").DataTable({
        
        "language": {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    }
} );
    
           },
           error: function(jqXHR,estado,error){
           }
           });
          });





}
