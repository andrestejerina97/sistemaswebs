<!DOCTYPE html>
<html lang="en">
<?php  
session_start();
if($_SESSION['autorizado']<>1){
    header("Location: index.php");
}
?>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="frontend/img/favicon1.ico">
  <title>Ovinos-Sistema de acopio</title>

  <!-- Bootstrap core CSS -->
   <link href="libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/menuinicial.css" rel="stylesheet">
  <link href="fontawesome-pro-5.7.2-web/css/all.css" rel="stylesheet">
</head>

<body>

  <div class="d-flex" id="wrapper">

 <?php
       
        $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","S�bado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha=$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
        
        ?>
    <!-- Sidebar -->
  <?php include 'frontend/sidebar.php'; ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">


     <?php include 'frontend/nav_bar.php'; ?>

      <div class="container-fluid">
        <h1 class="mt-4"></h1>

  <section class="content-header">
          <h1>
          <?php
         
          ?>
            <small><?php echo $fecha; ?></small>
          </h1>


                 
          <ol class="breadcrumb">
          <h2>
        
            <small>Usuario: <?php echo $_SESSION['nombre_de_usuario']; ?></small>
            <br>
            <small>Autorizador: <?php echo $_SESSION['autorizador']; ?></small>
          </h2>
          </ol>
        </section>

        <p></p>
        

        
        <footer class="fixed-bottom">
          <div class="float-right">2019</div>
       
        </footer>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="libraries/jquery-3.3.1.min.js"></script>
    <script src="libraries/jquery.min.js"></script>
  <script src="libraries/bootstrap/js/bootstrap.bundle.js"></script>
  <script src="libraries/bootstrap/js/bootstrap.min.js"></script>
  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

  
<script type="text/javascript" src="custom/js/ocultar.js"></script> 

</body>

</html>

