<?php
 //checking for minimum PHP version
if (version_compare(PHP_VERSION, '5.3.7', '<')) {
   exit("Sorry, Simple PHP Login does not run on a PHP version smaller than 5.3.7 !");
} else if (version_compare(PHP_VERSION, '5.5.0', '<')) {
   //  if you are using PHP 5.3 or PHP 5.4 you have to include the password_api_compatibility_library.php
     //(this library adds the PHP 5.5 password hashing functions to older versions of PHP)
  require_once("libraries/password_compatibility_library.php");
}

session_start();
$_SESSION['autorizado']=0;
    // for demonstration purposes, we simply show the "you are not logged in" view.
    ?>
    
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="frontend/img/favicon1.ico">
    <title>Inicio de sesión</title>
    <!-- Bootstrap core CSS -->
    <link href="libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="libraries/sweetalert2.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href=" custom/css/style.css" rel="stylesheet">
  </head>
  <body class="uno">
    
    <div class="container">
      <br><br><br>
      <div class="row align-items-lg-center justify-content-center">
        <div class="col-12">
          <form id="formulario" action="backend/funciones/valida_entrada.php" method="POST" accept-charset="utf-8"  data-type-form="login"   name="loginform" autocomplete="off" role="form" class="form-signin">
            
            <center>
            <img src="frontend/img/login.png" width="120" height="120">
            <h4> INGRESO AL SISTEMA</h4>
            </center>
            <br>

      <div class="form-group">
             <input type="text" id="usuario" name="usuario" class="form-control" placeholder="Usuario" autocomplete="off" autofocus="" required>
             </div>
<div class="form-group">
             <input type="password" id="password" name="password" class="form-control" placeholder="Contraseña" autocomplete="off" required>
    </div>
             <div align="right">
              <button class="btn btn-large btn-primary"  name="login" type="submit" id="boton"><strong>Ingresar</strong></button>
            </div>      
        </form>

        </div><!-- /col-12 -->
      </div> <!-- /row -->
    </div><!-- /container -->
 <div id="msj" ></div>
     <script src="libraries/jquery-3.3.1.min.js"></script>

     <script src="libraries/jquery.min.js"></script>
<script src="libraries/sweetalert2.min.js"></script>  
      <script src="libraries/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="libraries/bootstrap/js/bootstrap.min.js"></script>
    
  

 

  </body>
</html>

