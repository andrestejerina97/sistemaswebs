<!DOCTYPE html>
<html lang="en">
<?php  
session_start();
if($_SESSION['autorizado']<>1){
    header("Location: index.php");
}
?>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="frontend/img/favicon1.ico">
  <title>Registro de socios</title>

  <!-- Bootstrap core CSS -->
     <link href="libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/menuinicial.css" rel="stylesheet">
  <link href="fontawesome-pro-5.7.2-web/css/all.css" rel="stylesheet">
  <link href=" custom/css/style.css" rel="stylesheet">
  
</head>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
  <?php include 'frontend/sidebar.php' ?>
    <!-- /#sidebar-wrapper -->



    <!-- Page Content -->
    <div id="page-content-wrapper">

<?php include 'frontend/nav_bar.php'; ?>

      <div class="container-fluid">
        <h1 class="mt-4"></h1>

        
        <div class="row justify-content-center">
          <div class="col-11 col-md-5">
            <p class="titular-principal">FORMULARIO DE REGISTRO DE SOCIOS</p>
          </div>
        </div>
        
        <form >
          <section>
            <div class="container">
              <div class="row">
                <div class="col-12">
                    <div class="subtitular subrayado">
                      DATOS GENERALES
                    </div>
                </div>
              </div>
              <!--FIN SUBTITULAR-->
            </div>
            <!--FIN CONTAINER SUBTITULAR-->
      
            <div class="container">         
              <div class="row espacio-superior">
                <div class="col-12 col-md-2 nombres-campos align-self-center">Cédula:</div>
                <div class="col-12 col-md-3">
                  <input type="text" name="cedula" id="cedula" class="form-control" placeholder="Número de Cédula" title="Por favor ingresar 10 dígitos" >
                </div>
              </div>

              <!--FIN FILA CEDULA DE IDENTIDAD-->
<div class="row espacio-superior">
                <div class="col-12 col-md-2 nombres-campos align-self-center">Apellidos:</div>
    
                <div class="col-12 col-md-3">
                  <input type="text" id="p_apellido" name="p_apellido" class="form-control" placeholder="Primer Apellido" >
                </div>
                <div class="col-12 col-md-3 espacio-superior-dm">
                  <input type="text" id="s_apellido" name="s_apellido"  class="form-control" placeholder="Segundo Apellido">
              </div>

</div>


              <div class="row espacio-superior">
                <div class="col-12 col-md-2 nombres-campos align-self-center">Nombres completos:</div>

                <div class="col-12 col-md-3 espacio-superior-dm">
                  <input type="text" id="nombres" name="nombres"  class="form-control" placeholder="Nombres" >
                </div>
              </div>
              <!--FIN FILA NOMBRES-->
              <div class="row espacio-superior">
                <div class="col-12 col-md-2 nombres-campos align-self-center">Estado civil</div>
                <div class="col-12 col-md-2">
                  <select class="custom-select form-control" name="estado_civil" id="estado_civil">
                    <option selected value="Soltero/a">Soltero/a</option>
                    <option value="Casado/a">Casado/a</option>
                    <option value="Divorciado/a">Divorciado/a</option>
                    <option value="Viudo/a">Viudo/a</option>
                    <option value="Unión de hecho">Unión de hecho</option>
                  </select>
                </div>
              </div>
              <!--FIN FILA ESTADO CIVIL-->
              <div class="row espacio-superior">
                <div class="col-12 col-md-2 nombres-campos align-self-center" >Fecha de nacimiento</div>
                <div class="col-12 col-md-3">
                  <input type="date" name="f_nacimiento" class="form-control" max="2000-01-01" id="f_nacimiento">
                </div>
              </div>
              <!--FIN FILA FECHA DE NACIMIENTO-->
              <div class="row espacio-superior">
                <div class="col-12 col-md-2 nombres-campos align-self-center">Género:</div>
                <div class="col-12 col-md-2">
                  <select class="custom-select form-control" name="genero" id="genero" onChange="Generos(this)">
                    <option selected value="Masculino" class="genero">Masculino</option>
                    <option value="Femenino" class="genero">Femenino</option>
                    <option value="Otro-Genero" class="genero" >Otro</option>
                  </select>
                </div>
              </div>
              <!--FIN FILA GENERO-->
              <div class="row espacio-superior" id="o-genero"  style="display: none;">
                <div class="col-12 col-md-6 align-self-center">
                  <input class="form-control" id="esp_genero" type="text" placeholder="Especifique el género con el que se identifica" name="esp_genero">
                </div>
              </div>
              <!--FIN ESPECIFICAR GENERO-->
      
              <div class="row espacio-superior">
                <div class="col-12 col-md-2 nombres-campos align-self-center"> Ocupación:</div>
                <div class="col-12 col-md-4">
                  <input type="text" name="trabajo" id="trabajo" placeholder="Especifique el trabajo que desempeña" class="form-control" >
                </div>
              </div>
              <!--FIN FILA OCUPACIÓN-->
      
              <div class="row espacio-superior">
                <div class="col-12 col-md-2 nombres-campos align-self-center">Nacionalidad:</div>
                <div class="col-12 col-md-4">
                  <input type="text" name="nacionalidad" id="nacionalidad" class="form-control">
                </div>
              </div>
              <!--FIN FILA NACIONALIDAD-->
               <div class="row espacio-superior">
                <div class="col-auto col-md-2 align-self-center nombres-campos">Domicilio</div>
                <div class="col-12 col-md-2">
                  <input type="text" id="provincia" name="provincia" class="form-control" placeholder="Provincia">
                </div>    
                <div class="col-12 col-md-2">
                  <input type="text" id="canton" name="canton" class="form-control" placeholder="Cantón" >
                </div>
                <div class="col-12 col-md-2">
                  <input type="text" id="parroquia" name="parroquia" class="form-control" placeholder="Parroquia" >
                </div>
                <div class="col-12 col-md-2">
                  <input type="text" id="barrio" name="barrio" class="form-control" placeholder="Barrio" >
                </div> 
              </div>
             
             
              <!--FIN FILA TITULAR DOMICILIO-->

              <!--FILA SECTOR-->
              <div class="row espacio-superior">
              <div class="col-12 col-md-2 nombres-campos align-self-center">Sector:</div>
              <div class="col-12 col-md-2">
              <input type="text" id="sector" name="sector" class="form-control" placeholder="Sector al que pertenece" title="completar el sector" >
                      </select>
                    </div>
              </div>
              
              <!--FIN DE FILA SECTOR-->            
              
              <div class="row espacio-superior">
                <div class="col-auto col-md-2 align-self-center nombres-campos">Dirección</div>
                <div class="col-12 col-md-3">
                  <input type="text" id="calle_principal" name="calle_principal" class="form-control" placeholder="Calle Principal" >
                </div>
                <div class="col-12 col-md-2">
                  <input type="text" id="numero_casa" name="numero_casa" class="form-control" placeholder="Numeración" >
                </div>
                <div class="col-12 col-md-3">
                  <input type="text" id="calle_secundaria" name="calle_secundaria" class="form-control" placeholder="Calle Secundaria" >
                </div>
              </div>

              <!--FIN FILA TITULAR DIRECCION-->
              <div class="row espacio-superior">
                <div class="col-12 col-md-2"></div>
                <div class="col-12 col-md-5">
                  <textarea id="referencia" name="referencia" class="form-control float-right" placeholder="Ingrese una referencia"  rows="6"></textarea>     
                </div>        
              </div>
              <!--FIN FILA DIRECCION-->
              
              <div class="row espacio-superior">
                <div class="col-auto col-md-2 align-self-center nombres-campos">Teléfono:</div>
                <div class="col-12 col-md-2">
                  <input type="text" id="telefono" name="telefono" class="form-control" placeholder="De domicilio" >
                </div>
                <div class="col-12 col-md-2">
                  <input type="text" id="movil" name="movil" class="form-control" placeholder="Móvil" >
                </div>
                <div class="col-12 col-md-2">
                  <input type="text" id="tlf_adicional" name="tlf_adicional" class="form-control" placeholder="Adicional" >
                </div>
              </div>
             <!--FIN FILA TELEFONOS-->
              <div class="row espacio-superior">
                <div class="col-12 col-md-2 nombres-campos align-self-center">Correo electrónico:</div>
                <div class="col-12 col-md-auto" >
                 <input type="email" id="email" name="email" placeholder="Ingrese su email" class="form-control">
                </div>
              </div>  
              <!--FIN FILA CORREO ELECTRONICO-->
              <div class="row espacio-superior">
                <div class="col-auto align-self-center">
                  <span class="nombres-campos">¿Por cuál de los siguientes medios preferiría ser contactado?</span>
                </div>
              </div>
              <!--FIN FILA TITULAR MEDIO DE COMUNICACION PREFERIDO-->
              <div class="row espacio-superior">
                <div class="col-12 col-md-auto">
                  <div class="custom-control custom-radio">
                    <input type="radio" id="tlf_convencional" name="medio_contacto" class="custom-control-input" value="Teléfono convencional">
                    <label class="custom-control-label" for="tlf_convencional">Teléfono convencional</label>
                  </div>
                 </div>
                <div class="col-12 col-md-auto">
                  <div class="custom-control custom-radio">
                    <input type="radio" id="tlf_celular" name="medio_contacto" class="custom-control-input" value="Teléfono celular">
                    <label class="custom-control-label" for="tlf_celular">Teléfono celular</label>
                  </div>
                 </div>
                 <div class="col-12 col-md-auto">
                  <div class="custom-control custom-radio">
                    <input type="radio" id="email_contacto" name="medio_contacto" class="custom-control-input" value="Correo electrónico" checked="checked">
                    <label class="custom-control-label" for="email_contacto">Correo electrónico</label>
                  </div>
                 </div>
                 <div class="col-12 col-md-auto">
                  <div class="custom-control custom-radio">
                    <input type="radio" id="whatsapp" name="medio_contacto" class="custom-control-input" value="Whatsapp">
                    <label class="custom-control-label" for="whatsapp">Whatsapp</label>
                  </div>
                 </div>
             </div>
              <!--FIN FILA MEDIO DE COMUNICACION PREFERIDO-->

             </div>
            <!--FIN CONTENEDOR DATOS GENERALES-->
 <div class="container">
  <div class="row espacio-superior">
          <div class="col-12 col-md-4">
            <!-- <input type="submit" value="Guardar" onclick="alta_socios();" class="btn btn-warning" id="btn_altas"> -->
            <button type='button' tabindex="6" class='btn btn-warning btn-lg' onclick='alta_socios();' id='btn-altas'><i class='fas fa-check-circle'></i> Registrar socio.</button>
          </div>
          <div class="col-12 col-md-auto">
            <button type="button" class="btn btn-lg"  style="background-color: #c6df45;"  data-toggle="modal" data-target="#registroproduccion"> <i class="fas fa-cow"></i> Registrar y agregar Producción</button>
        </div>
        
    </div>
    <hr>
    </div>
            <!--FIN BOTON ENVIAR-->
                      <!--Modal para agregar producción-->
            <div class="modal fade" id="registroproduccion" tabindex="-1" role="dialog" aria-labelledby="registroproduccionlLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-warning">
                    <h5 class="modal-title" id="registroproduccionLabel">Registro de Producción</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="container">
                      <div class="row espacio-superior">
                        <div class="col-auto col-md-5 align-self-center nombres-campos">
                          <span class="nombres-campos">Numero de Finca:</span>
                        </div>
                        <div class="col-12 col-md-7">
                          <input type="text" name="finca" id="finca" class="form-control" placeholder="Ingrese el número finca" required>
                        </div>
                      </div>

                      <div class="row espacio-superior">
                        <div class="col-auto col-md-5 align-self-center nombres-campos">
                          <span class="nombres-campos">Litros de Leche:</span>
                        </div>
                        <div class="col-12 col-md-7">
                          <input type="text" name="leche" id="leche" class="form-control" placeholder="Ingrese los litros de leche" required>
                        </div>
                      </div>
                      <br>
                      <br>
                      <div class="modal-footer bg-warning">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-md" onclick="registrar_ingresar();" style="background-color: #c6df45;"> <i class="fas fa-thumbs-up"></i>Guardar</button>
                      </div>
                    </div>
                  </div>

              </div>
</div>
          </div>
 
          <!--FIN DE MODAL--> 



          </section>


        </form>
<div class="container-fluid">
<div id="lista_socios">
  
  </div>        
</div>
        
        <footer class="fixed-bottom">
          <div class="float-right">2019</div>
          <strong>Parroquia La Matriz (de prueba)</strong>  
        </footer>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="libraries/jquery-3.3.1.min.js"></script>
  <script src="libraries/bootstrap/js/bootstrap.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

 <script src="libraries/jquery-3.3.1.min.js"></script>
    <script src="libraries/noty/packaged/jquery.noty.packaged.min.js"></script>
    <script type="text/javascript" src="custom/js/ocultar.js"></script> 
    <script type="text/javascript" src="backend/funciones/scrips/ingresos.js">
  </script>
    <script type="text/javascript" src="backend/funciones/scrips/usuarios.js"></script>
   


</body>

</html>
