function Generos(genero){
        if (genero.value == "Otro-Genero") {
          document.getElementById("o-genero").style.display="inline";
        }
        else{
          document.getElementById("o-genero").style.display="none";
        }
      };

function Etnias(etnia){
   if (etnia.value == "Otra-etnia") {
     document.getElementById("otra-etnia").style.display="inline";
   }
   else{
     document.getElementById("otra-etnia").style.display="none";
   }
 };

 function Educacion(educacion){
   if (educacion.value == "Tercer-Nivel" || educacion.value == "Cuarto-Nivel") {
     document.getElementById("profesion").style.display="inline";
   }
   else{
     document.getElementById("profesion").style.display="none";
   }
 };

function Cargas(cargas_familiares){
   if (cargas_familiares.value == "Si") {
     document.getElementById("num_cargas").style.display="inline";
   }
   else{
     document.getElementById("num_cargas").style.display="none";
   }
 };

 function Ocupacion(ocupacion){
   if (ocupacion.value == "Otra-ocupación") {
     document.getElementById("otra_ocupacion").style.display="inline";
   }
   else{
     document.getElementById("otra_ocupacion").style.display="none";
   }
 };

  function Conadis(conadis){
   if (conadis.value == "Si") {
     document.getElementById("detalles_conadis").style.display="inline";
   }
   else{
     document.getElementById("detalles_conadis").style.display="none";
   }
 };

 function OComerciales(comerciantes){
   if (comerciantes.value == "Si") {
     document.getElementById("ocomerciales").style.display="inline";
   }
   else{
     document.getElementById("ocomerciales").style.display="none";
   }
 };

 function OEPS(miembro_activo){
   if (miembro_activo.value == "EPS") {
     document.getElementById("detalles_oeps").style.display="inline";
   }
   else{
     document.getElementById("detalles_oeps").style.display="none";
   }
 };

//FINANCIAMIENTO
 function Financiamiento(emprendimiento){
   if (emprendimiento.value == "Financiamiento") {
     document.getElementById("info_financiamiento").style.display="inline";
   }
   else{
     document.getElementById("info_financiamiento").style.display="none";
   }
 };

//INSTITUCIONES PROMOTORAS
 function EmpreAsesoras(asesoramiento_inst){
   if (asesoramiento_inst.value == "Si") {
     document.getElementById("asesoras").style.display="inline";
   }
   else{
     document.getElementById("asesoras").style.display="none";
   }
 };

 //COMERCIALIZACION POR LO MENOS UN AÑO
 function TiempoEmpren(etapa_emprendimiento){
   if (etapa_emprendimiento.value == "Comercialización por menos de un año" || etapa_emprendimiento.value == "Comercialización por más de un año") {
     document.getElementById("tiempo_emprendimiento").style.display="inline";
   }
   else{
     document.getElementById("tiempo_emprendimiento").style.display="none";
   }
 };

 //UNIPERSONAL
 function Unipersonal(unipersonal){
   if (unipersonal.value == "No") {
     document.getElementById("num_personas").style.display="inline";
   }
   else{
     document.getElementById("num_personas").style.display="none";
   }
 };