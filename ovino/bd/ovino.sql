-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-07-2019 a las 02:10:49
-- Versión del servidor: 5.7.14
-- Versión de PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ovino`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion_socio`
--

CREATE TABLE `direccion_socio` (
  `iddireccion_socio` int(11) NOT NULL,
  `barrio` varchar(45) DEFAULT NULL,
  `calle_principal` varchar(45) DEFAULT NULL,
  `calle_secundaria` varchar(45) DEFAULT NULL,
  `nacionalidad` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `canton` varchar(45) DEFAULT NULL,
  `parroquia` varchar(45) DEFAULT NULL,
  `numero_casa` varchar(15) NOT NULL,
  `referencia` varchar(45) DEFAULT NULL,
  `socios_idsocios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `direccion_socio`
--

INSERT INTO `direccion_socio` (`iddireccion_socio`, `barrio`, `calle_principal`, `calle_secundaria`, `nacionalidad`, `provincia`, `canton`, `parroquia`, `numero_casa`, `referencia`, `socios_idsocios`) VALUES
(13, 'Pango', 'Unique ', '', '', '', '', '', '', '', 16),
(14, '25 de mayo', 'Saveedra', '', '', '', '', '', '', '', 17),
(15, '25 de mayo', 'Olsacher', '', '', '', '', '', '', '', 18),
(16, '9 de Julio', 'Joaquin Victor Gonzales', '', '', '', '', '', '', '', 19),
(53, 'Chinquingalla', 'los amauces', 'second', 'Venezuela', 'Rosca', '34', '4', '34', 'soblando a la esquina', 56);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingresos`
--

CREATE TABLE `ingresos` (
  `idingresos` int(11) NOT NULL,
  `socios_idsocios` int(11) NOT NULL,
  `finca` int(11) NOT NULL,
  `usuarios_idusuarios` int(11) NOT NULL,
  `litros` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ingresos`
--

INSERT INTO `ingresos` (`idingresos`, `socios_idsocios`, `finca`, `usuarios_idusuarios`, `litros`) VALUES
(1, 18, 2, 1, '50'),
(2, 18, 2, 1, '50'),
(3, 17, 3, 1, '70'),
(4, 16, 3, 1, '78'),
(5, 19, 3, 1, '78'),
(6, 16, 3, 1, '78'),
(7, 16, 3, 1, '78'),
(8, 18, 3, 1, '78'),
(9, 17, 3, 1, '50'),
(10, 17, 254, 1, '43254'),
(11, 17, 3, 1, '45'),
(29, 56, 4, 1, '80');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE `socios` (
  `idsocios` int(11) NOT NULL,
  `cedula` varchar(45) DEFAULT NULL,
  `apellido_uno` varchar(45) DEFAULT NULL,
  `apellido_dos` varchar(45) DEFAULT NULL,
  `nombres` varchar(45) DEFAULT NULL,
  `estado_civil` varchar(45) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `genero` varchar(45) DEFAULT NULL,
  `otro_genero` varchar(45) DEFAULT NULL,
  `trabajo` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `m_contacto` varchar(45) DEFAULT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `sector` varchar(45) DEFAULT 'Desconocido'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`idsocios`, `cedula`, `apellido_uno`, `apellido_dos`, `nombres`, `estado_civil`, `fecha_nac`, `genero`, `otro_genero`, `trabajo`, `email`, `m_contacto`, `fecha`, `hora`, `sector`) VALUES
(16, '346', 'Sosa', 'romero', 'rodrigo José', 'Divorciado/a', '1949-03-02', 'Masculino', 'undefined', '', '', '', '2019-03-17', '06:09:30', 'San Francisco'),
(17, '222', 'Ruiz', 'romero', 'Enrique Fabricio', 'Divorciado/a', '1949-03-02', 'Masculino', 'undefined', '', '', 'undefined', '2019-03-17', '06:10:15', 'Tanquis'),
(18, '345', 'Toledo', 'romero', 'maría Inés', 'Divorciado/a', '1949-03-02', 'Masculino', 'undefined', '', '', 'undefined', '2019-03-17', '06:10:17', 'Loma alto cicalpa'),
(19, '2223', 'romero', 'tejerina', 'Rocío Porcha', 'Soltero/a', '1989-02-23', 'Masculino', 'undefined', '', '', 'undefined', '2019-03-19', '14:47:34', 'Guamote'),
(20, '454545', 'tere', 'iuyiu', 'khiuh', 'Soltero/a', '1999-05-03', 'Masculino', 'undefined', '', '', 'TelÃ©fono convencional', '2019-03-23', '17:23:57', 'Desconocido'),
(25, '908872', 'roldan', '', 'marcelo', 'Soltero/a', '1798-01-03', 'Otro-Genero', 'undefined', '', '', 'Correo electrÃ³nico', '2019-03-23', '20:26:12', 'aquil'),
(56, '20690267', 'Ronca', 'fuenza', 'José', 'Soltero/a', '1988-07-18', 'Masculino', '', 'estudiante', 'andrestejerin@gmail.com', 'Correo electrÃ³nico', '2019-03-24', '15:28:03', 'asis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono_socio`
--

CREATE TABLE `telefono_socio` (
  `idtelefono_socio` int(11) NOT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `movil` varchar(45) DEFAULT NULL,
  `secundario` varchar(45) DEFAULT NULL,
  `socios_idsocios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `telefono_socio`
--

INSERT INTO `telefono_socio` (`idtelefono_socio`, `telefono`, `movil`, `secundario`, `socios_idsocios`) VALUES
(15, '', '', '', 18),
(16, '', '', '', 19),
(17, '', '', '', 20),
(53, '3444343', '556656', '56565', 56);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `usuario` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `nombre`, `usuario`, `password`) VALUES
(1, 'admin', 'admin', 'Admin.19');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `direccion_socio`
--
ALTER TABLE `direccion_socio`
  ADD PRIMARY KEY (`iddireccion_socio`),
  ADD KEY `fk_direccion_socio_socios1_idx` (`socios_idsocios`);

--
-- Indices de la tabla `ingresos`
--
ALTER TABLE `ingresos`
  ADD PRIMARY KEY (`idingresos`),
  ADD KEY `fk_ingresos_usuarios1_idx` (`usuarios_idusuarios`),
  ADD KEY `socios_idsocios` (`socios_idsocios`),
  ADD KEY `idingresos` (`idingresos`);

--
-- Indices de la tabla `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`idsocios`);

--
-- Indices de la tabla `telefono_socio`
--
ALTER TABLE `telefono_socio`
  ADD PRIMARY KEY (`idtelefono_socio`),
  ADD KEY `fk_telefono_socio_socios_idx` (`socios_idsocios`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuarios`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `direccion_socio`
--
ALTER TABLE `direccion_socio`
  MODIFY `iddireccion_socio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT de la tabla `ingresos`
--
ALTER TABLE `ingresos`
  MODIFY `idingresos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `socios`
--
ALTER TABLE `socios`
  MODIFY `idsocios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT de la tabla `telefono_socio`
--
ALTER TABLE `telefono_socio`
  MODIFY `idtelefono_socio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `direccion_socio`
--
ALTER TABLE `direccion_socio`
  ADD CONSTRAINT `fk_direccion_socio_socios1` FOREIGN KEY (`socios_idsocios`) REFERENCES `socios` (`idsocios`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ingresos`
--
ALTER TABLE `ingresos`
  ADD CONSTRAINT `fk_ingresos_usuarios1` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_socios_idsocios1` FOREIGN KEY (`socios_idsocios`) REFERENCES `socios` (`idsocios`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `telefono_socio`
--
ALTER TABLE `telefono_socio`
  ADD CONSTRAINT `fk_telefono_socio_socios1` FOREIGN KEY (`socios_idsocios`) REFERENCES `socios` (`idsocios`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
