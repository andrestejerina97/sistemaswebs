<!DOCTYPE html>
<html lang="en">
<?php  
session_start();
if($_SESSION['autorizado']<>1){
    header("Location: index.php");
}
?>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="frontend/img/favicon1.ico">
  <title>Ovinos-Sistema de acopio</title>

  <!-- Bootstrap core CSS -->
   <link href="libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/menuinicial.css" rel="stylesheet">
  <link href="fontawesome-pro-5.7.2-web/css/all.css" rel="stylesheet">
</head>

<body>

  <div class="d-flex" id="wrapper">

 <?php
       
        $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","S�bado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha=$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
        
        ?>
    <!-- Sidebar -->
  <?php include 'frontend/sidebar.php'; ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">


     <?php include 'frontend/nav_bar.php'; ?>

      <div class="container-fluid">
        <h1 class="mt-4"></h1>



        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            REGISTRO DE USUARIOS DEL SISTEMA.
            <small></small>
          </h1>

        </section>
           <section class="project-tab">
           <div class="conteiner">
          <div class='row'>
           <div class='col-md-12'>

             <nav>

                  <ul class="nav nav-tabs pull-right">

                  <li><a href="#bajas" data-toggle="tab">Baja</a></li>
                  <li class="active"><a href="#altas" data-toggle="tab">Alta</a></li>

                  <li class="pull-left header"><i class="fa fa-file-text"></i> Operaciones con Articulos.</li>
                </ul>
</nav>
                
               <div class="tab-content">
                  <div class="tab-pane active" id="altas">
                   <form class="form-horizontal">

                    <div class='form-group'>
                    <label for="codigo" class="col-sm-2 control-label">Nombre:</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control" id='codigo' placeholder='Codigo del articulo...'>
                    </div>
                    </div>
                     <div class='form-group'>
                    <label for="codigo" class="col-sm-2 control-label">tipo:</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control" id='codigo' placeholder='Codigo del articulo...'>
                    </div>
                    </div>

                    <div class='form-group'>
                    <label for="codigo" class="col-sm-2 control-label">Usuario:</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control " id='codigo' placeholder='Codigo del articulo...'>
                    </div>
                    </div>
                                        <div class='form-group'>
                    <label for="codigo" class="col-sm-2 control-label">Password:</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control" id='codigo' placeholder='Codigo del articulo...'>
                    </div>
                    </div>



</form>
        

        </div>
         <div class="tab-pane" id="bajas">
                    <form class='form-horizontal' onkeypress="return anular(event)">
                    <div class='form-group'>
                    <label for="codigo_busqueda" class="col-sm-2 control-label">Codigo:</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control" id='codigo_busqueda' onchange="busca_articulo();" placeholder='Codigo del articulo...'>
                    </div>
                    </div>

                    <div id='info_articulo'></div>
                    <br>
                    <div class="btn-group">
                    <button type='button' class='btn btn-primary btn-lg' onclick='busca_articulo();' id='btn-buscar'><i class='fa  fa-search'></i> Buscar...</button>
                    <button type='button' class='btn btn-success btn-lg' onclick='elimina_articulo();' id='btn-procede-baja' disabled><i class='fa   fa-times'></i> Eliminar...</button>
                    <button type='button' class='btn btn-danger btn-lg' onclick='cancela_eliminacion();' id='btn-cancela-baja' disabled><i class='fa  fa-recycle'></i> Cancelar...</button>
                    </div>

                    </form>
                  </div><!-- /.tab-pane -->

        </div>
         </div>
         </div>
        </div>
          </div>
          </div>
         </section>
        <footer class="fixed-bottom">
          <div class="float-right">2019</div>
       
        </footer>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="libraries/jquery-3.3.1.min.js"></script>
    <script src="libraries/jquery.min.js"></script>
  <script src="libraries/bootstrap/js/bootstrap.bundle.js"></script>
  <script src="libraries/bootstrap/js/bootstrap.min.js"></script>
  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

  
<script type="text/javascript" src="custom/js/ocultar.js"></script> 

</body>

</html>