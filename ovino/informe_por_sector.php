<!DOCTYPE html>
<html lang="en">
<?php
session_start();
if($_SESSION['autorizado']<>1){
    header("Location: index.php");
}
?>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="frontend/img/favicon1.ico">
  <title>INFORMES POR SECTOR</title>

  <!-- Bootstrap core CSS -->
    <link href="libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/menuinicial.css" rel="stylesheet">
  <link href="fontawesome-pro-5.7.2-web/css/all.css" rel="stylesheet">
  <link href=" custom/css/style.css" rel="stylesheet">

<link href="libraries/dataTables/css/dataTables.bootstrap4.min.css" ">
<link href="libraries/responsive/css/responsive.bootstrap4.min.css" >
<link href="libraries/Buttons/css/buttons.dataTables.min.css">
</head>

<body onload="informe_por_sector();">

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
 <?php include 'frontend/sidebar.php'; ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

 <?php include 'frontend/nav_bar.php'; ?>

      <div class="container-fluid">
        <h1 class="mt-4"></h1>

        


      
        
        <div class="row justify-content-center">
          <div class="col-11 col-md-5">
            <p class="titular-principal">FORMULARIO DE INFORMES</p>
          </div>
        </div>
        
        <form >
          <section>

            <div class="container">
              <div class="row">
                <div class="col-12">
                    <div class="subtitular subrayado">
                     Listado de litros de leche por sector:
                    </div>
                </div>
              </div>
              <!--FIN SUBTITULAR-->
            </div>
            <!--FIN CONTAINER SUBTITULAR-->
            <div class="container">
              <div class="col-md-auto">
              <br>
              <a class="btn btn-primary"  href="/ovino/backend/funciones/pdf_por_sector.php" role="button"> IMPRIMIR PDF</a>
<br>
             <div id="listado_informe_por_sector"></div>
            </div>
            </div>
          </section>


        </form>
        

        
        <footer class="fixed-bottom">
          <div class="float-right">2019</div>
          <strong>Parroquia La Matriz (de prueba)</strong>  
        </footer>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>


  




    <script type="text/javascript" src="custom/js/ocultar.js"></script> 
    <script type="text/javascript" src="backend/funciones/scrips/informes.js"></script>
    <script src="libraries/jquery-3.3.1.min.js"></script>
    <script src="libraries/noty/packaged/jquery.noty.packaged.min.js"></script>  
 
     <script type="text/javascript" src="backend/funciones/scrips/informes.js"></script>
<script src="libraries/DataTables/DataTables/js/jquery.dataTables.min.js"></script>
<script src="libraries/DataTables/datatables.min.js"></script>
<script src="libraries/DataTables/DataTables/js/dataTables.bootstrap4.min.js"></script> 
<script src="libraries/DataTables/Responsive/js/dataTables.responsive.min.js"></script>
<script src="libraries/DataTables/Responsive/js/responsive.bootstrap4.min.js"></script>
<script src="libraries/DataTables/Buttons/js/dataTables.buttons.min.js"></script> 
<script src="libraries/DataTables/Buttons/js/buttons.flash.min.js"></script> 
<script src="libraries/DataTables/Buttons/js/buttons.print.min.js"></script>
 <script src="libraries/DataTables/Buttons/js/buttons.html5.min.js"></script> 
 <script src="libraries/DataTables/pdf/pdfmake.min.js"></script> 
 <script src="libraries/DataTables/pdf/vfs_fonts.js"></script> 
    
</body>

</html>
