<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicinesRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->string('home');
            $table->string('description_home')->nullable();
            $table->string('code_postal');
            $table->integer('payment');
            $table->integer('state')->default(0);
            $table->time('arrival');
            $table->float('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicines_requests');
    }
}
