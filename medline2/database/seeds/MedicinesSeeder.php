<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Float_;

class MedicinesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medicines')->insert([
            'name' => "Paracetamol comprimidos",
            'price' => 23,
            'purchase_price' => 2,
            'id_category'=>2,
            'resource'=>'Medicine-1.jpeg',
            'description'=>Str::random(9)."medicamento de prueba",
        ]);
        DB::table('medicines')->insert([
            'name' => "Ibuprofeno 600 mg",
            'price' => 45,
            'purchase_price' => 30,
            'id_category'=>2,
            'resource'=>'Medicine-2.jpeg',
            'description'=>Str::random(9)."medicamento de prueba",
        ]);
        DB::table('medicines')->insert([
            'name' => "Ranitidina 2gr",
            'price' => 223,
            'purchase_price' => 112,
            'id_category'=>2,
            'resource'=>'Medicine-3.jpeg',
            'description'=>Str::random(9)."medicamento de prueba",
        ]);
        DB::table('medicines_stock')->insert([
            'id_medicine' => 1,
            'stock' => 22,
        
        ]);
        DB::table('medicines_stock')->insert([
            'id_medicine' => 2,
            'stock' => 2,
        
        ]);
        DB::table('medicines_stock')->insert([
            'id_medicine' => 3,
            'stock' =>13,
        
        ]);
    }
}
