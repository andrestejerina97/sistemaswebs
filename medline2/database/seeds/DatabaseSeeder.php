<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(MedicinesSeeder::class);
         $this->call(MedicineCategoriesSeeder::class);
         $this->call(UsersSeeder::class);

    }
}
