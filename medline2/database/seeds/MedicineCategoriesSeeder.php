<?php

use Illuminate\Database\Seeder;

class MedicineCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medicine_categories')->insert([
            'name' => "Medicamentos",
            
        ]);
        DB::table('medicine_categories')->insert([
            'name' => "Insumos médicos",
            
        ]);
        DB::table('medicine_categories')->insert([
            'name' => "Aseo personal",
            
        ]);
        DB::table('medicine_categories')->insert([
            'name' => "Linea infantil",
            
        ]);
        DB::table('medicine_categories')->insert([
            'name' => "Cremas",
            
        ]);
        DB::table('medicine_categories')->insert([
            'name' => "Perfumes",
            
        ]);
         DB::table('medicine_categories')->insert([
            'name' => "Bebidas",
            
        ]);
         DB::table('medicine_categories')->insert([
            'name' => "Alimentos",
            
        ]);
        DB::table('medicine_categories')->insert([
            'name' => "Varios",
            
        ]);
        
    }
}
