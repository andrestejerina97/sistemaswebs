<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Administrador",
            'home' =>'Ecuador',
            'email' => 'medline2020@gmail.com',
            'code_postal'=>232,
            'password'=>Hash::make('medline2020.'),
            'user'=>'ecuador',
        ]);
        DB::table('users')->insert([
            'name' => "andres",
            'home' =>'arg',
            'email' => 'andres@gmail.com',
            'code_postal'=>232,
            'password'=>Hash::make('andres'),
            'user'=>'argentina',
        ]);
    }
}
