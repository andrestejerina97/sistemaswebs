<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\medicines_request_items;
use Illuminate\Support\Facades\Facade;

class medicines_request extends Model
{
    protected $table = 'medicines_requests';
    protected $fillable = ['id_user','code_postal','description_home','home','payment','arrival','total'];
    protected $guarded = ['id'];

public static function MedicinesRequest()
{
    $listRequest= medicines_request_items::join('medicines_requests',function($join){
        $join->on('medicines_requests.id','=','medicines_request_items.id_request');})
        ->join('users',function($join){
            $join->on('medicines_requests.id_user','=','users.id');
        })->where("medicines_requests.state",'=',0)
        ->get();


    /*$list=InternalResolution::join('users', function($join){
        $join->on('users.id',"=","internal_resolutions.user_id");})  
            ->join('subjects','subjects.id','=','internal_resolutions.subject')
            ->select('internal_resolutions.*','users.name as name_user','users.surname','subjects.subject_name')
            ->where('internal_resolutions.flag_urgent','=',0)
            ->where('internal_resolutions.status','=',$status)
            //->where('internal_resolutions.status','=',$status2)
            ->where('internal_resolutions.addresse_id','=',$id_user)
            ->get(); */
        return $listRequest;
}
}
