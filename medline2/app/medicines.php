<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medicines extends Model
{
    protected $table = 'medicines';
    protected $fillable = ['name','resource_path','description','price','purchase_price','code','id_category','resource'];
    protected $guarded = ['id'];
}
