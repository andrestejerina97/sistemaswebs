<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medicines_request_items extends Model
{
    protected $table = 'medicines_request_items';
    protected $fillable = ['id_request','id_medicines','quantity','rebate','aggregate','subtotal'];
   protected $guarded = ['id'];
}
