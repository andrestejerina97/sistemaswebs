<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\medicines;
use App\medicines_request;
use Illuminate\Support\Facades\Storage;
use App\medicine_categories;
use App\medicines_stock;
class MedicinesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
 public function index()
 {
    $medicines= medicines::join('medicines_stock',function($join){$join->on('medicines_stock.id_medicine','=','medicines.id');})->get();
    $medicinesRequest=medicines_request::MedicinesRequest();
    $categories=medicine_categories::all();
 
    
     return view('administrator.medicines.home')
     ->with('categories',$categories)
     ->with('medicines',$medicines);
 }
 public function save(Request $request)
 {
     $medicine= new medicines();
     $stock=new medicines_stock();

     $file = $request->file('file');
    // $currentUser = Auth::user();
     $medicine->name= $request->input('name');
     $medicine->code=$request->input('code');
     $medicine->id_category=$request->input('category');
     $medicine->description= $request->input('description');
     $medicine->purchase_price=$request->input('purchase_price');
     $medicine->price=$request->input('price');
     $medicine->offer_price=$request->input('offer_price');
     $medicine->resource="none";
     $medicine->save();
     $medicine->resource="Medicine-".$medicine->id.".". $file->getClientOriginalExtension();
     $medicine->update();
     $stock->id_medicine=$medicine->id;
     $stock->stock=$request->input('stock');
     $path = $file->storeAs('public/medicines',$medicine->resource);
     $medicine->resource_path=Storage::url('public/medicines/'.$medicine->resource);
     $stock->save();
   
    //indicamos que queremos guardar un nuevo archivo en el disco local
    //Storage::disk('local')->put($nombre,  \File::get($file));
    


    $medicines= medicines::join('medicines_stock',function($join){$join->on('medicines_stock.id_medicine','=','medicines.id');})->get();

    $categories=medicine_categories::all();
    
    return redirect('admin/Medicines')
    ->with('categories',$categories)
    ->with('medicines',$medicines);

 }

 public function RequestCurrent()
 {
    $medicinesRequest=medicines_request::MedicinesRequest();
 
     return View('administrator.medicines.request')
     ->with('medicinesRequest',$medicinesRequest);
}
 public function SavePurchase(request $request)
 {
    
 }
 public function update(Request $request)
 {
    $medicine= medicines:: find($request->input('idU'));
  
   // $currentUser = Auth::user();
   $medicine->name= $request->input('nameU');
   $medicine->code=$request->input('codeU');
   $medicine->id_category=$request->input('categoryU');
   $medicine->description= $request->input('descriptionU');
   $medicine->purchase_price=$request->input('purchase_priceU');
   $medicine->price=$request->input('priceU');
   $medicine->offer_price=$request->input('offer_priceU');
   $stockU=$request->input('stockU');
   $stock = medicines_stock::where('id_medicine','=',$request->input('idU'))->update(array('stock' => $stockU));
   if ($request->hasFile('fileU')) {
    Storage::delete('public/medicines/'.$medicine->resource); 
    $file = $request->file('fileU');
    $medicine->resource="Medicine-".$medicine->id.".". $file->getClientOriginalExtension();
    $path = $file->storeAs('public/medicines',$medicine->resource);
}
    
    $medicine->update();
 

    $medicines= medicines::join('medicines_stock',function($join){$join->on('medicines_stock.id_medicine','=','medicines.id');})->get();
    $categories=medicine_categories::all();
    
    return redirect('admin/Medicines')
     ->with('categories',$categories)
     ->with('medicines',$medicines);

 }
 public function delete(Request $request)
 {
    $medicine= medicines:: find($request->input('id'));
    Storage::delete('public/medicines/'.$medicine->resource);
    $stock = medicines_stock::where('id_medicine','=',$medicine->id)->delete();
    
    $medicine->delete();


    $medicines= medicines::join('medicines_stock',function($join){$join->on('medicines_stock.id_medicine','=','medicines.id');})->get();

    $categories=medicine_categories::all();
    
    return redirect('admin/Medicines')
    ->with('categories',$categories)
    ->with('medicines',$medicines);
 }
}
