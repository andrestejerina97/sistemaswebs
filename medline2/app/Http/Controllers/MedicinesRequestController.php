<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\medicines_request;
use App\medicines_request_items;
class MedicinesRequestController extends Controller
{
    public function SavePurchase(Request $request){
        $total=0;
        $medicineRequest= new medicines_request();
        $medicineRequest->id_user=1;
        $medicineRequest->home=$request->input('home');
        $medicineRequest->payment=1;
        $medicineRequest->arrival=12;
        $medicineRequest->description_home=$request->input('description');
        $medicineRequest->code_postal=$request->input('code_postal');
        $medicineRequest->total=0;
        $medicineRequest->save();
        $id_Request=$medicineRequest->id;

        for ($i=1; $i <= $request->input('itemCount') ; $i++) {
        $medicineRequest_item= new medicines_request_items(); 
        $medicineRequest_item->id_request=$id_Request;
        $elemento= "item_name_".$i;
        $medicineRequest_item->id_medicines=$request->input($elemento);
        
        $elemento= "item_price_".$i;
        $cantidad=(int)$request->input($elemento);
        $elemento= "item_quantity_".$i;
        $precio=(int)$request->input($elemento);
        
        $subtotal= (int)($cantidad * $precio);
        $total+=(int)$subtotal;       
        $medicineRequest_item->subtotal=$subtotal;
        $medicineRequest_item->quantity=$cantidad;
        $medicineRequest_item->rebate=0;
        $medicineRequest_item->aggregate=0;
            $medicineRequest_item->save();
    }
       $medicineRequest->total= $total;
       $medicineRequest->update();
       $resultado="Su pedido se registrón con éxito";
       return redirect('/Medicines')->with("resultado",$resultado);
     }
}
