<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/Medical','MedicalsController@index')->name('Medicals');
Route::get('/Profile','MedicalsController@ProfileHome')->name('MedicalProfile');
Route::get('/Hola','MedicalsController@ProfileHome')->name('Hola');
Route::get('/Recipe','MedicalsController@RecipeHome')->name('MedicalRecipe');
Route::get('/Medicines','MedicinesController@index')->name('Medicines');
Route::post('/SavePurchase','MedicinesRequestController@SavePurchase')->name('SavePurchase');
Route::namespace('admin')->group(function(){
    Route::get('/admin/home','MedicinesController@RequestCurrent')->name('HomeAdmin');
    Route::get('/admin/Medicines','MedicinesController@index')->name('MedicinesHome');
    Route::get('/admin/MedicinesRequest','MedicinesController@RequestCurrent')->name('MedicinesRequest');
    Route::post('/admin/SaveMedicines','MedicinesController@save')->name('SaveMedicines');
    Route::post('/admin/UpdateMedicines','MedicinesController@update')->name('UpdateMedicines');
    Route::post('/admin/DeleteMedicines','MedicinesController@delete')->name('DeleteMedicines');

});


Route::post('/UpdateMedical','MedicalsController@Update')->name('UpdateMedical');

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
