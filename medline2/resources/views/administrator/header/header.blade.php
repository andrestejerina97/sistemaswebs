  <!--================ Header Menu Area start =================-->
  <header class="header_area">
    <div class="main_menu">
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container box_1620">
        <a class="navbar-brand logo_h" href="{{route('HomeAdmin')}}"><img src="{{asset('img/logo.png')}}" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
            <ul class="nav navbar-nav menu_nav justify-content-end">
              <li class="nav-item active"><a class="nav-link" href="{{route('HomeAdmin')}}">Inicio</a></li> 
           
              <li class="nav-item submenu dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                  aria-expanded="false">Farmacia</a>
                <ul class="dropdown-menu">
                  <li class="nav-item"><a class="nav-link"  href="{{route('MedicinesRequest')}}">Solicitudes</a>
                    <li class="nav-item"><a class="nav-link"  href="{{route('MedicinesHome')}}" >Alta Farmacia</a>                 
                 
                </ul>
              </li>
            
              <li class="nav-item"><a class="nav-link" href={{route('HomeAdmin')}}>Médicos</a>
              <li class="nav-item submenu dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                  aria-expanded="false">Turnos</a>
                <ul class="dropdown-menu">
                  <li class="nav-item"><a class="nav-link" href="{{route('HomeAdmin')}}">Solicitudes</a>                 
                </ul>
              </li>
              
              <li class="nav-item submenu dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                  aria-expanded="false">Perfil</a>
                <ul class="dropdown-menu">
                  <li class="nav-item"><a class="nav-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();"   href="{{route('logout')}}">                    
                    {{ __('Cerrar Sesión') }}</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                      </form>
</a>


                </ul>
              </li>
            </ul>

            <div class="nav-right text-center text-lg-right py-4 py-lg-0">
            
            </div>
          </div> 
        </div>
      </nav>
    </div>
  </header>
  <!--================Header Menu Area =================-->