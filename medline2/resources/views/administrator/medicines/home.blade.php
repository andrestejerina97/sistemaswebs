<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Administrador</title>
<link rel="icon" href="{{asset('img/ICONO.ico')}}" type="image/png">

  <link rel="stylesheet" href="{{ asset('vendors/bootstrap/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{asset('vendors/fontawesome/css/all.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('vendors/linericon/style.css')}}">
<link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.theme.default.min.css')}}" >
<link rel="stylesheet" href="{{asset('vendors/owl-carousel/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/flat-icon/font/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('vendors/nice-select/nice-select.css')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('css/style.css')}}">

</head>
<body class="bg-shape">
@include('administrator.header.header')

  <!--================Hero Banner Area Start =================-->

    <!--==================Form Area -->
    <section class="bg-gray section-padding hero-banner  magic-ball ">
      <div class="section-intro text-center pb-90px">
               <div class="col-lg-12 col-md-8 mb-4 mb-md-0 ">
                <h2>Alta de medicamentos</h2>
               <form method="POST"  action="{{route('SaveMedicines')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                      <div class="form-row">
                          <div class="form-group col-md">
                              <label for="">Nombre del Medicamento:</label>
                              <input type="text" name="name" id="name" tabindex="1" class="form-control" 
                              placeholder="ingrese nombre completo" required>
                          </div>

                          <div class="form-group col-md">
                            <label for="">Casa comercial:</label>
                              <input type="text" name="description" tabindex="2" id="description" class="form-control"
                                  placeholder="ingrese una descripción para mostrar al público">
                             
                          </div>
                      </div>
                      <div class="form-row">
                          <div class="form-group col-md">
                              <label for="">Precio de compra(opcional):</label>
                              <input type="text" name="purchase_price" tabindex="3" id="purchase_price" class="form-control"
                                  placeholder="Precio compra">
                          </div>
                          <div class="form-group col-md">
                            <label class="" for="">Precio de venta normal:</label>                           
                            <input type="text" tabindex="4" id="price" name="price" class="form-control"
                                placeholder="Precio normal" required>
                          </div>
                          <div class="form-group col-md">
                            <label class="" for="">Precio de venta oferta:</label>                           
                            <input type="text" tabindex="5" id="offer_price" name="offer_price" class="form-control"
                                placeholder="Precio oferta" required>
                          </div>
                      </div>

                      <div class="form-row">
                          <div class="form-group col-md">
                              <label for="">Stock de producto(opcional):</label>
                              <input type="tel" tabindex="5" name="stock" id="stock" class="form-control" value="1" placeholder="Cantidad de unidades disponibles" required>
                          </div>
                          <div class="form-group col-md">
                            <label for="">Codigo de producto:</label>
                            <input type="tel" tabindex="6" name="code" id="code" class="form-control" placeholder="Codigo que identificará el producto">
                        </div>
                        
             </div>
             <div class="form-row">
              <div class="input-group  col-md ">
                <label class="form-label" for="">Categoría :&nbsp;</label>
                <select class="form-control " id="category" name="category">
                  @foreach ($categories as $category)
                  <option  value="{{$category->id}}">{{$category->name}}</option>
                  @endforeach
                </select>
              </div>  
            </div>
                   
                        <div class="form-group col-md">
                          <label for="">Imagen de venta(obligatorio):</label>
                          <input type="file" class="form-control" name="file" required >
                      </div>
                    
                      <div class="frow justify-content-center">
                          <button type="submit" class="button" 
                              data-dismiss="modal">Guardar</button>
                      </div>
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </form>
              </div>
          </div>
  </section>
  <!--End form area -->
   
  <!--================Hero Banner Area End =================-->
<!--================Table medicines =================-->
<section class="bg-gray  magic-ball magic-ball-testimonial pb-xl-5">
  <div class="container">
    <div class=" text-center pb-90px">
      <h2>Listado de medicamentos</h2>
      
    </div>
      <div class="testimonial__item">
        <div class="row">
          <div class="col-md-9 col-lg-12">
            <div class="testimonial__content mt-4 mt-sm-0">
              <table class="table">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Precio</th>
                    <th>Precio Compra</th>
                    <th>Precio Oferta</th>
                    <th>Stock</th>
                    <th>Código</th>
                    <th></th>
                    <th></th>
                  </tr>
                  <tbody>
                    @foreach($medicines as $medicine)
                    <tr>
                    <td>{{$medicine->id}}</td>
                    <td>{{$medicine->name}}</td>
                    <td>{{$medicine->description}}</td>
                    <td>{{$medicine->price}}</td>
                    <td>{{$medicine->purchase_price}}</td>
                    <td>{{$medicine->offer_price}}</td>
                    <td>{{$medicine->stock}}</td>
                    <td>{{$medicine->code}}</td>
                    <td>

                        <button id_med="{{$medicine->id}}" type="button " id="btn_update" class="btn btn-success btn_update"><i class="ti-pencil-alt"></i></button>  
                    </td>
                    <td>
                      <form method="post" action="{{route('DeleteMedicines')}}" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="id" id="id" value="{{$medicine->id}}">
                          <button  type="submit" class="btn btn-success"><i class="ti-trash"></i></button>
                        </form>
                      </td>
                    </tr>
                    @endForeach
                  </tbody>
                </thead>
                  </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--================End Table medicines =================-->

<!--MODAL UPDATE -->
<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="modalUpdateLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalUpdateLabel">Ya estás a un paso!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
        <form method="POST" action="{{route('UpdateMedicines')}}" accept-charset="UTF-8" enctype="multipart/form-data">
          <div class="row">
              <div class="form-group col-md">
                  <label for="">Nombre del Medicamento:</label>
                  <input type="text" name="nameU" id="nameU" tabindex="1" class="form-control" 
                  placeholder="ingrese nombre completo" required>
              </div>

              <div class="form-group col-md">
                <label for="">Casa comercial:</label>
                  <input type="text" name="descriptionU" tabindex="2" id="descriptionU" class="form-control"
                      placeholder="ingrese una descripción para mostrar al público">
                 
              </div>
          </div>
          <div class="row">
              <div class="form-group col-md">
                  <label for="">Precio de compra(opcional):</label>
                  <input type="text" name="purchase_priceU" tabindex="3" id="purchase_priceU" class="form-control"
                      placeholder="Precio compra">
              </div>
              <div class="form-group col-md">
                <label class="" for="">Precio de venta:</label>                           
                <input type="text" tabindex="4" id="priceU" name="priceU" class="form-control"
                    placeholder="Precio normal" required>
              </div>
             
          </div>
<div class="row">
  <div class="form-group col-md">
    <label class="" for="">Precio de venta oferta:</label>                           
    <input type="text" tabindex="" id="offer_priceU" name="offer_priceU" class="form-control"
        placeholder="Precio oferta" required>
  </div>
</div>
          <div class="row">
              <div class="form-group col-md">
                  <label for="">Stock de producto(opcional):</label>
                  <input type="tel" tabindex="5" name="stockU" id="stockU" class="form-control" value="1" placeholder="Cantidad de unidades disponibles" required>
              </div>
              <div class="form-group col-md">
                <label for="">Codigo de producto:</label>
                <input type="tel" tabindex="6" name="codeU" id="codeU" class="form-control" placeholder="Codigo que identificará el producto">
            </div>
            
 </div>

 <div class="form-row">
  <div class="input-group  col-md ">
    <label class="form-label" for="">Categoría :&nbsp;</label>
    <select class="form-control " id="categoryU" name="categoryU">
      @foreach ($categories as $category)
      <option  value="{{$category->id}}">{{$category->name}}</option>
      @endforeach
    </select>
  </div>  
</div>
           <div class="form-group col-md">
                          <label for="">Imagen de venta(obligatorio):</label>
                          <input type="file" class="form-control" name="fileU"  >
         </div>

           <input type="hidden" name="idU" id="idU">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
      
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">cerrar</button>
        <button type="submit" id="btn_update" class="btn btn-primary">Guardar cambios</button>
      </div>
    </form>
    </div>
  </div>
</div>
</div>


<!--MODAL UPDATE -->
  @include('administrator.footer.footer')
  




<script src="{{asset('vendors/jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('vendors/nice-select/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('js/mail-script.js')}}"></script>
<script src="{{asset('js/skrollr.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script type="text/javascript">

$(".btn_update").click(function() {
  $(this).parents("tr").find("td").each(function(){

              id = $(this).parents("tr").find("td").eq(0).html();
              name= $(this).parents("tr").find("td").eq(1).html();
	            description = $(this).parents("tr").find("td").eq(2).html();
	            price = $(this).parents("tr").find("td").eq(3).html();
	            purchase_price = $(this).parents("tr").find("td").eq(4).html();
              offer_price = $(this).parents("tr").find("td").eq(5).html();
	            stock = $(this).parents("tr").find("td").eq(6).html();
              code = $(this).parents("tr").find("td").eq(7).html();
	            $('#idU').val(id);
	            $('#nameU').val(name);
	            $('#descriptionU').val(description);
	            $('#priceU').val(price);
	            $('#purchase_priceU').val(purchase_price);
	            $('#stockU').val(stock);
              $('#codeU').val(code);
              $('#offer_priceU').val(offer_price);


});
$("#modalUpdate").modal("show");
});
</script>
</body>
</html>