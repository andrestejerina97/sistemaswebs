<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>MedLine S.A</title>
	<link rel="icon" href="img/ICONO.ico" type="image/ico">

  <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="vendors/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="vendors/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="vendors/linericon/style.css">
  <link rel="stylesheet" href="vendors/owl-carousel/owl.theme.default.min.css">
  <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
  <link rel="stylesheet" href="vendors/flat-icon/font/flaticon.css">
  <link rel="stylesheet" href="vendors/nice-select/nice-select.css">

  <link rel="stylesheet" href="css/style.css">
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="bg-shape">
@include('public.header.header')


  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner magic-ball ">
    <div class="container">
      <div class="row align-items-center text-center text-md-left">
        <div class="col-md-6 col-lg-5 mb-5 mb-md-0">
        </div>
        <div class="col-md-6 col-lg-7 col-xl-6 offset-xl-1">
          <img class="img-fluid" src="img/home/medicals.jpg" alt="">
        </div>
      </div>
    </div>
  </section>
  <!--================Hero Banner Area End =================-->
  <!--================Blog section Start =================-->
  <section class="bg-gray magic-ball-about">
    <div class="section-intro">
      <h2>MEDLINE PRODUCTOS</h2>
    </div>
    <div class="container">
      <div class="section-intro text-center ">
       <!-- <img class="section-intro-img" src="img/home/section-icon.png" alt=""> -->
        <form class="search-form" action="#">
          <div class="form-group">
            <div class="input-group">
              <input id="searchMedicines" name="searchMedicines" type="text" class="form-control" placeholder="Buscar mi medicamento">
              <div class="input-group-append">
                <span class="input-group-text"><i class="ti-search"></i></span>
              </div>
            </div>
          </div>
        </form>
              <!--aside categories -->
              <aside class="single_sidebar_widget tag_cloud_widget">
                  <h4 class="widget_title">Categorías</h4>
                  <ul class="list cat-list">
                    @foreach ($categories as $category)  
                    <li>
                      <a href="#">{{$category->name}}</a>
                  </li>
                    @endforeach 
                    
                  </ul>
                </aside>
              
                <a class="button " id="paymentFinalice" href="#"> <i class="ti-shopping-cart"></i> Finalizar (<span id="simpleCart_quantity" class="simpleCart_quantity"></span>) </a>
                <a href="javascript:;" class="simpleCart_empty ">Vaciar carrito </a>
        <!-- End aside categories -->
        <!--aside payment -->
      </div>
    </div>
          <!-- end aside payment -->
    </section>
    <!-- -------------------------------------------------------- -->       
           
<section class="">
  <div class="section-med ">

      @php
      $i = 0
      @endphp
@foreach ($medicines as $medicine)

@if ( ($i== 3) or $loop->first ) 

<div class="row">
  <div class="col-md-6 col-lg-4 mb-4 mb-lg-0 ">
    <div class="card-blog">
      <img class="card-img rounded-0" src="{{ asset('storage/medicines/'.$medicine->resource) }}" alt="">
      <div class="card-blog-body simpleCart_shelfItem">
        <h3 class="item_price">${{$medicine->price}} </h3>
        <a href="#">
          <h4  style="display:none" class="item_name" >{{$medicine->id}}</h4>
          <h4  >{{$medicine->name}}</h4>
        </a>
        <input style="display:none" value="{{$medicine->name}}"  class="item_nombre">
        <input type="text" class="item_quantity form-control" value="1">
    
        <ul class="card-blog-info">
          <a class="item_add payment" href="javascript:;"><i class="ti-shopping-cart">Añadir</i></a>
        </ul>
      </div>
    </div>
    </div>

@else
<div class="col-md-6 col-lg-4 mb-4 mb-lg-0 ">
<div class="card-blog">
  <img class="card-img rounded-0" src="{{ asset('storage/medicines/'.$medicine->resource) }}" alt="">
  <div class="card-blog-body simpleCart_shelfItem">
    <h3 class="item_price">${{$medicine->price}} </h3>
    <a href="#">
      <h4  style="display:none" class="item_name" >{{$medicine->id}}</h4>
      <h4  >{{$medicine->name}}</h4>
    </a>
    <input style="display:none" value="{{$medicine->name}}"  class="item_nombre">
    <input type="text" class="item_quantity form-control" value="1">

    <ul class="card-blog-info">
      <a class="item_add payment" href="javascript:;"><i class="ti-shopping-cart">Añadir</i></a>
    </ul>
  </div>
</div>
</div>

@if ( ($i==3) or ($loop->remaining==0))

</div>
@php
$i = 0
@endphp
@endif
    
@endif
@php
$i ++
@endphp
@endforeach
     

</div>
</section>
  <!--================Blog section End =================-->

  <input type="hidden" class=" item_token" id="_token" name="_token" value="{{ csrf_token() }}">
<!-- -->
<!-- Modal -->
<div class="modal fade" id="modalPayment" tabindex="-1" role="dialog" aria-labelledby="modalPaymentLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalPaymentLabel">Listado de productos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
          <h4 class="widget_title">Detalles de la compra:</h4>
          <div class="simpleCart_items" >
          </div>
     
          -----------------------------<br/>
          Final Total: <span id="simpleCart_grandTotal" class="simpleCart_grandTotal"></span> <br />
        
          
         
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">cerrar</button>
        <button type="button"  id="btn_confirmar1" class="btn btn-primary ">Confirmar Compra</button>
      </div>
    </div>
  </div>
</div>
</div>
<!-- -->
<!-- Modal -->
<div class="modal fade" id="modalPayment2" tabindex="-1" role="dialog" aria-labelledby="modalPayment2Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalPayment2Label">Ya estás a un paso!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
        <section class="bg-gray section-padding magic-ball ">
          <div class="section-intro text-center pb-20px">
                   <div class="col-lg-12 col-md-8 mb-4 mb-md-0 ">
                    <h2>Dinos como llegar a tí</h2>
                   <form method="POST" action="{{route('SaveMedicines')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                          <div class="row">
                              <div class="form-group col-md">
                                  <label for="">Escribe tu dirección:</label>
                                  <input type="text" name="home" id="home" class="form-control" 
                                  placeholder="Dirección completa" required>
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-md">
                                <label for="">Descripción </label>
                                  <input type="text" name="description" id="description" class="form-control"
                                      placeholder="Danos alguna referencia útil">
                                 
                              </div>
                            </div>
                          <div class="row">
                              <div class="form-group col-md">
                                  <label for="">Código postal:</label>
                                  <input type="text" name="code_postal" id="code_postal" class="form-control"
                                      placeholder="CP" required>
                              </div>
                            </div>

                      </form>
                  </div>
              </div>
      </section>
          
         
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">cerrar</button>
        <button type="button" id="btn_confirmar2" class="btn btn-primary simpleCart_checkout">Confirmar Compra</button>
      </div>
    </div>
  </div>
</div>
</div>
<!-- -->


  @include('public.footer.Medical')

  <script src="vendors/jquery/jquery-3.2.1.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
  <script src="vendors/nice-select/jquery.nice-select.min.js"></script>
  <script src="js/jquery.ajaxchimp.min.js"></script>
  <script src="js/mail-script.js"></script>
  <script src="js/skrollr.min.js"></script>
  <script src="js/main.js"></script>
  <script src="js/simpleCart.min.js"></script>
  <script type="text/javascript">

  $("#paymentFinalice").click(function() {
    $("#modalPayment").modal("show");
  })
  $("#btn_confirmar1").click(function() {
    $("#modalPayment1").modal("toggle");
    $("#modalPayment2").modal("show");
  })
  simpleCart.empty();
  simpleCart.bind("beforeCheckout", function(data) {
 data._token=$("#_token").val();
 data.home=$("#home").val();
 data.code_postal=$("#code_postal").val();
 data.description=$("#description").val();
  });
    simpleCart({
      cartColumns: [
        {attr: "nombre",label:'nombre'},
        
        {attr: "price",label:'precio',view: 'currency'},
        {attr:'quantity',label:'cant'},
	    	{ view: "decrement" , label: false , text: "-" } ,
        { view: "increment" , label: false , text: "+" } ,

 
      ],
      cartStyle: "table", 
      checkout: {
        type: "SendForm",
        url: 'SavePurchase',
        extra_data: {
        
        }
      }
    });

  </script>
</body>
</html>