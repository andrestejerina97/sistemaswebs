<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Safario Travel - Home</title>
    <link rel="icon" href="img/Fevicon.png" type="image/png">

    <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/flat-icon/font/flaticon.css">
    <link rel="stylesheet" href="vendors/nice-select/nice-select.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="css/style.css">
</head>

<body class="bg-shape">
    @include('public.header.Medical')

    <!--================Hero Banner Area Start =================-->

    <div class="container">
        <div class="hero-banner">
            <div class="owl-carousel owl-theme testimonial pb-xl-5">
                <div class="testimonial__item">
                    <div class="row">
                        <div class="col-md-3 col-lg-2 align-self-center">
                            <div class="testimonial__img">
                                <img class="card-img rounded-0" src="img/testimonial/t-slider1.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-10">
                            <div class="testimonial__content mt-3 mt-sm-0">
                                <h3></h3>
                                <p></p>
                                <p class="testimonial__i"></p>
                                <span class="testimonial__icon"><i class="ti-quote-right"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--================Hero Banner Area End =================-->

        <!--==================Form Area -->
        <section class="bg-gray section-padding magic-ball magic-ball-about ">
            <div class="container ">
                     <div class="col-lg-7 col-md-6 mb-4 mb-md-0 ">
                     <form method="POST" action="{{route('UpdateMedical')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group col-md">
                                    <label for="">Apellido:</label>
                                    <input type="text" id="apellido" name="apellido" class="form-control"
                                        placeholder="ingrese apellido">
                                </div>

                                <div class="form-group col-md">
                                    <label class="" for="">Nombres:</label>
                                    <input type="text" name="nombres" id="nombres" class="form-control" placeholder="ingrese nombre completo">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md">
                                  <label for="">CÉDULA:</label>
                                  <input type="text" name="dni" id="dni" class="form-control"
                                      placeholder="ingrese cédula sin puntos..">
                                </div>
                                <div class="form-group col-md">
                                    <label for="">Domicilio:</label>
                                    <input type="text" name="domicilio" id="domicilio" class="form-control"
                                        placeholder="ingrese Domicilio">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md">
                                    <label for="">Teléfono:</label>
                                    <input type="tel" name="telefono" id="telefono" class="form-control" placeholder="ingrese Teléfono">
                                </div>
                                <div class="form-group col-md">
                                    <label for="">Mail:</label>
                                    <input type="text" name="mail" id="mail" class="form-control" placeholder="ingrese Mail">
                                </div>

                            </div>
                            <div class="row justify-content-center">
                                <button type="button" class="btn btn-success" 
                                    data-dismiss="modal">Guardar</button>


                            </div>
                        </form>
                    </div>
                </div>
        </section>
        <!--End form area -->
        <!--================Search Package section Start =================-->
  <section class="section-margin">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-xl-5 align-self-center mb-5 mb-lg-0">
          <div class="search-content">
            <h2>Search suitable <br class="d-none d-xl-block"> and affordable plan <br class="d-none d-xl-block"> for your tour</h2>
            <p>Make she'd moved divided air. Whose tree that replenish tone hath own upon them it multiply was blessed is lights make gathering so day dominion so creeping</p>
            <a class="button" href="#">Learn More</a>
          </div>
        </div>
        <div class="col-lg-6 col-xl-6 offset-xl-1">
          <div class="search-wrapper">
            <h3>Search Package</h3>

            <form class="search-form" action="#">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Recipient's username">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="ti-search"></i></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <select name="category" id="category">
                  <option value="disabled" disabled selected>Category</option>
                  <option value="8 AM">8 AM</option>
                  <option value="12 PM">12 PM</option>
                </select>
              </div>
              <div class="form-group">
                <select name="tourDucation" id="tourDuration">
                  <option value="disabled" disabled selected>Tour duration</option>
                  <option value="8 AM">8 AM</option>
                  <option value="12 PM">12 PM</option>
                </select>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <input type="date" class="form-control">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="ti-notepad"></i></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <select name="priceRange" id="priceRange">
                  <option value="disabled" disabled selected>Price range</option>
                  <option value="8 AM">8 AM</option>
                  <option value="12 PM">12 PM</option>
                </select>
              </div>
              <div class="form-group">
                <button class="button border-0 mt-3" type="submit">Search Package</button>
              </div>
            </form>
          </div>

        </div>
      </div>
    </div>
  </section>
  <!--================Search Package section End =================-->

        @include('public.footer.Medical')





        <script src="vendors/jquery/jquery-3.2.1.min.js"></script>
        <script src="vendors/bootstrap/bootstrap.bundle.min.js"></script>
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="vendors/nice-select/jquery.nice-select.min.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/mail-script.js"></script>
        <script src="js/skrollr.min.js"></script>
        <script src="js/main.js"></script>
</body>

</html>