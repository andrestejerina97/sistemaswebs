<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Simulador de ventas</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top" onload="comboComplejos();">

  <!-- Page Wrapper -->
  <div id="wrapper">

  <?php include 'sidebar.php' ?>


    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

<?php include 'navbar.php' ?>
   

<section>       <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="row justify-content-center">
          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">VENTAS</h1>
      </div>

        <div class="row justify-content-center">
         <form >
         <div class='input-group'>
                    <input class="form-control dni" id="buscarDni"  onkeypress="pulsar(event);"  placeholder='Ingresar DNI del cliente'> 
                    <button type='button' class='btn btn-success btn-sm' onclick='buscar_cliente();' id='btn-buscar-cambio'><i class='fa fa-search'></i>Buscar...</button>
                    </div>    
                </form>
       
</div>

          

</div>
<br>

</section >
     
<section  id="seccionVentaAlquiler" style="display:none" >
     <div class="container-fluid">

<div class="row justify-content-center">


          <div class="container col-md-6">
          <form >
          <div  id="tabla_clientes"></div>
           
           <table class='table'>
 <thead class=' thead-dark'>
 <tr>
 <th>DATOS DE OPERACIÓN</th>    
 </tr>
 </thead>
</table >
            <div class="form-group">
           <p><b>Seleccione Complejo:</p></b>
           <select id="comboComplejos" class="form-control" >
      
           </select>
           </div> 
           <div class="form-group">
           <p><b>Seleccione Departamento:</p></b>
           <select id="comboDepartamentos" class="form-control" >
      
           </select>
           </div>
          

           <div class="form-group">
          <p><b>Precio mensual de alquiler($):</p></b>
          <input id="costoMensual" class="form-control money">
          </div>
          <div class="form-group">
         <p><b>Tiempo de contrato:</p></b>

         <select id="comboContrato" class="form-control" >
           <option value="6">6 MESES</option>
           <option value="12">1 AÑO</option>
           <option value="24">2 AÑOS</option>
      
      </select>



          </div>



            </form>
      

<br><br>

<table class='table'>
 <thead class=' thead-dark'>
 <tr>
 <th>DATOS DE FINANCIACIÓN</th>    
 </tr>
 </thead>
</table >
        <form id="form1" onchange="calcularVenta();" action="">
        
        <div class="form-group">
          <p><b>Costo inicial de alquiler($):</p></b>
          <input id="precio" placeholder="cuota mensual + depósito+ llave, etc" class="form-control money">
          </div>
         <div class="form-group">
          <p><b>Entrega($):</p></b>
          <input id="entrega" class="form-control money">

          </div>
         

          <div class="form-group">
           <p><b>Cantidad de cuotas:</p></b>
          <input type="number" id="cuotas" class="form-control ">
           </div> 
           <div class="form-group">
           <p><b>Tasa de interés(%):</p></b>
         <input id="interes" class="form-control">
           </div> 
      
           <div class="card ">
  <div class="card-body">
    <h5 class="card-title"></h5>
    <h5 class="card-subtitle mb-2 text-muted" id="resultadoVenta"></h5>

  </div>
</div>
<div class="form-group">
			<p><b>Fecha primer pago:</b></p>
      </div>
      
			<div class="form-group">
				<input type="date" class="form-control" id="primerpago" placeholder="aaaa/mm/dd" >
			</div>
           

<br>
          <div class="btn-group">
        
            <button type='button'data-target="#modal_confirmar_venta" data-toggle="modal" class='btn btn-raised btn-primary btn-lg'>Confirmar</button>
        <br><br>
            <button class="btn btn-primary" style="display:none" onclick="imprimir_pdf()"> imprimir </button>
          </div>
          </form>

            </div>
            </div>
          
        


        </div>
        <!-- /.container-fluid -->
</section>

      </div>
      <!-- End of Main Content -->


      <!-- Footer -->
 <?php include 'footer.php' ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->



<!-- Logout Modal-->
<div class="modal fade" id="modal_confirmar_venta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea confirmar la compra?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
        <button class="btn btn-primary" onclick="procesa_venta_alquiler();" type="submit">Confirmar </button>

        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
        
        </div>
      </div>
    </div>
  </div>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script src="controlador/ventas.js"></script>
  <script src="plugins/inputMask/dist/jquery.mask.min.js"> </script>

</body>

</html>
