<?php

session_start();

if ($_SESSION['tipo']> 0) {
    
}else{

   header('Location: index.php');
};

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Inicio- inmobiliaria</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top" onload="comboComplejos();">

  <!-- Page Wrapper -->
  <div id="wrapper">

  <?php include 'sidebar.php' ?>



    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

<?php include 'navbar.php' ?>
<section>
<div class="container-fluid">

          <div class="row justify-content-center">
          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">CARGA DE Alquileres</h1>
      </div>
    
        <div class="row justify-content-center">

<form >
  <label class="control-label"> Seleccione complejo:</label>
  <div class="input-group" >
    
    
    <select onchange="habilitar();" class="form-control" id="comboComplejos">
    
    </select>

    <button class=" btn btn-success" type="button" data-target="#modal_nuevo_complejo" data-toggle="modal">Nuevo complejo</button>

  
     </div>

<div class="row " id="departamento" style="display: none" >
<br>
   <label class="control-label">Departamento:</label>
  
  <div class="input-group">
    <div class="input-group col-md">
    <label for="">Número:</label>
    <input type="text" id="numero" class="form-control">
    </div>
    <div class="input-group col-md">
      <label for="">Obervacion:</label>
      <input type="text" id="observacion" class="form-control">
  </div>
  </div>
<br>
  <div class="row justify-content-center"> 
  <button class=" btn btn-primary" type="button" onclick="cargarDepartamento();">Guardar</button>

  </div>
</div>

</form>
</div>
  <div id="tabla_alquileres"></div>
  </div>


 </section>




 <div class="modal fade" id ="modal_nuevo_complejo" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">

              <h4 class="modal-title">Ingrese nuevo cliente:</h4>       
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
           
          </div>
          <div class="modal-body">
           <div class="row justify-content-center">
     
           </div>
            <form action="">
          

              <div class="form-group col-md">
                <label class="" for="">Nombre:</label>
                <input type="text" id="nombre" class="form-control" placeholder="ingrese nombre....">
                          </div>

<div class="row justify-content-center">
     </div>
            </form>
          </div>
          <div class="modal-footer">
          <button type="button" onclick="cargar()" class="btn btn-success" data-dismiss="modal">Guardar</button>

            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
 <?php include 'footer.php' ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

 


  <!-- Scroll to Top Button-->

  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>




  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>





  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
<script src="controlador/alquiler.js"></script>
</body>

</html>
