
     
<section  id="seccionVentaAlquileres" style="display:none" >
     <div class="container-fluid">

<div class="row justify-content-center">


          <div class="container col-md-6">
          <form >
          <div  class="tabla_clientes"></div>
           
           <table class='table'>
 <thead class=' thead-dark'>
 <tr>
 <th>DATOS DE OPERACIÓN</th>    
 </tr>
 </thead>
</table >
            <div class="form-group">
           <p><b>Seleccione Complejo:</p></b>
           <select id="comboComplejos" class="form-control" >
      
           </select>
           </div> 
           <div class="form-group">
           <p><b>Seleccione Departamento:</p></b>
           <select id="comboDepartamentos" class="form-control" >
      
           </select>
           </div>
          

           <div class="form-group">
          <p><b>Precio mensual de alquiler($):</p></b>
          <input id="costoMensual" class="form-control money">
          </div>
          <div class="form-group">
         <p><b>Tiempo de contrato:</p></b>

         <select id="comboContrato" class="form-control" >
           <option value="6">6 MESES</option>
           <option value="12">1 AÑO</option>
           <option value="24">2 AÑOS</option>
      
      </select>



          </div>



            </form>
      

<br><br>

<table class='table'>
 <thead class=' thead-dark'>
 <tr>
 <th>DATOS DE FINANCIACIÓN</th>    
 </tr>
 </thead>
</table >
        <form id="form1" onchange="calcularVenta();" action="">
        
        <div class="form-group">
          <p><b>Costo inicial de alquiler($):</p></b>
          <input id="precio" placeholder="cuota mensual + depósito+ llave, etc" class="form-control money">
          </div>
         <div class="form-group">
          <p><b>Entrega($):</p></b>
          <input id="entrega" class="form-control money">

          </div>
         

          <div class="form-group">
           <p><b>Cantidad de cuotas:</p></b>
          <input type="number" id="cuotas" class="form-control ">
           </div> 
           <div class="form-group">
           <p><b>Tasa de interés(%):</p></b>
         <input id="interes" class="form-control">
           </div> 
      
           <div class="card ">
  <div class="card-body">
    <h5 class="card-title"></h5>
    <h5 class="card-subtitle mb-2 text-muted" id="resultadoVenta"></h5>

  </div>
</div>
<div class="form-group">
			<p><b>Fecha primer pago:</b></p>
      </div>
      
			<div class="form-group">
				<input type="date" class="form-control" id="primerpago" placeholder="aaaa/mm/dd" >
			</div>
           

<br>
          <div class="btn-group">
        
            <button type='button'data-target="#modal_confirmar_venta" data-toggle="modal" class='btn btn-raised btn-primary btn-lg'>Confirmar</button>
        <br><br>
            <button class="btn btn-primary" style="display:none" onclick="imprimir_pdf()"> imprimir </button>
          </div>
          </form>

            </div>
            </div>
          
        


        </div>
        <!-- /.container-fluid -->
</section>




<!-- Logout Modal-->
<div class="modal fade" id="modal_confirmar_venta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea confirmar la compra?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
        <button class="btn btn-primary" onclick="procesa_venta_alquiler();" type="submit">Confirmar </button>

        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
        
        </div>
      </div>
    </div>
  </div>

