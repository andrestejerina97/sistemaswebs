<?php

session_start();

if ($_SESSION['tipo']> 0) {
    
}else{

   header('Location: index.php');
};

?>
<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Simulador de ventas</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top"  onload="comboVehiculo();">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php include 'sidebar.php' ?>


    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include 'navbar.php' ?>

        <div class="container-fluid" >

          <section>
            <!-- Begin Page Content -->

            <div class="row justify-content-center">
              <!-- Page Heading -->
              <h1 class="h3 mb-4 text-gray-800">VENTAS</h1>
            </div>

            <div class="row justify-content-center">
              <form>
                <div class='input-group'>
                  <input class="form-control dni" id="buscarDni" onkeypress="pulsar(event);"
                    placeholder='Ingresar DNI del cliente'>
                  <button type='button' class='btn btn-success btn-sm' onclick='buscar_cliente();'
                    id='btn-buscar-cambio'><i class='fa fa-search'></i>Buscar...</button>
                </div>
                
                <p><b>TIPO DE VENTA:</p></b>
<select onchange="cb_tipo_de_venta();" class="form-control" id="comboTipoDeVenta">
  <option value="vehiculos">VEHICULOS</option>
  <option value="alquileres">ALQUILERES</option>
  </select>
               
              </form>

            </div>

            <br>

          </section>

   <?php include 'venta_oficial_alquiler.php' ?>

<section id="seccionVentaVehiculos" style="display:none">

            <div class="row justify-content-center ">
            
  
              <form id="form1">
              <div  class="tabla_clientes" ></div>
           
              <table class='table'>
    <thead class=' thead-dark'>
    <tr>
    <th>DATOS DE OPERACIÓN</th>    
    </tr>
    </thead>
</table >
                <div class="row">
                  <div class="form-group col-md">
                    <p><b>Seleccione Vehiculo:</p></b>
                    <select id="comboVehiculos" class="form-control">
                    </select>
                  </div>
                  <div class="form-group col-md">
                    <p><b> Precio del vehiculo($):</p></b>
                    <input id="precioI" class="form-control">
                  </div>
                </div>
                <input id="preciov" type="hidden" class="form-control">
                <div class="row"> 
                <div class="form-group col-md">
                  <p><b>Entrega:</b></p>                  
                  <select id="tipoEntrega" class="form-control">
                    <option value="efectivo" selected=""> efectivo </option>
                    <option value="vehiculo"> vehiculo usado </option>
                    <option value="otro"> otro </option>
                  </select> 	
                  </div>
                  <div class="form-group col-md">
                  <p><b>valor:</b></p>    
                  <div class="input-group">
                  <input  placeholder="Valor de entrega en $" onchange="calcular_entregaV();" class="form-control entregaParcial">

                  <button class="btn btn-facebook add_more"  type="button"  >+</button>

                </div>

                  </div>
            

                
                </div>
                <div  id="input2"> </div>
                <table class='table'>
    <thead class='thead-dark'>
    <tr>
    <th>DATOS DE FINANCIACIÓN</th>    
    </tr>
    </thead>
</table >

              </form >
              </div>
              <div class="row justify-content-center ">
                  <form onchange="calcularVentaV();" id="form2" >
                  <div class="row">        
                  <div class="form-group col-md">
                    <p><b>Monto de entrega($):</p></b>
                    <input id="entregav" readonly="false" class="form-control money">
                  </div>
                  <div class="form-group col-md">
                    <p><b>Saldo:</p></b>
                    <input id="saldov" readonly="false" class="form-control money">
                  </div>
                  </div>
                  <div class="row">
                  <div class="form-group col-md">
                    <p><b> Cantidad de cuotas:</p></b>
                    <input type="number" id="cuotasv" class="form-control ">
                  </div>
                  <div class="form-group col-md">
                    <p><b> Tasa de interés(%):</p></b>
                    <input id="interesv" class="form-control">
                  </div>
                  </div>
                  <div class="form-group">
                    <p><b>Fecha primer pago:</b></p>
                  </div>
                  <div class="form-group">
                    <input type="date" class="form-control" id="primerpagov" placeholder="aaaa/mm/dd">
                  </div>
                  <div class="card ">
                    <div class="card-body">
                      <h5 class="card-title"></h5>
                      <h5 class="card-subtitle mb-2 text-muted" id="resultadoVentav"></h5>

                    </div>
                  </div>


                  <br>
                  <div class="btn-group">

                    <button type='button' data-target="#modal_confirmar_ventav" data-toggle="modal"
                      class='btn btn-raised btn-primary btn-lg'>Confirmar</button>
                  </div>
              
                </form>
              </div>
              
         


            <!-- /.container-fluid -->
          </section>
        </div>

      </div>
      <!-- End of Main Content -->


      <!-- Footer -->
      <?php include 'footer.php' ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->



  <!-- Logout Modal-->
  <div class="modal fade" id="modal_confirmar_ventav" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea confirmar la compra?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="procesa_venta();" type="submit">Confirmar </button>

          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>

        </div>
      </div>
    </div>
  </div>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script src="controlador/ventas.js"></script>
  <script src="plugins/inputMask/dist/jquery.mask.min.js"> </script>

</body>

</html>
