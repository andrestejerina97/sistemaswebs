<?php if(!isset($_SESSION)) { session_start(); 

} 
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Inicio- inmobiliaria</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link rel="stylesheet" href="plugins/tabla/dist/bootstrap-table.min.css">

</head>

<body id="page-top" onload="lista_gastos('pendientes'); lista_gastos('pagados');">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php include 'sidebar.php' ?>



    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php include 'navbar.php' ?>

        <section>
          <!-- Begin Page Content -->
          <div class="container-fluid">

            <div class="row justify-content-center">
              <!-- Page Heading -->
              <h1 class="h3 mb-4 text-gray-800">Gastos</h1>
            </div>

            <div class="row justify-content-center">
            <button class="btn btn-primary"data-target="#modal_nuevo_gasto" data-toggle="modal" >Cargar nuevo gasto</button>
 
            <form>



              </form>
            </div>


            <div class="col-md-12">
              <h4>Gastos pendientes</h4>
      <table id="tabla_gastos_pendientes"   
  data-toggle="table"
  data-pagination="true"
    style="width:80%">
  
<thead class="thead-dark" >
<tr>
    <th>DESCRIPCIÓN</th>
    <th>FECHAS</th>
    <th>MONTO</th>
    <th>ESTADO</th>
    <th>OPERACIÓN</th>
  </tr> 
 </thead>
 <tbody id="cuerpo_gastos_pendientes">
 
 </tbody> 
</table>
</div>

<br><br>
<div class="col-md-12">
              <h4>Gastos pagados</h4>
<table id="tabla_gastos_pagados"   
  data-toggle="table"
  data-pagination="true"
    style="width:80%">
  
<thead class="thead-dark" >
<tr>
    <th>DESCRIPCIÓN</th>
    <th>FECHAS</th>
    <th>MONTO</th>
    <th>ESTADO</th>
    <th>OPERACIÓN</th>




  </tr> 
 </thead>
 <tbody id="cuerpo_gastos_pagados">
  
 </tbody> 
</table>

</  div>
</div>

 

        </section>
    
        </div>
        <!-- End of Main Content -->
        <!-- Footer -->
        <?php include 'footer.php' ?>
        <!-- End of Footer -->

      </div>
      <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->




    <!-- Scroll to Top Button-->

    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>


    <div class="modal fade" id ="modal_nuevo_gasto" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">

              <h4 class="modal-title">Complete la información necesaria:</h4>       
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
           
          </div>
          <div class="modal-body">
           <div class="row justify-content-center">
     
           </div>
            <form action="modelo/alta_gasto.php" method="POST">
          
              <div class="form-group col-md">
                <label class="" for="">descripción del gasto:</label>
                <input type="text" id="descripcion" class="form-control" placeholder="ingrese descripcion breve del gasto....">
                          </div>
                          <div class="form-group col-md">
                <label class="" for="">monto:</label>
                <input type="tel" id="monto" class="form-control" placeholder="ingrese monto....">
 
                          </div>
                          <div class="form-group col-md">
                <label class="" for="">Estado del gasto:</label>
               
                <select class="form-control" id="estado">
                  <option value="pendiente">pendiente</option>
                  <option value="pagado">pagado</option>  
                </select>
                          </div>
                          

<div class="row justify-content-center">
     </div>
            </form>
          </div>
          <div class="modal-footer">
          <button type="submit" onclick="alta_gasto();" class="btn btn-success" data-dismiss="modal">Guardar</button>

            <button type="button"  class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


<!-- Logout Modal-->
<div class="modal fade" id="modal_confirmar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea confirmar el pago?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
        <button class="btn btn-primary" onclick="alta_pago();" type="submit">Confirmar </button>

        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
        
        </div>
      </div>
    </div>
  </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
    <script src="plugins/tabla/dist/bootstrap-table.min.js"></script>
  <script src="plugins/tabla/dist/bootstrap-table-es-AR.min.js"></script>
  <script src="controlador/gastos.js"></script>
  <script src="plugins/inputMask/dist/jquery.mask.min.js"> </script>
</body>

</html>
