<?php

session_start();

if ($_SESSION['tipo']> 0) {
    
}else{

   header('Location: index.php');
};

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Inicio- inmobiliaria</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="plugins/tabla/dist/bootstrap-table.min.css">


  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top" onload='tabla_clientes();'>

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php include 'sidebar.php' ?>
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include 'navbar.php' ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="row justify-content-center">
            <h1 class="h3 mb-4 text-gray-800">CARGA DE CLIENTES</h1>
          </div>





        
            <form>
              <div class="row justify-content-center">
                
               
                  <div class="col-sm-4">
                    <button type='button' class='btn btn-primary ' data-target="#modal_nuevo_cliente"
                      data-toggle="modal" id='btn-buscar-cambio'><i class=' fas fa-man'></i>Nuevo cliente</button>
                  
                </div>
              </div>
            </form>



<br> <br>
          <div class=" col-md-12" id="tabla_cliente">
          
          </div>

<div id="modal_cliente"></div>

          <div class="modal fade" id="modal_nuevo_cliente" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">

                  <h4 class="modal-title">Ingrese nuevo cliente:</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                </div>
                <div class="modal-body">
                  <div class="row justify-content-center">

                  </div>
                  <form id="alta_clientes">
                    <div class="row">
                      <div class="form-group col-md">
                        <label for="">Apellido:</label>
                        <input type="text"id="apellido" class="form-control" placeholder="ingrese apellido">
                      </div>

                      <div class="form-group col-md">
                        <label class="" for="">Nombres:</label>
                        <input type="text" id="nombres" class="form-control" placeholder="ingrese nombre">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-md">
                        <label for="">DNI:</label>
                        <input type="text" id="dni" class="form-control" placeholder="ingrese DNI sin puntos..">
                      </div>
                      <div class="form-group col-md">
                        <label for="">Domicilio:</label>
                        <input type="text" id="domicilio" class="form-control" placeholder="ingrese Domicilio">
                      </div>
                    </div>

                    <div class="row">
                      <div class="form-group col-md">
                        <label for="">Teléfono:</label>
                        <input type="tel" id="telefono" class="form-control" placeholder="ingrese Teléfono">
                      </div>
                      <div class="form-group col-md">
                        <label for="">Mail:</label>
                        <input type="text" id="mail" class="form-control" placeholder="ingrese Mail">
                      </div>

                    </div>
                    <div class="row justify-content-center">
                      <button type="button"  class="btn btn-success"  onclick="alta_cliente();" data-dismiss="modal">Guardar cliente</button>


                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->

<!-- Logout Modal-->
<div class="modal fade" id="modal_eliminar_cliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea eliminar este cliente cargado?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
        <button class="btn btn-primary" type="button" id="btn_delete_cliente" onclick="delete_cliente_completo();" >Confirmar </button>

        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
        
        </div>
      </div>
    </div>
  </div>




        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <?php include 'footer.php' ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->




  <!-- Scroll to Top Button-->

  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>



  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script src="plugins/tabla/dist/bootstrap-table.min.js"></script>
  <script src="plugins/tabla/dist/bootstrap-table-locale-all.min.js"></script>
  <script src="plugins/tabla/dist/bootstrap-table-es-AR.min.js"></script>
  <script src="controlador/clientes.js"></script>

</body>

</html>
