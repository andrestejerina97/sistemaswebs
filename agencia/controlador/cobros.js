
function buscar_cliente() {
    var dni = $("#buscarDni").val();
    $(document).ready(function () { //esto sirve para controlar todo el dom desde que carga 
      //aqui empieza la peticion ajax  
      $.ajax({
        beforeSend: function () {
  
        },
        url: 'modelo/cobros.php',
        type: 'POST',
  
        data: '&dni=' + dni + '&tipo='+'cuotas_mora',
  
        success: function (datos) {
  
  
          if (datos == 0) {
  
            alert("NO HAY CLIENTES CON ESE DNI REGISTRADOS,PUEDE REGISTRAR UNO NUEVO");
  
  
  
          } else {
            $("#tabla_cuotas_mora").html(datos);
            $("#tabla_cuota_mora").bootstrapTable({
              pagination: true,
              search: true});

  
          }
  
        },
        /**************************/
        error: function (jqXHR, estado, error) {
          alert("cuidado:" + estado + " " + error);
        }
      }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos
  
    });
  
  
  }

 function cb_cobros(valor){
tabla_general();
  }
  function pulsar2(q) {
    if (q.keyCode === 13 && !q.shiftKey) {
      q.preventDefault();
     buscar_cliente()
  
    }
  }
  function pulsar(q) {
    if (q.keyCode === 13 && !q.shiftKey) {
      q.preventDefault();
      
  
  var dni=$("#buscarDni").val();
  $("#dni").val(dni)
      tabla_general();
  
    }
  }

function tabla_general() {
 
  $("#combo_cobros").show();
  $("#cartel").hide();
$("#resultado_total").html("");
$("#tabla_cobros_ventas").hide();
$("#tabla_cobros_venta").hide();
var cobro= $("#combo_cobros option:selected").val();
  var dni=$("#buscarDni").val();
 $("#dni").val(dni)
  var datos={
    dni:dni,
    tipo:"tabla_general",
    cobro:cobro,
  };
  $.ajax({
 
    url: 'modelo/cobros.php',
    type: 'POST',

    data: datos,

    success: function (datos) {


      if (datos == 0) {

        alert("No hay ventas registradas para este cliente dni:"+dni);



      } else {
        $("#tabla_cobro").show();
        $("#tabla_cobros").show();
   
        $("#tabla_cobro").html(datos);
        $("#tabla_cobros").bootstrapTable({
          pagination: true,
          search: true});

      }
     

    },
    /**************************/
    error: function (jqXHR, estado, error) {
      alert("cuidado:" + estado + " " + error);
    }
  }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos
}




function tabla_venta(id) {
    var opcion =confirm("¿Desea pagar alguna cuota de esta venta?");
  if (opcion == true) {
    var cobro= $("#combo_cobros option:selected").val();

    var datos={
      idVenta:id,
      cobro:cobro,
      tipo:"tabla_venta",
    };
    $.ajax({
   
      url: 'modelo/cobros.php',
      type: 'POST',
  
      data: datos,
  
      success: function (datos) {
  
  
        if (datos == 0) {
  
          alert("No hay cuotas en mora registradas para esta venta");
  
  
  
        } else {
          $("#tabla_cobro").hide();
          $("#tabla_cobros_venta").html(datos);

          $("#tabla_cobros_ventas").bootstrapTable({
            pagination: true,
            checkboxHeader: false,
            clickToSelect:true,
            search: true});
            $("#tabla_cobros_venta").show();
            $("#tabla_cobros_ventas").show();
  
        }
     

  
      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos
  
  
  


  }
  
}

function procesar_cobro(){
 //var table=$("#tabla_cobros_ventas");
var monto=0;
var m=0;
var html="";
var cantidadCuota=0;
var total=0;
$("input[type=checkbox]:checked").each(function(){
  //cada elemento seleccionado
  interes=parseInt($(this).parent().parent().find('td').eq(4).find('input').val());

  monto=$(this).parent().parent().find('td').eq(3).text();
  m = parseInt(monto.replace('$',''));
  
  if(interes>0){

  valorInteres= Math.round(((interes*m)/100))
  total+=valorInteres+m;
  
}else{
  total += m;}



  cantidadCuota++;
 // alert($(this).parent().parent().html());
  //alert($(this).parent().parent().text());
});
html="IMPORTE TOTAL A PAGAR DE:$"+format(total) +" CANTIDAD DE CUOTAS:" + cantidadCuota;
$("#cartel").show();
$("#resultado_total").html(html);
}

function click_check(result,id) {
  var aux="#i"+id

  if(result == true){

procesar_cobro();
$(aux).prop('disabled', false);;

  }else{
    $(aux).prop('disabled', true);;

    procesar_cobro();


  }
  
}

function pagar_cobro() {
  var opcion =confirm("¿Usted tiene cargado un "+$("#resultado_total").text()+" desea procesar el cobro ?");
  var id=0;
  var monto=0;
var m=0;

var cuota=0;
var total=0;
var cobro= $("#combo_cobros option:selected").val();

id=parseInt($("#inputIdventas").val());

  if (opcion == true) {
    $("input[type=checkbox]:checked").each(function(){
      //cada elemento seleccionado
      
       interes=parseInt($(this).parent().parent().find('td').eq(4).find('input').val());

       monto=$(this).parent().parent().find('td').eq(3).text();
       m = parseInt(monto.replace('$',''));
       cuota= parseInt($(this).parent().parent().find('td').eq(1).text());
       if(interes>0){
     
       valorInteres= Math.round(((interes*m)/100))
       total=valorInteres+m;
      }else{
        total = m;}

          var datos={
     cuota:cuota,
     pago:'contado',
     idventas:id,
     monto:total,
     cobro:cobro,

   };
    $.ajax({
   
      url: 'modelo/procesa_cobro.php',
      type: 'POST',
      dataType:'json',
  
      data: datos,
  
      success: function (datos) {
        if (datos.result == 1) {
          $("#alert").show();
         

   
  $("#btn_procesar").hide();
  $("#tabla_cobros_venta").hide();
 
  tabla_general();
  $("#alert").fadeOut("2000");
  
        } else {
          if(datos.result== 0){
            alert("hubo un error al cobrar la cuota:"+ cuota);


          }
          
  
        }
     

  
      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos
  
  });

  
  }else{

  }
}

function imprimir_cobro(id) {
 
  var ruta = "modelo/pdf_recibo.php?id="+id;
  window.open(ruta, 'recibo');
}


function click_input(id) {
 
  var aux="#i"+id;
 procesar_cobro();



  
}
function format(input) {
  var num = input;
  if (!isNaN(num)) {
    num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
    num = num.split('').reverse().join('').replace(/^[\.]/, '');
    return num
  } else {
    return 0
  }
}
function cancelar_cuotas() {
  $("#cartel").hide();
$("#resultado_total").html("");
$("#tabla_cobros_venta").hide();

$("#tabla_cobros_ventas").hide();

tabla_general();

}

function imprimir_cobro() {

}