function alta_gasto() {
  var descripcion = $("#descripcion").val();
  var monto = $("#monto").val();
  var estado = $("#estado option:selected").val();



  $(document).ready(function () { //esto sirve para controlar todo el dom desde que carga 
    //aqui empieza la peticion ajax  


    var data = {
      descripcion: descripcion,
      monto: monto,
      estado: estado,

    }
    $.ajax({
      beforeSend: function () {

      },
      url: 'modelo/alta_gastos.php',
      type: 'POST',
      data: data,


      success: function (datos) {


        if (datos == 1) {

        lista_gastos('pendientes');
        lista_gastos('pagados');


        } else {
          alert("error:" + datos);
        }

      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos

  
  });

}

function lista_gastos(tipo) {


  // data: 1: pendientes , 2:pagados, 3: todos
  var dato = {
    tipo: tipo
  }

  $.ajax({
    beforeSend: function () {

    },
    url: 'modelo/gastos_lista.php',
    type: 'POST',
    dataType: 'json',
    data: dato,
    success: function (datos) {
     
      if(datos.tipo==1){
        $("#cuerpo_gastos_pendientes").html(datos.html);

      }else{
        if(datos.tipo=2){
          $("#cuerpo_gastos_pagados").html(datos.html);

        }else{

        }
      }

    },
    /**************************/
    error: function (jqXHR, estado, error) {
      alert("cuidado:" + estado + " " + error);
    }
  }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos



}

function alta_pago() {
  var id = $("#");

  var datos = {
    id: id,
  }
  $.ajax({
    beforeSend: function () {

    },
    url: 'modelo/update_gasto.php',
    type: 'POST',
    data: datos,
    success: function (datos) {
      if (datos == 0) {


        alert("hubo un error al actualizar")


      } else {
        if (datos == 1) {

          location.reload();
        }
        alert("error consulte a soporte:" + datos);


      }



    },
    /**************************/
    error: function (jqXHR, estado, error) {
      alert("cuidado:" + estado + " " + error);
    }
  }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos




}

function update_gasto(id) {
  var id= id;

  var opcion =confirm("¿Desea dar por pagado este gasto?");
  if (opcion == true) {
    $.ajax({

      url: 'modelo/update_gasto.php',
      type: 'POST',
      data: 'id='+id,

   
      success: function (datos) {

        if (datos == 0) {




        } else {
          if (datos==1) {
            lista_gastos("pendientes");
            lista_gastos("pagados");  
          }else{

            alert(datos);
          }
      

        }

      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos

     
} else {

}
}
function delete_gasto(id) {
  var id= id;

  var opcion =confirm("¿Desea eliminar este gasto?");
  if (opcion == true) {
    $.ajax({

      url: 'modelo/delete_gasto.php',
      type: 'POST',
      data: 'id='+id,

   
      success: function (datos) {

        if (datos == 0) {




        } else {
          if (datos==1) {
            lista_gastos("pendientes");
            lista_gastos("pagados");
          }else{

            alert(datos);
          }
      

        }

      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos

     
} else {

}
}