

function alta_cliente() {
  var nombres = $("#nombres").val();
  var apellido = $("#apellido").val();
  var dni = $("#dni").val();
  var email = $("#email").val();
  var telefono = $("#telefono").val();
  var domicilio = $("#domicilio").val();



  $(document).ready(function () { //esto sirve para controlar todo el dom desde que carga 
    //aqui empieza la peticion ajax  
    $.ajax({
      beforeSend: function () {

      },
      url: 'modelo/alta_clientes.php',
      type: 'POST',

      data: '&apellido=' + apellido + '&nombres=' + nombres + '&dni=' + dni + '&email=' + email + '&telefono=' + telefono + '&domicilio=' + domicilio,

      success: function (datos) {


        if (datos == 1) {

          alert("nuevo cliente ingresado con exito");
 tabla_clientes();
 $("#alta_clientes")[0].reset();


        } else {
          alert("error:" + datos);
        }

      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos

  });

}


function buscar_cliente() {
  var dni = $("#buscarDni").val();
  $(document).ready(function () { //esto sirve para controlar todo el dom desde que carga 
    //aqui empieza la peticion ajax  
    $.ajax({
      beforeSend: function () {

      },
      url: 'modelo/buscar_clientes.php',
      type: 'POST',

      data: '&dni=' + dni,

      success: function (datos) {


        if (datos == 0) {

          alert("NO HAY CLIENTES CON ESE DNI REGISTRADOS,PUEDE REGISTRAR UNO NUEVO");



        } else {
          $("#tabla_clientes").html(datos);

        }

      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos

  });


}

function tabla_clientes() {

    $.ajax({

      url: 'modelo/tabla_clientes.php',
      type: 'POST',

   
      success: function (datos) {

        if (datos == 0) {




        } else {

         $("#tabla_cliente").html(datos);
         $("#tabla_clientes").bootstrapTable({
          pagination: true,
          checkboxHeader: false,
          clickToSelect:true,
          search: true});

        }

      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos
}

function delete_cliente(id) { 
  var id= id;

  var opcion =confirm("¿Seguro que desea eliminar el cliente?");
  if (opcion == true) {
    $.ajax({

      url: 'modelo/delete_cliente.php',
      type: 'POST',
      data: 'id='+id,

   
      success: function (datos) {

        if (datos == 0) {




        } else {
          if (datos==1) {
            tabla_clientes();
          }else{

            alert(datos);
          }
      

        }

      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos

     
} else {

}
  

}

function update_cliente(id) { 
  var id= id;

    $.ajax({

      url: 'modelo/retorna_cliente.php',
      type: 'POST',
      data: 'idcliente='+id,

   
      success: function (datos) {
        if (datos == 0) {




        } else {
          
          $("#modal_cliente").html(datos);
          $("#modal_update_cliente").modal("show");


        }

      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos

     

  

}
function procesa_update_cliente(id) { 
  var id= $("#idC").val();
  var nombres = $("#nombresC").val();
  var apellido = $("#apellidoC").val();
  var dni = $("#dniC").val();
  var email = $("#mailC").val();
  var telefono = $("#telefonoC").val();
  var domicilio = $("#domicilioC").val();
 
var datos={

  idcliente:id,
  nombres:nombres,
  apellido:apellido,
  dni:dni,
  domicilio:domicilio,
  email:email,
  telefono:telefono
}; 
    $.ajax({

      url: 'modelo/update_cliente.php',
      type: 'POST',
      data: datos,

   
      success: function (datos) {
        if (datos == 0) {




        } else {
          if(datos==1){
            $("#modal_update_cliente").modal("hide");

            alert("Cambios realizados con éxito");


          }
          


        }

      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos

     

  

}
