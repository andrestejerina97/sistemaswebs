/*
$.fn.datepicker.dates['en'] = {
    days:  ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    daysShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    daysMin:  ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "December"],
    monthsShort: ["En", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Hoy",
    clear: "Cerrar",
    format: "yyyy/mm/dd",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
/*
    weekStart: 0

};



$('.datepicker').datepicker('update', '2011-03-05', '2011-03-07');  */
$(function () {
    $('#daterange-btn').daterangepicker({
            "locale": {
                "format": "yyyy-mm-dd",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "hasta",
                "customRangeLabel": "Mostrar calendario",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agusto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },

            ranges: {
                'Este dia': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Los ultimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Los ultimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Este mes': [moment().startOf('month'), moment().endOf('month')],
                'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(1, 'days'),
            endDate: moment()
        },
        function (start, end) {
            $('.fe').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            var xstart = start.format('YYYY-MM-DD');
            var xend = end.format('YYYY-MM-DD');
            $("#fi").val(xstart);
            $("#ff").val(xend);
            // alert(start.format('YYYY-MM-DD')+'    '+end.format('YYYY-MM-DD'));
        }
    );
});



function reporte_vehiculo() {
    var fechai=$("#fi").val();
    var fechaf=$("#ff").val();
    var datos={
        fechai:fechai,
        fechaf:fechaf,
        tipo:'vehiculo',
    }
 
    $.ajax({
        
        url: 'modelo/reporte.php',
        type: 'POST',
        data: datos,
        success: function (x) {
            if (x == 0) {
              alert("No hay ningún registro disponible para esa fecha ")
              $("#cuerpo_reporte_vehiculos").html("")
              $("#resultado_total").html("");
              $("#btnImprimir").prop("disabled",true)


            } else {
                $("#btnImprimir").prop("disabled",false)
                $("#cuerpo_reporte_vehiculos").html(x)
               // procesarReporte();
            }



        },
        /**************************/
        error: function (jqXHR, estado, error) {
            alert("cuidado:" + estado + " " + error);
        }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos



}

function procesarReporte(){
    var detalle=0.00;
    var detalle_cancelado=0.00;
    var total_credito=0.00;
    var venta_neta=0.00;
    var detalle_contado=0.00;
    var detalle_cancelado_contado=0.00;
    var total_contado=0.00;
    var total_gastos=0.00;
    var total_abonos=0.00;
    $('#tabla_reporte_vehiculos > tbody > tr').each(function(){
                var monto = $(this).find('td').eq(2).html();
                var status = $(this).find('td').eq(3).html();
                monto=monto.replace(",","");
                monto=parseFloat(monto);
                total_credito+=monto;
             if(status=="pagado"){
               detalle_cancelado+=monto;
               }
             if(status=="pendiente"){
               detalle+=monto
             }
         $("#resultado_total").html("");
         $("#resultado_total").html("<span class='label label-success'>Total Neto: $ "+format(total_credito)+"</span> <br> <span class='label label-success'>Cancelados: $ "+format(detalle_cancelado)+"</span> <br> <span class='label label-danger'>Pendientes: $ "+format(detalle)+"</span>");
        });

}
function format(input)
{
var num = input;
if(!isNaN(num)){
num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
num = num.split('').reverse().join('').replace(/^[\.]/,'');
return num
}
 
else{ 
return 0
}
}

function imprimir_reporte(date) {
  

    var datos='tipo='+date+'&fechai='+$("#fi").val()+'&fechaf='+ $("#ff").val()

    
    window.location.href="modelo/reportes_imprimir_vehiculos.php?"+datos;
/*
$.ajax({
    url:'modelo/reportes_imprimir_vehiculos.php',
    type: 'POST',
    data:datos,
    success: function (x) {
        if (x == 0) {
          alert("No hay ningún registro disponible para esa fecha ")
          $("#cuerpo_reporte_vehiculos").html("")
          $("#resultado_total").html("");
          $("#btnImprimir").prop("disabled",true)


        } else {
        }



    },
    /**************************//*
    error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
    }



})
*/
}


function reporte_alquiler() {
    var fechai=$("#fi").val();
    var fechaf=$("#ff").val();
    var datos={
        fechai:fechai,
        fechaf:fechaf,
        tipo:'alquiler',
    }
 
    $.ajax({
        
        url: 'modelo/reporte.php',
        type: 'POST',
        data: datos,
        success: function (x) {
            if (x == 0) {
              alert("No hay ningún registro disponible para esa fecha ")
              $("#cuerpo_reporte_alquileres").html("")
              $("#resultado_total").html("");
              $("#btnImprimir").prop("disabled",true)


            } else {
                $("#btnImprimir").prop("disabled",false)
                $("#cuerpo_reporte_alquileres").html(x)
               // procesarReporte();
            }



        },
        /**************************/
        error: function (jqXHR, estado, error) {
            alert("cuidado:" + estado + " " + error);
        }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos



}


function reporte_gastos(tipo) {


    // data: 1: pendientes , 2:pagados, 3: todos
    var dato = {
      tipo: tipo
    }
  
    $.ajax({
      beforeSend: function () {
  
      },
      url: 'modelo/gastos_lista.php',
      type: 'POST',
      dataType: 'json',
      data: dato,
      success: function (datos) {
       
        if(datos.tipo==1){
          $("#cuerpo_gastos_pendientes").html(datos.html);
  
        }else{
          if(datos.tipo=2){
            $("#cuerpo_gastos_pagados").html(datos.html);
  
          }else{
  
          }
        }
  
      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos
  
  
  
  }


function reporte_ingresos() {
  var fechai=$("#fi").val();
    var fechaf=$("#ff").val();
    var datos={
        fechai:fechai,
        fechaf:fechaf,
        tipo:'vehiculo',
    }
 
    $.ajax({
        
        url: 'modelo/reporte.php',
        type: 'POST',
        data: datos,
        success: function (x) {
            if (x == 0) {
              
              $("#cuerpo_reporte_vehiculos").html("")
              $("#resultado_total").html("");
              $("#btnImprimir").prop("disabled",true)
            }
            else{
              $("#btnImprimir").prop("disabled",false)
              $("#cuerpo_reporte_vehiculos").html(x)

            }
           var datos={
                fechai:fechai,
                fechaf:fechaf,
                tipo:'alquiler',
            }
              $.ajax({
          
                url: 'modelo/reporte.php',
                type: 'POST',
                data: datos,
                success: function (x) {
                    if (x == 0) {
                     
                      $("#cuerpo_reporte_alquileres").html("")


                    
                      

                    } else {

                        $("#btnImprimir").prop("disabled",false)
                        $("#cuerpo_reporte_alquileres").html(x)
                   
                                     
                    }
                    var datos={
                      fechai:fechai,
                      fechaf:fechaf,
                      tipo:'gastos',
                  }
    
                    $.ajax({

                      url: 'modelo/reporte.php',
                      type: 'POST',
                      data: datos,
                      success: function (datos) {
                       
                        if(datos==0){
                          $("#cuerpo_gastos_pagados").html(datos);
                                             
                        }else{

                         
                            $("#cuerpo_gastos_pagados").html(datos);
                      
    
    
                  
                      
                        }
                        var total_creditoV=0.00;
      var total_creditoA=0.00;
      var total_creditoG=0.00;
      var montoA=0;
      var montoV=0;
      var montoG=0; 
      $('#cuerpo_reporte_vehiculos tr').each(function(){
                    var montoV =$(this).find('td').eq(2).html();
                   // var status = $(this).find('td').eq(3).html();
                  
                // montoV=montoV.replace(",","");
                    montoV=parseFloat(montoV);
                    total_creditoV+=montoV;
                   
                  });
      $('#tabla_reporte_alquileres > tbody > tr').each(function(){
                    var montoA = $(this).find('td').eq(4).html();
                 
                    // var status = $(this).find('td').eq(3).html();
                  // montoA=montoA.replace(",","");
                    montoA=parseFloat(montoA);
                    total_creditoA+=montoA;
                  });
    $('#tabla_reporte_gastos > tbody > tr').each(function(){
                    var montoG = $(this).find('td').eq(2).html();
                   // var status = $(this).find('td').eq(3).html();
                 // montoG=montoG.replace(",","");
                    montoG=parseFloat(montoG);
                    total_creditoG+=montoG;
                   
                  });
    
    var total_neto= (total_creditoA+total_creditoV)-total_creditoG ;
             $("#resultado_total").html("");
             $("#resultado_total").html("<span class='label label-success'>Total Neto: $ "+format(total_neto)+
             "</span> <br> <span class='label label-success'>Ingresos: $ "+format(total_creditoV+total_creditoA)+
             "</span> <br> <span class='label label-danger'>Egresos: $ "+format(total_creditoG)+"</span>");
                      },
                      /**************************/
                      error: function (jqXHR, estado, error) {
                        alert("cuidado:" + estado + " " + error);
                      }
                    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos        
        
                },
                /**************************/
                error: function (jqXHR, estado, error) {
                    alert("cuidado:" + estado + " " + error);
                }
            }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos        
                
                    },
                    /**************************/
                    error: function (jqXHR, estado, error) {
                      alert("cuidado:" + estado + " " + error);
                    }
                  }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos





}

function procesar() {
  var total_creditoV=0.00;
  var total_creditoA=0.00;
  var total_creditoG=0.00;
  var montoA=0;
  var montoV=0;
  var montoG=0; 
  $('#cuerpo_reporte_vehiculos tr').each(function(){
                var montoV =alert($(this).find('td').eq(2).html());
               // var status = $(this).find('td').eq(3).html();
              
             montoV=montoV.replace(",","");
                montoV=parseFloat(montoV);
                total_creditoV+=montoV;
                
              });
  $('#tabla_reporte_alquileres > tbody > tr').each(function(){
                var montoA = $(this).find('td').eq(4).html();
               alert(montoA)
                // var status = $(this).find('td').eq(3).html();
               montoA=montoA.replace(",","");
                montoA=parseFloat(montoA);
                total_creditoA+=montoA;
              });
$('#tabla_reporte_gastos > tbody > tr').each(function(){
                var montoG = $(this).find('td').eq(2).html();
               // var status = $(this).find('td').eq(3).html();
              montoG=montoG.replace(",","");
                montoG=parseFloat(montoG);
                total_creditoG+=montoG;
              });

var total_neto= (total_creditoA+total_creditoV)-total_creditoG
         $("#resultado_total").html("");
         $("#resultado_total").html("<span class='label label-success'>Total Neto: $ "+format(total_neto)+
         "</span> <br> <span class='label label-success'>Ingresos: $ "+format(total_creditoV+total_creditoA)+
         "</span> <br> <span class='label label-danger'>Egresos: $ "+format(total_creditoG)+"</span>");
}