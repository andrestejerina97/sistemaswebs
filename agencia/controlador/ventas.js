function buscar_cliente() {
  var dni = $("#buscarDni").val();
  $(document).ready(function () { //esto sirve para controlar todo el dom desde que carga 
    //aqui empieza la peticion ajax  

    $.ajax({
      beforeSend: function () {

      },
      url: 'modelo/buscar_clientes.php',
      type: 'POST',
      method: 'POST',

      data: 'dni=' + dni,

      success: function (datos) {


        if (datos == 0) {

          alert("NO HAY CLIENTES CON ESE DNI REGISTRADOS,PUEDE REGISTRAR UNO NUEVO");



        } else {

          $(".tabla_clientes").html(datos);
        cb_tipo_de_venta();
        }

      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    }); //aquí termina la petición ajax,es importante tener cuidado con los errores de sintaxis porque a veces es dificil depurarlos

  });
}

function pulsar(q) {
  if (q.keyCode === 13 && !q.shiftKey) {
    q.preventDefault();
    buscar_cliente();

  }
}
$(document).ready(function () {

  $('.money').mask('000.000.000.000.000', {
    reverse: true
  });
  $('.dni').mask('0000000000');


});

$('#comboComplejos').change(function () {
  var id = $("#comboComplejos option:selected").val();

  if (id !== 0) {

    comboDepartamentos(id);
  }
});
function cb_tipo_de_venta(){
  var id = $("#comboTipoDeVenta option:selected").val();

  if (id=='vehiculos') {
    $("#seccionVentaAlquileres").css({
      display: "none"
    });
    $("#seccionVentaVehiculos").css({
      display: "block"
    });
  }else{
    if (id=='alquileres') {
      $("#seccionVentaVehiculos").css({
        display: "none"
      });
      $("#seccionVentaAlquileres").css({
        display: "block"
      });
    }

    
  }
};



function comboComplejos() {
  $.ajax({
    url: 'modelo/combo_complejos.php',

    success: function (datos) {
      if (datos == 0) {
        $("#comboComplejos").html(datos);

      } else {
        $("#comboComplejos").html(datos);


      }
    },
    /**************************/
    error: function (jqXHR, estado, error) {
      alert("cuidado:" + estado + " " + error);
    }
  });
}
$('#comboVehiculos').change(function () {
  var id = $("#comboVehiculos option:selected").val();

  if (id !== 0) {
    $.ajax({
      url: 'modelo/precio_vehiculo.php',
      method:'POST',
      data:'id='+id,
      
  
      success: function (datos) {
        $("#precioI").val(format(datos));
        $("#preciov").val(datos);

         
          
  
  
       
      },
      /**************************/
      error: function (jqXHR, estado, error) {
        alert("cuidado:" + estado + " " + error);
      }
    });
  }
});
function comboVehiculo() {
  $.ajax({
    url: 'modelo/combo_vehiculos.php',
    

    success: function (datos) {
      if (datos== 0) {
alert("error al cargar los vehículos en stock,recarga la página(F5) o consulte a soporte");
      } else {  
        $("#comboVehiculos").html(datos);
       
        


      }
    },
    /**************************/
    error: function (jqXHR, estado, error) {
      alert("cuidado:" + estado + " " + error);
    }
  });
}

function habilitar() {
  if ($("#comboComplejos option:selected").val() == 0) {
    $("#departamento").css({
      display: "none"
    })

  } else {
    $("#departamento").css({
      display: "block"
    })
  }
}


$(document).ready(function(){
  //$('#boton1').hide();
  $('.add_more').click(function(e){
      
      $("#input2").append('<div class="row"> <div class="form-group col-md"><select name="tipoEntrega" class="form-control"><option value="efectivo" selected=""> efectivo </option><option value="vehiculo"> vehiculo usado </option><option value="otro"> otro </option></select> 	</div><div class="form-group col-md"><input onchange="calcular_entregaV();"  placeholder="Valor" class="form-control entregaParcial"></div> </div>');
   

  });
});
function comboDepartamentos(id) {

  $.ajax({
    url: 'modelo/combo_departamentos.php',
    method: 'POST',
    data: 'dpto=' + parseInt(id),
    success: function (datos) {
      if (datos == 0) {
        $("#comboDepartamentos").html(datos);

      } else {
        $("#comboDepartamentos").html(datos);


      }
    },
    /**************************/
    error: function (jqXHR, estado, error) {
      alert("cuidado:" + estado + " " + error);
    }
  });
}


/*function calcular_entrega() {
  total=0.00;
          $(".entregaParcial").each(function(){
            if(parseInt($(this).val())){ 
            total=parseInt($(this).val())+total;
            }
            });

   $("#entrega").val(format(total));
  
  var precio = parseInt($("#precio").val());
  
  var entrega = total
  var monto = precio - entrega;  
  $("#saldo").val(format(monto));

}*/
function calcular_entregaV() {
  total=0.00;
          $(".entregaParcial").each(function(){
            if(parseInt($(this).val())){ 
            total=parseInt($(this).val())+total;
            }
            });

   $("#entregav").val(format(total));
  
  var precio = parseInt($("#preciov").val());
  
  var entrega = total
  var monto = precio - entrega;  
  $("#saldov").val(format(monto));

}
function calcularVenta() {
  var precio = parseInt($("#precio").val());
  var entrega = parseInt($("#entrega").cleanVal());
  var monto = precio - entrega;  
  var cuotas = parseInt($("#cuotas").val());
  var interes = parseInt($("#interes").val());
  if (monto && cuotas) {

    total = Math.round(interes * monto / 100);
    totalf = Math.round(monto + total);
    totalc = format(Math.round(totalf / cuotas));


    $("#resultadoVenta").text(" TOTAL: " + cuotas + " CUOTAS DE $" + totalc)
  }else{

    if (entrega==precio) {

      var cuotas = 1;

      totalc = format(Math.round(precio/ cuotas));
      $("#resultadoVenta").text(" TOTAL: " + cuotas + " CUOTAS DE $" + totalc);


      


    }
    
   
  }
}
function calcularVentaV() {
  var precio = parseInt($("#preciov").val());
  var entrega = parseInt($("#entregav").cleanVal());
  var monto = precio - entrega;  
  var cuotas = parseInt($("#cuotasv").val());
  var interes = parseInt($("#interesv").val());
  if (monto && cuotas) {

    total = Math.round(interes * monto / 100);
    totalf = Math.round(monto + total);
    totalc = format(Math.round(totalf / cuotas));


    $("#resultadoVentav").text(" TOTAL: " + cuotas + " CUOTAS DE $" + totalc)
  }else{

    if (entrega==precio) {

      var cuotas = 1;

      totalc = format(Math.round(precio/ cuotas));
      $("#resultadoVentav").text(" TOTAL: " + cuotas + " CUOTAS DE $" + totalc);


      


    }
    
   
  }
}


function procesa_venta() {
  var precio = parseInt($("#preciov").val());
  var entrega = parseInt($("#entregav").cleanVal());
  var primerPago = $("#primerpagov").val()
  var tipoEntrega = $("#tipoEntrega option:selected").val()
  var saldo = parseInt($("#saldov").cleanVal());
  var vehiculo = $("#comboVehiculos option:selected").val();
  var cuotas = parseInt($("#cuotasv").val());
  var interes = parseInt($("#interesv").val());
  var id = parseInt($("#idcliente").val());
  var monto = precio - entrega;  


  if (cuotas) {
    if (entrega==precio) {
      totalf=Math.round(precio);
      totalc = format(Math.round(totalf / cuotas));



    }else{
    total = Math.round(interes * monto / 100);
    totalf = Math.round(monto + total);
    totalc = format(Math.round(totalf / cuotas));
    }

  }else{

    if (entrega==precio) {

      var cuotas = 1;

      totalc = format(Math.round(precio/ cuotas));


      


    }else{
      alert("las cuotas no pueden ser 0");
     
    }
    
   
  }

  var data = {
    precio: precio,
    entrega: entrega,
    saldo: saldo,
    cuotas: cuotas,
    interes: interes,
    vehiculo: vehiculo,
    id: id,
    cuotaValor: totalc,
    primerPago: primerPago,
    tipoEntrega: tipoEntrega

  };
  $.ajax({
    url: 'modelo/procesa_venta_vehiculo.php',
    type: 'POST',
    data: data,
    dataType: 'json',
    success: function (datos) {
      if (datos == 0) {

      } else {
        if (datos.result == 1) {
          alert("venta exitosa");
          $("#buscarDni").focus();
          $("#preciov").val("");
          $("#precioI").val("");
          $(".entregaParcial").each(function(){
            $(this).val("");
            
            });
          $("#seccionVentaVehiculos").css({
            display: "none"
          });
          $("#modal_confirmar_ventav").modal("toggle"); 

          $("#comboVehiculo option:selected").val("0");

          $("#entregav").val("");
          $("#cuotasv").val("");
          $("#interesv").val("");
          $("#idcliente").val("");
          $("#buscarDni").val("");

          $("#resultadoVentav").text(" ")
          $("#form1")[0].reset();
          $("#form2")[0].reset();
          window.open('modelo/pdf_contrato_vehiculo.php?id='+datos.idventa);
        } else {
          alert("error:" + datos);
        }
      }


    },
    /**************************/
    error: function (jqXHR, estado, error) {
      alert("cuidado:" + estado + " " + error);
    }
  });

}


function procesa_venta_alquiler() {



  var primerPago = $("#primerpago").val()

  var precio = parseInt($("#precio").cleanVal());
  var entrega = parseInt($("#entrega").cleanVal());
  var saldo = precio - entrega;
  var costoMensual = parseInt($("#costoMensual").cleanVal())
  var dpto = $("#comboDepartamentos option:selected").val();
  var contrato = $("#comboContrato option:selected").val();
  var cuotas = parseInt($("#cuotas").val());
  var interes = parseInt($("#interes").val());
  var id = parseInt($("#idcliente").val());

  total = Math.round(interes * saldo / 100);
  totalf = Math.round(saldo + total);
  totalc = Math.round(totalf / cuotas);
  var data = {
    primerpago: primerPago,
    precio: precio,
    entrega: entrega,
    saldo: saldo,
    cuotas: cuotas,
    interes: interes,
    costoMensual: costoMensual,
    dpto: dpto,
    id: id,
    contrato: contrato,
    cuotaValor: totalc,

  };
  $.ajax({
    url: 'modelo/procesa_venta_alquiler.php',
    type: 'POST',
    dataType: 'JSON',
    data: data,
    success: function (datos) {
      if (datos.result == 0) {

      } else {
        if (datos.result == 1) {
          alert("venta exitosa");
          $("#buscarDni").focus();
          $("#precio").val("");
          $("#seccionVenta").css({
            display: "none"
          });
          $("#comboVehiculo option:selected").val("0");

          $("#entrega").val("");
          $("#cuotas").val("");
          $("#interes").val("");
          $("#idcliente").val("");
          $("#buscarDni").val("");

          $("#resultadoVenta").text(" ")
          $("#modal_confirmar_venta").modal("toggle");
          
          window.open('modelo/pdf_contrato.php?id='+datos.idventa);

          }else{
if(datos.result==2){
alert("Debe completar todos los campos para poder realizar la operación");
}
          }


        } 

    },
    /**************************/
    error: function (jqXHR, estado, error) {
      alert("cuidado:" + estado + " " + error);
    }
  });

}

function format(input) {
  var num = input;
  if (!isNaN(num)) {
    num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
    num = num.split('').reverse().join('').replace(/^[\.]/, '');
    return num
  } else {
    return 0
  }
}

function imprimir_pdf() {
  var datos="dni="+$("#buscarDni").val()
  var ruta = "modelo/pdf_contrato.php?"+datos;
  window.open(ruta, 'Nombre Ventana');
}