
$(function(){
    $('#daterange-btn').daterangepicker(
          {
                       "locale": {
                       "format": "yyyy-mm-dd",
                       "separator": " - ",
                       "applyLabel": "Aplicar",
                       "cancelLabel": "Cancelar",
                       "fromLabel": "Desde",
                       "toLabel": "hasta",
                       "customRangeLabel": "Custom",
                       "daysOfWeek": [
                           "Do",
                           "Lu",
                           "Ma",
                           "Mi",
                           "Ju",
                           "Vi",
                           "Sa"
                       ],
                       "monthNames": [
                           "Enero",
                           "Febrero",
                           "Marzo",
                           "Abril",
                           "Mayo",
                           "Junio",
                           "Julio",
                           "Agusto",
                           "Septiembre",
                           "Octubre",
                           "Noviembre",
                           "Diciembre"
                       ],
                       "firstDay": 1
                   },
               
            ranges: {
              'Este dia': [moment(), moment()],
              'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Los ultimos 7 dias': [moment().subtract(6, 'days'), moment()],
              'Los ultimos 30 dias': [moment().subtract(29, 'days'), moment()],
              'Este mes': [moment().startOf('month'), moment().endOf('month')],
              'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate: moment()
          },
      function (start, end) {
        $('.fe').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        var xstart=start.format('YYYY-MM-DD');
        var xend=end.format('YYYY-MM-DD');
        $("#fi").val(xstart);
        $("#ff").val(xend);
        //alert(start.format('YYYY-MM-DD')+'    '+end.format('YYYY-MM-DD'));
       }
      );
     });