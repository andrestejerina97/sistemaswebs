<?php

session_start();

if ($_SESSION['tipo']> 0) {
    
}else{

   header('Location: index.php');
};

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AGENCIA</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="plugins/tabla/dist/bootstrap-table.min.css">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include 'sidebar.php' ?>



        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include 'navbar.php' ?>

                <section>
                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <div class="row justify-content-center">
                            <!-- Page Heading -->
                            <h1 class="h3 mb-4 text-gray-800">Cobros</h1>
                        </div>

                        <div class="row justify-content-center">
                        <h1 class="text-gray-800" style="display:none" id="alert">Procesando cobro...</h1>

                            <form>
                                <div class='input-group'>
                                    <input class="form-control dni" id="buscarDni" onkeypress="pulsar(event);"
                                        placeholder='Ingresar DNI del cliente'>
                                    <button type='button' class='btn btn-success btn-sm' onclick='tabla_general();'
                                        id='btn-buscar-cambio'><i class='fa fa-search'></i>Buscar...</button>
                                </div>

                                <input type="hidden" id="dni">
                                <br><br>
                <select style="display:none" class="form-control" onchange="cb_cobros(this.value);" id="combo_cobros">
                
                <option value="vehiculos">VEHICULOS</option>

                    <option value="alquileres">ALQUILERES</option>


                </select>
                            </form>
    
                        </div>

                        <div id="cartel" class="col-md-6 justify-content-end" style="display:none">
                        <br><br>
                <div class="card"  >
                  <div class="card-body">

                    <h5 class="card-subtitle mb-2 text-muted" id="resultado">RESÚMENES</h5>
                    <div></div>
                    <h5 id="resultado_total"></h5>
                  </div>
                </div>
               
                <button class="btn btn-success" id="btn_imprimir" style="display:none" onclick="imprimir_cobro();">Imprimir comprobante</button>


              </div>

                        <br>

                        <div class=" col-md-12" id="tabla_cobro">
                            


                          
                        </div>


                        <div class=" col-md-12" id="tabla_cobros_venta">
                           
                        </div>

                    </div>


                </section>



            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include 'footer.php' ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->




    <!-- Scroll to Top Button-->

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>


    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
    <script src="controlador/cobros.js"></script>
    <script src="plugins/tabla/dist/bootstrap-table.min.js"></script>
    <script src="plugins/tabla/dist/bootstrap-table-es-AR.min.js"></script>


</body>

</html>