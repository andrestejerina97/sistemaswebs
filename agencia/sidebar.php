    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="inicio.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-circule"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Agencia</sup></div>
      </a>
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="inicio.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Inicio</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        VENTAS
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link" href="ventas_simulador.php"   >
          <i class="fas fa-fw fa-cog"></i>
          <span>Simulador</span>
        </a>
 
      </li>
              <!-- Nav Item - Pages Collapse Menu -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="venta_oficial.php">
          <i class="fa fa-car"></i> <i class="fa fa-file-contract"></i>
          <span>Punto de ventas</span>
        </a>
        
      </li>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
        <a class="nav-link" href="cargar_clientes.php">
          <i class="fas fa-users"></i>
          <span>Cargar clientes</span>
        </a>
        
      </li>

      
 <!-- Divider -->
 <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
       Agencia
      </div>

      <!-- Nav Item - Utilities Collapse Menu -->
      
      
      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="cargar_vehiculo.php">
          <i class="fas fa-car"></i>
          <span>Cargar Vehiculos</span></a>
      </li>


        <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
       Alquileres
      </div>

      
      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="cargar_alquiler.php">
          <i class="fas fa-house-damage"></i>
          <span>Cargar Alquileres</span></a>
      </li>
      <!-- Nav Item - Pages Collapse Menu 
      <li class="nav-item">
        <a class="nav-link" href="venta_oficial_alquiler.php">
          <i class="fa fa-file-contract"></i>
          <span>Punto de alquiler</span>
        </a>
        
      </li>-->
       <!-- Divider -->
   <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
       FUNCIONES
      </div>
      <li class="nav-item">
        <a class="nav-link" href="gastos.php">
          <i class="fas fa-money-bill"></i>
          <span>Gastos</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="cuotas_en_mora.php">
          <i class="fas fa-book-dead"></i>
          <span>Cuotas en mora</span></a>
      </li>

  <!-- Divider -->
  <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
       COBROS
      </div>
      <li class="nav-item">
        <a class="nav-link" href="cobros.php">
          <i class="fas fa-house-damage"></i>
          <i class="fas fa-car"></i>
          <span>Punto de cobro</span></a>
      </li>

    
    <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
      Reportes
      </div>

      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link" href="vehiculos_vendidos.php">
          <i class="fas fa-car"></i>
          <span>Vehiculos vendidos</span></a>
      </li>

      <!-- Nav Item - Tables -->


      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="reporte_alquiler.php">
          <i class="fas fa-house-damage"></i>
          <span>Alquileres</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="reporte_ingresos.php">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Ingresos</span></a>
      </li>

     

      <!-- Sidebar Toggler (Sidebar) -->
  

    </ul>
    <!-- End of Sidebar -->