-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-08-2019 a las 19:19:33
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `agencia_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `idclientes` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `direccion` varchar(40) NOT NULL DEFAULT 'no definido'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`idclientes`, `dni`, `apellido`, `nombre`, `telefono`, `email`, `direccion`) VALUES
(2, 39989093, 'tejerina', 'josÃ© andrÃ©s', '1234567', 'andd', 'no definido'),
(4, 1234, 'Corzo', 'Pablo Esteban', '3884419080', 'asda@', 'no definido');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobranzas`
--

CREATE TABLE `cobranzas` (
  `idcobranzas` int(11) NOT NULL,
  `monto_cobro` varchar(30) NOT NULL,
  `fecha` date NOT NULL,
  `tipo_de_pago` varchar(45) NOT NULL,
  `cuota` varchar(10) NOT NULL,
  `interes` varchar(40) DEFAULT NULL,
  `monto_final` int(40) DEFAULT NULL,
  `ventas_idventas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cobranzas`
--

INSERT INTO `cobranzas` (`idcobranzas`, `monto_cobro`, `fecha`, `tipo_de_pago`, `cuota`, `interes`, `monto_final`, `ventas_idventas`) VALUES
(59, '833335000', '0000-00-00', 'contado', '6', NULL, NULL, 27),
(60, '833335000', '0000-00-00', 'contado', '5', NULL, NULL, 27),
(61, '833335000', '2019-08-03', 'contado', '8', NULL, NULL, 27),
(62, '833335000', '2019-08-03', 'contado', '9', NULL, NULL, 27),
(63, '833335000', '2019-08-03', 'contado', '7', NULL, NULL, 27),
(64, '833335000', '2019-08-03', 'contado', '1', NULL, NULL, 28),
(65, '833335000', '2019-08-03', 'contado', '2', NULL, NULL, 28),
(66, '833335000', '2019-08-03', 'contado', '3', NULL, NULL, 28),
(67, '833335000', '2019-08-03', 'contado', '4', NULL, NULL, 28),
(68, '833335000', '2019-08-03', 'contado', '10', NULL, NULL, 27),
(69, '833335000', '2019-08-03', 'contado', '11', NULL, NULL, 27),
(70, '833335000', '2019-08-03', 'contado', '12', NULL, NULL, 27),
(71, '833335000', '2019-08-03', 'contado', '1', NULL, NULL, 29),
(72, '833335000', '2019-08-03', 'contado', '2', NULL, NULL, 29),
(73, '0', '2019-08-03', 'contado', '1', NULL, NULL, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuotas`
--

CREATE TABLE `cuotas` (
  `idcuotas` int(11) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `numero_de_cuota` int(11) NOT NULL,
  `precio` varchar(40) NOT NULL,
  `vencimiento` date NOT NULL,
  `ventas_idventas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuotas`
--

INSERT INTO `cuotas` (`idcuotas`, `estado`, `numero_de_cuota`, `precio`, `vencimiento`, `ventas_idventas`) VALUES
(126, 'pagado', 1, '51000', '1969-12-31', 18),
(127, 'pagado', 2, '51000', '1970-01-31', 18),
(799, 'pagado', 1, '5100', '2019-07-22', 20),
(800, 'pagado', 2, '5100', '2019-06-10', 20),
(801, 'pagado', 1, '5100', '2019-07-22', 21),
(802, 'pagado', 2, '5100', '2019-08-22', 21),
(808, 'pagado', 1, '4000', '2019-07-03', 23),
(809, 'pagado', 2, '4000', '2019-08-03', 23),
(810, 'pagado', 3, '4000', '2019-09-03', 23),
(811, 'pagado', 1, '625', '2019-07-04', 24),
(812, 'pagado', 2, '625', '2019-08-04', 24),
(813, 'pagado', 1, '15000000000', '2019-07-16', 25),
(814, 'pagado', 2, '15000000000', '2019-08-16', 25),
(815, 'pagado', 1, '833335000', '2019-08-01', 26),
(816, 'pagado', 2, '833335000', '2019-09-01', 26),
(817, 'pagado', 3, '833335000', '2019-10-01', 26),
(818, 'pagado', 4, '833335000', '2019-11-01', 26),
(819, 'pagado', 5, '833335000', '2019-12-01', 26),
(820, 'pagado', 6, '833335000', '2020-01-01', 26),
(821, 'pagado', 7, '833335000', '2020-02-01', 26),
(822, 'pagado', 8, '833335000', '2020-03-01', 26),
(823, 'pagado', 9, '833335000', '2020-04-01', 26),
(824, 'pagado', 10, '833335000', '2020-05-01', 26),
(825, 'pagado', 11, '833335000', '2020-06-01', 26),
(826, 'pagado', 12, '833335000', '2020-07-01', 26),
(827, 'pagado', 1, '833335000', '2019-08-02', 27),
(828, 'pagado', 2, '833335000', '2019-09-02', 27),
(829, 'pagado', 3, '833335000', '2019-10-02', 27),
(830, 'pagado', 4, '833335000', '2019-11-02', 27),
(831, 'pagado', 5, '833335000', '2019-12-02', 27),
(832, 'pagado', 6, '833335000', '2020-01-02', 27),
(833, 'pagado', 7, '833335000', '2020-02-02', 27),
(834, 'pagado', 8, '833335000', '2020-03-02', 27),
(835, 'pagado', 9, '833335000', '2020-04-02', 27),
(836, 'pagado', 10, '833335000', '2020-05-02', 27),
(837, 'pagado', 11, '833335000', '2020-06-02', 27),
(838, 'pagado', 12, '833335000', '2020-07-02', 27),
(839, 'pagado', 1, '833335000', '2019-08-02', 28),
(840, 'pagado', 2, '833335000', '2019-09-02', 28),
(841, 'pagado', 3, '833335000', '2019-10-02', 28),
(842, 'pagado', 4, '833335000', '2019-11-02', 28),
(843, 'En mora', 5, '833335000', '2019-12-02', 28),
(844, 'En mora', 6, '833335000', '2020-01-02', 28),
(845, 'En mora', 7, '833335000', '2020-02-02', 28),
(846, 'En mora', 8, '833335000', '2020-03-02', 28),
(847, 'En mora', 9, '833335000', '2020-04-02', 28),
(848, 'En mora', 10, '833335000', '2020-05-02', 28),
(849, 'En mora', 11, '833335000', '2020-06-02', 28),
(850, 'En mora', 12, '833335000', '2020-07-02', 28),
(851, 'pagado', 1, '833335000', '2019-08-02', 29),
(852, 'pagado', 2, '833335000', '2019-09-02', 29),
(853, 'En mora', 3, '833335000', '2019-10-02', 29),
(854, 'En mora', 4, '833335000', '2019-11-02', 29),
(855, 'En mora', 5, '833335000', '2019-12-02', 29),
(856, 'En mora', 6, '833335000', '2020-01-02', 29),
(857, 'En mora', 7, '833335000', '2020-02-02', 29),
(858, 'En mora', 8, '833335000', '2020-03-02', 29),
(859, 'En mora', 9, '833335000', '2020-04-02', 29),
(860, 'En mora', 10, '833335000', '2020-05-02', 29),
(861, 'En mora', 11, '833335000', '2020-06-02', 29),
(862, 'En mora', 12, '833335000', '2020-07-02', 29),
(863, 'En mora', 1, '833335000', '2019-08-10', 30),
(864, 'En mora', 2, '833335000', '2019-09-10', 30),
(865, 'En mora', 3, '833335000', '2019-10-10', 30),
(866, 'En mora', 4, '833335000', '2019-11-10', 30),
(867, 'En mora', 5, '833335000', '2019-12-10', 30),
(868, 'En mora', 6, '833335000', '2020-01-10', 30),
(869, 'En mora', 1, '10417', '2019-07-31', 31),
(870, 'En mora', 2, '10417', '2019-08-31', 31),
(871, 'En mora', 3, '10417', '2019-10-01', 31),
(872, 'En mora', 4, '10417', '2019-11-01', 31),
(873, 'En mora', 5, '10417', '2019-12-01', 31),
(874, 'En mora', 6, '10417', '2020-01-01', 31),
(875, 'pagado', 1, '11250', '2019-08-01', 32),
(876, 'pagado', 2, '11250', '2019-09-01', 32),
(877, 'pagado', 3, '11250', '2019-10-01', 32),
(878, 'pagado', 4, '11250', '2019-11-01', 32),
(879, 'pagado', 5, '11250', '2019-12-01', 32),
(880, 'pagado', 6, '11250', '2020-01-01', 32),
(881, 'En mora', 1, '31250', '2019-08-09', 36),
(882, 'En mora', 2, '31250', '2019-09-09', 36),
(883, 'En mora', 1, '306.250', '2019-08-15', 46),
(884, 'En mora', 2, '306.250', '2019-09-15', 46),
(885, 'En mora', 1, '306.250', '2019-08-15', 50),
(886, 'En mora', 2, '306.250', '2019-09-15', 50),
(887, 'En mora', 1, '562.500', '2019-08-15', 51),
(888, 'En mora', 1, '500.000', '2019-08-15', 52),
(889, 'En mora', 1, '500.000', '2019-08-15', 53),
(890, 'En mora', 1, '500.000', '2019-08-14', 54),
(891, 'En mora', 1, '500.000', '2019-08-15', 55),
(892, 'En mora', 1, '500.000', '2019-08-15', 56),
(893, 'En mora', 1, '229.500', '2019-08-15', 57),
(894, 'En mora', 2, '229.500', '2019-09-15', 57),
(895, 'En mora', 1, '459.000', '2019-08-15', 58);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `idgastos` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `fecha` date NOT NULL,
  `monto` float NOT NULL,
  `estado` varchar(20) NOT NULL,
  `usuarios_idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`idgastos`, `descripcion`, `fecha`, `monto`, `estado`, `usuarios_idusuarios`) VALUES
(3, 'luz', '2019-06-22', 3000, 'pagado', 1),
(5, 'agua', '2019-06-22', 2000, 'pendiente', 1),
(6, 'telefono', '2019-06-27', 3000, 'pagado', 1),
(7, 'insumo', '2019-06-30', 2000, 'pagado', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `nombre`, `tipo`, `usuario`, `password`) VALUES
(1, NULL, NULL, '', NULL),
(2, 'José Andrés Tejerina', 1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE `vehiculos` (
  `idvehiculos` int(11) NOT NULL,
  `marca` varchar(45) NOT NULL,
  `modelo` varchar(45) NOT NULL,
  `precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`idvehiculos`, `marca`, `modelo`, `precio`) VALUES
(1, 'ford', '20003', 500000),
(2, 'RENAULT KANGOO', '2003', 500000),
(3, 'PEUGEOT PARTNER', '2003', 500000),
(4, 'ford', '20003', 500000),
(5, 'ford', '20003', 23333),
(6, 'ford', '20003', 4444),
(7, 'Renault', '2018', 900000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `idventas` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `monto` float NOT NULL,
  `cuotas` int(11) NOT NULL,
  `interes` float DEFAULT NULL,
  `estado` varchar(20) NOT NULL,
  `saldo` float DEFAULT NULL,
  `entrega` varchar(40) DEFAULT NULL,
  `clientes_idclientes` int(11) NOT NULL,
  `vehiculos_idvehiculos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`idventas`, `fecha`, `monto`, `cuotas`, `interes`, `estado`, `saldo`, `entrega`, `clientes_idclientes`, `vehiculos_idvehiculos`) VALUES
(18, '2019-06-21', 500000, 2, 2, 'pendiente', 100000, '', 2, 2),
(20, '2019-06-21', 300000, 2, 2, 'pendiente', 10000, '', 2, 2),
(21, '2019-06-21', 300000, 2, 2, 'pendiente', 10000, '', 2, 2),
(23, '2019-06-30', 20000, 3, 20, 'pendiente', 10000, '', 2, 2),
(24, '2019-06-30', 2000, 2, 25, 'pendiente', 1000, '', 2, 1),
(25, '2019-07-31', 500000, 2, 25, 'pendiente', 300000, '', 2, 2),
(26, '2019-08-03', 500000, 12, 25, 'pendiente', 100000, '', 2, 1),
(27, '2019-08-03', 500000, 12, 20, 'pendiente', 100000, '', 2, 3),
(28, '2019-08-03', 500000, 12, 20, 'pendiente', 100000, '', 2, 3),
(29, '2019-08-03', 500000, 12, 20, 'pendiente', 100000, '', 2, 1),
(30, '2019-08-03', 500000, 6, 20, 'pendiente', 50000, '', 2, 1),
(31, '2019-08-03', 500000, 6, 25, 'pendiente', 50000, '', 2, 2),
(32, '2019-08-03', 500000, 6, 35, 'pendiente', 50000, '', 2, 2),
(33, '2019-08-03', 500000, 1, 0, 'pagado', 0, '', 2, 1),
(34, '2019-08-03', 500000, 1, 1, 'pagado', 0, '', 2, 1),
(35, '2019-08-03', 500000, 1, 1, 'pagado', 455000, '', 2, 1),
(36, '2019-08-03', 500000, 2, 25, 'pendiente', 50000, '', 2, 1),
(37, '2019-08-03', 500000, 1, 25, 'pagado', 0, '', 2, 1),
(38, '2019-08-03', 500000, 1, 0, 'pagado', 0, '', 2, 1),
(39, '2019-08-03', 500000, 1, 0, 'pagado', 0, '', 2, 1),
(40, '2019-08-14', 500000, 1, 0, 'pagado', 0, '', 2, 2),
(41, '2019-08-14', 500000, 1, 0, 'pagado', 0, '', 2, 2),
(42, '2019-08-15', 500000, 1, 0, 'pagado', 0, '', 2, 2),
(43, '2019-08-15', 500000, 1, 0, 'pagado', 0, '', 2, 2),
(44, '2019-08-15', 500000, 1, 0, 'pagado', 0, '', 2, 2),
(45, '2019-08-15', 500000, 1, 1, 'pagado', 100000, '', 2, 2),
(46, '2019-08-15', 500000, 2, 25, 'pendiente', 490000, '', 2, 3),
(47, '2019-08-15', 500000, 1, 0, 'pagado', 0, '', 2, 2),
(48, '2019-08-15', 500000, 1, 0, 'pagado', 0, '', 2, 1),
(49, '2019-08-15', 500000, 1, 0, 'pagado', 0, '', 2, 1),
(50, '2019-08-15', 500000, 2, 25, 'pendiente', 490000, '', 2, 2),
(51, '2019-08-15', 500000, 1, 25, 'pendiente', 450000, NULL, 2, 1),
(52, '2019-08-15', 500000, 1, 0, 'pendiente', 0, NULL, 2, 2),
(53, '2019-08-15', 500000, 1, 0, 'pendiente', 0, NULL, 2, 2),
(54, '2019-08-15', 500000, 1, 0, 'pendiente', 0, NULL, 2, 2),
(55, '2019-08-15', 500000, 1, 0, 'pendiente', 0, NULL, 2, 2),
(56, '2019-08-15', 500000, 1, 0, 'pendiente', 0, NULL, 2, 2),
(57, '2019-08-15', 500000, 2, 2, 'pendiente', 450000, NULL, 2, 3),
(58, '2019-08-15', 500000, 1, 2, 'pendiente', 450000, NULL, 2, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`idclientes`);

--
-- Indices de la tabla `cobranzas`
--
ALTER TABLE `cobranzas`
  ADD PRIMARY KEY (`idcobranzas`),
  ADD KEY `ventas_idventas` (`ventas_idventas`);

--
-- Indices de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD PRIMARY KEY (`idcuotas`),
  ADD KEY `fkventas` (`ventas_idventas`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`idgastos`),
  ADD KEY `fk_usuarios` (`usuarios_idusuarios`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuarios`),
  ADD UNIQUE KEY `usuario_UNIQUE` (`usuario`);

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`idvehiculos`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`idventas`),
  ADD KEY `fk_clientes` (`clientes_idclientes`),
  ADD KEY `fk_vehiculos` (`vehiculos_idvehiculos`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `idclientes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cobranzas`
--
ALTER TABLE `cobranzas`
  MODIFY `idcobranzas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  MODIFY `idcuotas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=896;

--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `idgastos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `idvehiculos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `idventas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cobranzas`
--
ALTER TABLE `cobranzas`
  ADD CONSTRAINT `fk_ventas_idventas` FOREIGN KEY (`ventas_idventas`) REFERENCES `ventas` (`idventas`);

--
-- Filtros para la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD CONSTRAINT `fkventas` FOREIGN KEY (`ventas_idventas`) REFERENCES `ventas` (`idventas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD CONSTRAINT `fk_usuarios` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_clientes` FOREIGN KEY (`clientes_idclientes`) REFERENCES `clientes` (`idclientes`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vehiculos` FOREIGN KEY (`vehiculos_idvehiculos`) REFERENCES `vehiculos` (`idvehiculos`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
