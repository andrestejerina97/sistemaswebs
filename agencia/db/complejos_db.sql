-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-08-2019 a las 22:39:24
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `complejos_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `idclientes` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`idclientes`, `dni`, `apellido`, `nombre`, `telefono`, `email`) VALUES
(1, 0, '', '', '0', ''),
(2, 39989093, 'tejerina', 'josÃ© andrÃ©s', '1234567', 'andd'),
(3, 1234, 'romero', 'undefined', '232', 'undefined'),
(4, 1234, 'Corzo', 'Pablo Esteban', '3884419080', 'asda@'),
(5, 786876, 'prueba', 'hkjhk', '87687', 'undefined'),
(6, 78687, 'hjhgjh', 'hjgjhg', 'hjgjhg', 'undefined'),
(7, 2342, 'werw', 'werw', '12313', 'undefined');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobranzas`
--

CREATE TABLE `cobranzas` (
  `idcobranzas` int(11) NOT NULL,
  `monto_cobro` varchar(40) NOT NULL,
  `fecha` date NOT NULL,
  `tipo_de_pago` varchar(45) NOT NULL,
  `cuota` varchar(20) NOT NULL,
  `interes` varchar(40) NOT NULL,
  `monto_final` varchar(40) NOT NULL,
  `ventas_idventas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cobranzas`
--

INSERT INTO `cobranzas` (`idcobranzas`, `monto_cobro`, `fecha`, `tipo_de_pago`, `cuota`, `interes`, `monto_final`, `ventas_idventas`) VALUES
(1, '0', '2019-08-03', 'contado', '1', '', '', 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `complejos`
--

CREATE TABLE `complejos` (
  `idcomplejos` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `complejos`
--

INSERT INTO `complejos` (`idcomplejos`, `nombre`) VALUES
(1, 'Ana 2'),
(2, 'perales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuotas`
--

CREATE TABLE `cuotas` (
  `idcuotas` int(11) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `numero_de_cuota` int(11) NOT NULL,
  `precio` float NOT NULL,
  `vencimiento` date NOT NULL,
  `ventas_idventas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuotas`
--

INSERT INTO `cuotas` (`idcuotas`, `estado`, `numero_de_cuota`, `precio`, `vencimiento`, `ventas_idventas`) VALUES
(9, 'pagado', 1, 0, '2019-07-03', 18),
(10, 'En mora', 2, 0, '2019-08-03', 18),
(11, 'En mora', 3, 0, '2019-09-03', 18),
(12, 'En mora', 4, 0, '2019-10-03', 18),
(13, 'En mora', 5, 0, '2019-11-03', 18),
(14, 'En mora', 6, 0, '2019-12-03', 18),
(15, 'En mora', 1, 667, '2019-06-19', 19),
(16, '', 2, 667, '2019-07-19', 19),
(17, '', 3, 667, '2019-08-19', 19),
(18, '', 1, 7000, '2019-06-19', 19),
(19, '', 2, 7000, '2019-07-19', 19),
(20, '', 3, 7000, '2019-08-19', 19),
(21, '', 4, 7000, '2019-09-19', 19),
(22, '', 5, 7000, '2019-10-19', 19),
(23, '', 6, 7000, '2019-11-19', 19),
(24, '', 1, 3000, '2019-07-08', 20),
(25, '', 2, 3000, '2019-08-08', 20),
(26, '', 3, 3000, '2019-09-08', 20),
(27, '', 4, 3000, '2019-10-08', 20),
(28, '', 5, 3000, '2019-11-08', 20),
(29, '', 6, 3000, '2019-12-08', 20),
(30, '', 1, 1000, '2019-08-01', 21),
(31, '', 2, 1000, '2019-09-01', 21),
(32, '', 1, 5000, '2019-08-01', 21),
(33, '', 2, 5000, '2019-09-01', 21),
(34, '', 3, 5000, '2019-10-01', 21),
(35, '', 4, 5000, '2019-11-01', 21),
(36, '', 5, 5000, '2019-12-01', 21),
(37, '', 6, 5000, '2020-01-01', 21),
(38, '', 1, 7000, '2019-08-02', 22),
(39, '', 2, 7000, '2019-09-02', 22),
(40, '', 3, 7000, '2019-10-02', 22),
(41, '', 4, 7000, '2019-11-02', 22),
(42, '', 5, 7000, '2019-12-02', 22),
(43, '', 6, 7000, '2020-01-02', 22),
(44, '', 1, 5000, '2019-08-02', 23),
(45, '', 2, 5000, '2019-09-02', 23),
(46, '', 3, 5000, '2019-10-02', 23),
(47, '', 4, 5000, '2019-11-02', 23),
(48, '', 5, 5000, '2019-12-02', 23),
(49, '', 6, 5000, '2020-01-02', 23),
(50, '', 1, 0, '2019-08-08', 24),
(51, '', 2, 0, '2019-09-08', 24),
(52, '', 1, 3000, '2019-08-08', 24),
(53, '', 2, 3000, '2019-09-08', 24),
(54, '', 3, 3000, '2019-10-08', 24),
(55, '', 4, 3000, '2019-11-08', 24),
(56, '', 5, 3000, '2019-12-08', 24),
(57, '', 6, 3000, '2020-01-08', 24),
(58, '', 1, 5222, '2019-08-28', 25),
(59, '', 2, 5222, '2019-09-28', 25),
(60, '', 3, 5222, '2019-10-28', 25),
(61, '', 4, 5222, '2019-11-28', 25),
(62, '', 5, 5222, '2019-12-28', 25),
(63, '', 6, 5222, '2020-01-28', 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `iddepartamentos` int(11) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `observacion` varchar(45) DEFAULT NULL,
  `complejos_idcomplejos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`iddepartamentos`, `numero`, `observacion`, `complejos_idcomplejos`) VALUES
(1, 1, 'mono', 1),
(2, 2, 'mono', 1),
(3, 3, 'mono', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `idgastos` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `fecha` date NOT NULL,
  `monto` float NOT NULL,
  `estado` varchar(20) NOT NULL,
  `usuarios_idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `idventas` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `monto` float NOT NULL,
  `saldo` float NOT NULL,
  `cuotas` int(11) NOT NULL,
  `interes` float NOT NULL,
  `contrato` varchar(20) DEFAULT NULL,
  `costo_inicial` float NOT NULL,
  `clientes_idclientes` int(11) NOT NULL,
  `departamentos_iddepartamentos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`idventas`, `fecha`, `monto`, `saldo`, `cuotas`, `interes`, `contrato`, `costo_inicial`, `clientes_idclientes`, `departamentos_iddepartamentos`) VALUES
(13, '2019-06-30', 9000, 3000, 3, 0, '6', 18000, 2, 2),
(14, '2019-06-30', 5000, 0, 1, 0, '6', 5000, 2, 1),
(15, '2019-06-30', 5000, 0, 1, 0, '6', 5000, 2, 1),
(16, '2019-06-30', 5000, 0, 1, 0, '6', 10000, 2, 1),
(17, '2019-06-30', 5000, 0, 1, 0, '6', 10000, 2, 1),
(18, '2019-06-30', 5000, 0, 1, 0, '6', 10000, 2, 1),
(19, '2019-06-30', 7000, 2000, 3, 0, '6', 10000, 2, 2),
(20, '2019-07-04', 3000, 0, 1, 0, '6', 3000, 2, 2),
(21, '2019-08-03', 5000, 2000, 2, 0, '6', 7000, 2, 3),
(22, '2019-08-03', 7000, 0, 1, 0, '6', 5000, 2, 2),
(23, '2019-08-03', 5000, 0, 1, 0, '6', 5000, 2, 1),
(24, '2019-08-03', 3000, 0, 2, 20, '6', 2000, 2, 2),
(25, '2019-08-03', 5222, 0, 1, 0, '6', 2000, 2, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`idclientes`);

--
-- Indices de la tabla `cobranzas`
--
ALTER TABLE `cobranzas`
  ADD PRIMARY KEY (`idcobranzas`);

--
-- Indices de la tabla `complejos`
--
ALTER TABLE `complejos`
  ADD PRIMARY KEY (`idcomplejos`);

--
-- Indices de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD PRIMARY KEY (`idcuotas`),
  ADD KEY `fk_ventas` (`ventas_idventas`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`iddepartamentos`),
  ADD KEY `fk_complejos_departamentos` (`complejos_idcomplejos`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`idgastos`),
  ADD KEY `fk_usuarios` (`usuarios_idusuarios`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuarios`),
  ADD UNIQUE KEY `usuario_UNIQUE` (`usuario`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`idventas`),
  ADD KEY `fk_departamentos` (`departamentos_iddepartamentos`),
  ADD KEY `fk_clientes` (`clientes_idclientes`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `idclientes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `cobranzas`
--
ALTER TABLE `cobranzas`
  MODIFY `idcobranzas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `complejos`
--
ALTER TABLE `complejos`
  MODIFY `idcomplejos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  MODIFY `idcuotas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `iddepartamentos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `idgastos` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `idventas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD CONSTRAINT `fk_ventas` FOREIGN KEY (`ventas_idventas`) REFERENCES `ventas` (`idventas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD CONSTRAINT `fk_complejos_departamentos` FOREIGN KEY (`complejos_idcomplejos`) REFERENCES `complejos` (`idcomplejos`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD CONSTRAINT `fk_usuarios` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_clientes` FOREIGN KEY (`clientes_idclientes`) REFERENCES `clientes` (`idclientes`),
  ADD CONSTRAINT `fk_departamentos` FOREIGN KEY (`departamentos_iddepartamentos`) REFERENCES `departamentos` (`iddepartamentos`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
