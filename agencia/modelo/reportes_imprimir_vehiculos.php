<?php
require_once '../vendor/autoload.php';
// Create an instance of the class:

require('conexion.php');
require('funciones.php');

$fi=$_GET['fechai'];
$ff=$_GET['fechaf'];
$tipo=$_GET['tipo'];


//NOTA: test_input es una funcion creada para validar el campo,no permite nulos ni permite mayusculas ni espacios




function obtenerFechaEnLetra($fecha)
{
   
    $num = date("j", strtotime($fecha));
    $anno = date("Y", strtotime($fecha));
    $mes = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    $mes = $mes[(date('m', strtotime($fecha))*1)-1];
    return $num.' de '.$mes.' del '.$anno;
}	


	
    $html = " 
    <a>". obtenerFechaEnLetra(date("y-m-d")) ."</a>
     <fieldset>";
         
         
	 
         


         switch ($tipo) {
            case 'vehiculo':

            $html .= '     
            <h1>INFORME DE VENTAS. </h1>   <div >
         
            <table border=1   style="width:100%; 
            border:solid;
              " >
              <thead >
                <tr>
                  <th>FECHAS</th>
                  <th>VEHICULO</th>
                  <th>MONTO</th>
                  <th>SALDO</th>
                  <th>CUOTAS</th>
                   </tr>
              </thead>';
             $html.= '<tbody id="cuerpo_reporte_vehiculos">';


            $db=new ConexionMySQL("agencia_db");
            $buscar= "SELECT ventas.fecha,concat(vehiculos.marca,' ',vehiculos.modelo) as vehiculo,ventas.monto,ventas.saldo,ventas.cuotas FROM ventas join vehiculos on vehiculos.idvehiculos= ventas.vehiculos_idvehiculos where (fecha >= '$fi' and fecha <= '$ff')";
            $resultado=$db->consulta($buscar);
         $total=0.00;
         $pagado=0.00;
         $pendiente=0.00;
              while($filas=$db->buscar_array($resultado)) {
                $html .= "<tr>";
        
                $html .= "<td class='center'>" . $filas['fecha'] . "</td>";
                $html .= "<td class='center'>" . $filas['vehiculo'] . "</td>";
                $html .= "<td class='center'>" . $filas['monto'] . "</td>";
                $html .= "<td class='center'>" . $filas['saldo'] . "</td>";
                $html .= "<td class='center'>" . $filas['cuotas'] . "</td>";
                $html .= "</tr>";

                $total+=$filas['monto'] ;
                if($filas['estado']=="pagado"){
                    $pagado+=$filas['monto'];
                }else{
                    if ($filas['estado']=="pendiente") {
                        $pendiente+=$filas['monto'];
 
                    }
                }

                 }
              $html.= '</tbody> </table> <div>';
        
              $html.="<h3>Resumen: </h3>
              <h5>TOTAL NETO: $".number_format($total)." </h5>
              <h5>TOTAL PENDIENTES: $".number_format($pendiente)." </h5>
              <h5>TOTAL PAGADOS: $".number_format($pagado)." </h5>
              ";
                break;
            case 'alquiler':

                $html .= ' <h1>INFORME DE ALQUILERES. </h1> <div >
             
                <table border=1   style="width:100%; 
                border:solid;
                  " >
                  <thead >
                    <tr>
                    <th>FECHA DE CONTRATO</th>
                    <th>DEPARTAMENTO</th>
                    <th>COMPLEJO</th>
                    <th>CLIENTE</th>
                    <th>MONTO</th>
                       </tr>
                  </thead>';
                 $html.= '<tbody id="cuerpo_reporte_vehiculos">';
    
                 $total=0.00;
                 $pagado=0.00;
                 $pendiente=0.00;
                 $db=new ConexionMySQL("complejos_db");
                 $buscar="SELECT ventas.fecha,departamentos.numero,complejos.nombre as complejo,ventas.monto,concat(clientes.nombre,' ',clientes.apellido) as cliente from ventas join departamentos on  departamentos.iddepartamentos=ventas.idventas join complejos on departamentos.complejos_idcomplejos=complejos.idcomplejos JOIN clientes on clientes.idclientes=ventas.idventas  where (ventas.fecha >= '$fi' and fecha <= '$ff')";
                 $resultado=$db->consulta($buscar);
             
                
                 
                 
           
                  while($filas=$db->buscar_array($resultado)) {
                    $html .= "<tr>";
            
                    $html .= "<td class='center'>" . $filas['fecha'] . "</td>";
                    $html .= "<td class='center'>" . $filas['numero'] . "</td>";
                    $html .= "<td class='center'>" . $filas['complejo'] . "</td>";
                    $html .= "<td class='center'>" . $filas['cliente'] . "</td>";
                    $html .= "<td class='center'>" . $filas['monto'] . "</td>";
                    $html .= "</tr>";
    
                    $total+=$filas['monto'] ;
                
                     
                  
    
                     }
                  $html.= '</tbody> </table> <div>';
            
                  $html.="<h3>Resumen: </h3>
                  <h5>TOTAL NETO: $".number_format($total)." </h5>";
               
                    break;
          case 'alquiler':
            # code...
            break;
            case 'ingreso':
              # code...
              break;
            default:
                # code...
                break;
        }
	
	 
   
    
	 
	 
	 	$html .="
	 </fieldset>";


	$mpdf = new \Mpdf\Mpdf(); 
	$mpdf->SetDisplayMode('fullpage');
	$css = file_get_contents("../css/estilo.css");
	$mpdf->WriteHTML($css,1);
    $mpdf->WriteHTML($html);
    $mpdf->setFooter("PÁGINA {PAGENO} de {nb}"); 
	$mpdf->Output();

	exit;
	?>