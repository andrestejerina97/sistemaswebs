<?php
// este es un ejemplo que corresponde al alta de un artículo

//error_reporting(0);
require('conexion.php');
require('funciones.php');



$db=new ConexionMySQL("agencia_db");
//NOTA: test_input es una funcion creada para validar el campo,no permite nulos ni permite mayusculas ni espacios

$id=$_POST['idcliente'];



 $buscar= "SELECT idclientes,nombre,apellido,dni,telefono,email from clientes where idclientes='$id'";

$resultado=$db->consulta($buscar);
if($db->numero_de_registros($resultado)){
    
	 $html=' <div class="modal fade" id="modal_update_cliente" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">

		  <h4 class="modal-title">Información del cliente:</h4>
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

		</div>
		<div class="modal-body">
		  <div class="row justify-content-center">

		  </div>
		  <form id="form_update_cliente">
		  
		  ';


    while($filas=$db->buscar_array($resultado)) {
		$html .='<div class="row">
		<input type="hidden"id="idC" class="form-control"  value="'.$id.'">

		<div class="form-group col-md">
		  <label for="">Apellido:</label>
		  <input type="text"id="apellidoC" class="form-control" placeholder="ingrese apellido" value="'.$filas['apellido'].'">
		</div>

		<div class="form-group col-md">
		  <label class="" for="">Nombres:</label>
		  <input type="text" id="nombresC" class="form-control" placeholder="ingrese nombre" value="'.$filas['nombre'].'">
		</div>
	  </div>
	  <div class="row">
		<div class="form-group col-md">
		  <label for="">DNI:</label>
		  <input type="text" id="dniC" class="form-control" value="'.$filas['dni'].'">
		</div>
		<div class="form-group col-md">
		  <label for="">Domicilio:</label>
		  <input type="text" id="domicilioC" class="form-control" value="'.$filas['domicilio'].'">
		</div>
	  </div>

	  <div class="row">
		<div class="form-group col-md">
		  <label for="">Teléfono:</label>
		  <input type="tel" id="telefonoC" class="form-control" value="'.$filas['telefono'].'">
		</div>
		<div class="form-group col-md">
		  <label for="">Mail:</label>
		  <input type="text" id="mailC" class="form-control" value="'.$filas['email'].'">
		</div>

	  </div>';
	 
    }
	$html.='<div class="row justify-content-center">
	<button type="button"  class="btn btn-success"  onclick="procesa_update_cliente();" data-dismiss="modal">Guardar cambios</button>


  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
</div>
</div>
</div>
</div>';

 echo $html;

}else{
echo "0";
}



?>
