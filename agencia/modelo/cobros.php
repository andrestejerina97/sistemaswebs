<?php
// este es un ejemplo que corresponde al alta de un artículo

//error_reporting(0);
require('conexion.php');
require('funciones.php');

$tipo=$_POST['tipo'];

switch ($tipo) {
    case 'tabla_general':
    $dni=$_POST['dni'];
    $cobro=$_POST['cobro'];


    switch ($cobro) {
        case 'vehiculos':
        $db=new ConexionMySQL("agencia_db");
        //NOTA: test_input es una funcion creada para validar el campo,no permite nulos ni permite mayusculas ni espacios
        
        
         $buscar= "SELECT idventas,fecha,clientes.apellido,clientes.nombre,vehiculos.marca,vehiculos.modelo FROM cuotas 
         join ventas on ventas.idventas=cuotas.ventas_idventas
         JOIN clientes on clientes_idclientes=clientes.idclientes 
         join vehiculos on ventas.vehiculos_idvehiculos=vehiculos.idvehiculos
          where clientes.dni=$dni and cuotas.estado ='En mora'
          group by idventas,fecha,clientes.apellido,clientes.nombre,vehiculos.marca,vehiculos.modelo ";
        
           $html=' <table id="tabla_cobros" >
           <thead class="thead-dark">
           <tr>
           <th>N° DE VENTA</th>
             <th>FECHA DE VENTA</th>
             <th>DATOS DEL CLIENTE</th>
             <th>DATOS DE VENTA</th>
             <th>SELECCIONE VENTA</th>
        
        
           </tr>
         </thead>
         <tbody id="cuerpo_tabla_cobros">
            
           ';
        
        $resultado=$db->consulta($buscar);
        if($db->numero_de_registros($resultado)){
        
            while($filas=$db->buscar_array($resultado)) {
                $html .= "<tr>"; 
                $html .= "<td>".$filas['idventas']."</td>";
                $html .= "<td>".$filas['fecha']."</td>";
                $html .= "<td>".$filas['apellido']." ".$filas['nombre']."</td>";
                $html .= "<td>".$filas['marca']." ".$filas['modelo']."</td>";
                $html .= "<td >"."<button class='btn btn-primary' onclick='tabla_venta(this.id);' id='".$filas['idventas']."'><i class='fas fa-cash-register'></i> </button>"."</td>";
                $html .= "</tr>";
            }
        
            $html .='</tbody>  </table>
            ';
        echo $html;
        }else{
        echo "0";
        }
        
        
        $db->DesconectaServer();
            break;
        case 'alquileres':
        $db=new ConexionMySQL("complejos_db");
        //NOTA: test_input es una funcion creada para validar el campo,no permite nulos ni permite mayusculas ni espacios
        
        
         $buscar= "SELECT idventas,fecha,clientes.apellido,clientes.nombre,departamentos.numero FROM cuotas 
         join ventas on ventas.idventas=cuotas.ventas_idventas
         JOIN clientes on clientes_idclientes=clientes.idclientes 
         join departamentos on departamentos.iddepartamentos=ventas.departamentos_iddepartamentos
          where clientes.dni=$dni and cuotas.estado ='En mora'
           group by idventas,fecha,clientes.apellido,clientes.nombre,departamentos.numero ";
        
           $html=' <table id="tabla_cobros" >
           <thead class="thead-dark">
           <tr>
           <th>N° DE OPERACION</th>
             <th>FECHA DE VENTA</th>
             <th>DATOS DEL CLIENTE</th>
             <th>DATOS DE OPERACION</th>
             <th>SELECCIONE ALQUILER</th>
        
        
           </tr>
         </thead>
         <tbody id="cuerpo_tabla_cobros">
            
           ';
        
        $resultado=$db->consulta($buscar);
        if($db->numero_de_registros($resultado)){
        
            while($filas=$db->buscar_array($resultado)) {
                $html .= "<tr>"; 
                $html .= "<td>".$filas['idventas']."</td>";
                $html .= "<td>".$filas['fecha']."</td>";
                $html .= "<td>".$filas['apellido']." ".$filas['nombre']."</td>";
                $html .= "<td> N° DPTO: ".$filas['numero']."</td>";
                $html .= "<td >"."<button class='btn btn-primary' onclick='tabla_venta(this.id);' id='".$filas['idventas']."'><i class='fas fa-cash-register'></i> </button>"."</td>";
                $html .= "</tr>";
            }
        
            $html .='</tbody>  </table>
            ';
        echo $html;
        }else{
        echo "0";
        }
        
        
        $db->DesconectaServer();
            break;
    }

   
       
        break;
        case 'tabla_venta':

        $idVenta=$_POST['idVenta'];
        $cobro=$_POST['cobro'];
        switch ($cobro) {
            case 'vehiculos':
            $db=new ConexionMySQL("agencia_db");
            //NOTA: test_input es una funcion creada para validar el campo,no permite nulos ni permite mayusculas ni espacios
            
            
             $buscar= "SELECT idcuotas,cuotas.idcuotas,cuotas.numero_de_cuota,cuotas.precio,cuotas.vencimiento FROM `cuotas`
              join ventas ON cuotas.ventas_idventas = ventas.idventas where ventas.idventas=$idVenta and cuotas.estado='En mora'";
            
            $resultado=$db->consulta($buscar);
            if($db->numero_de_registros($resultado)){
            $html='
            <div class=" justify-content-center">
            <button class="btn btn-primary" id="btn_procesar" onclick="pagar_cobro();">PROCESAR COBRO</button>
            <button class="btn btn-danger" id="btn_cancelar" onclick="cancelar_cuotas();">CANCELAR</button>
            <input type="hidden" id="inputIdventas" value='.$idVenta .'>
    
    
    
            <br>
            </div>
            <table id="tabla_cobros_ventas" data-toggle="table" data-pagination="true"
            style="width:80%">
    
            <thead class="thead-dark">
                <tr>
    
                   <th  ></th>
                    <th>N° CUOTA</th>
                    <th>VENCIMIENTO</th>
                    <th>PRECIO</th>
                    <th>% DE INTERÉS(OPCIONAL)</th>
              
    
                </tr>
            </thead>
            <tbody id="cuerpo_tabla_cuotas">  ';
    
    
    
    
                while($filas=$db->buscar_array($resultado)) {
                    $html .= "<tr>"; 
    
                    $html .= "<td><input type='checkbox' class=' form-check-inline' id='".$filas['idcuotas']."' onclick='click_check(this.checked,this.id);' ></td>";
    
                    $html .= "<td>".$filas['numero_de_cuota']."</td>";
                    $html .= "<td>".$filas['vencimiento']."</td>";
    
                    $html .= "<td> $".$filas['precio']."</td>";
    
                    $html .= "<td>"."<input class='form-control' type='tel'  id='i".$filas['idcuotas']."' onchange='click_input(this.id)' value='0' >"."</td>";
    
    
                    $html .= "</tr> ";
    
    
                    
                }
            //    <input class='form-control' type='checkbox' name='' id=''>
    
                $html .='</tbody>
                </table>';
                echo $html;
            
            }else{
            echo "0";
            }
                break;
            case 'alquileres':
            $db=new ConexionMySQL("complejos_db");
            //NOTA: test_input es una funcion creada para validar el campo,no permite nulos ni permite mayusculas ni espacios
            
            
             $buscar= "SELECT idcuotas,cuotas.idcuotas,cuotas.numero_de_cuota,cuotas.precio,cuotas.vencimiento FROM `cuotas`
              join ventas ON cuotas.ventas_idventas = ventas.idventas where ventas.idventas=$idVenta and cuotas.estado='En mora'";
            
            $resultado=$db->consulta($buscar);
            if($db->numero_de_registros($resultado)){
            $html='
            <div class=" justify-content-center">
            <button class="btn btn-primary" id="btn_procesar" onclick="pagar_cobro();">PROCESAR COBRO</button>
            <button class="btn btn-danger" id="btn_cancelar" onclick="cancelar_cuotas();">CANCELAR</button>
            <input type="hidden" id="inputIdventas" value='.$idVenta .'>
    
    
    
            <br>
            </div>
            <table id="tabla_cobros_ventas" data-toggle="table" data-pagination="true"
            style="width:80%">
    
            <thead class="thead-dark">
                <tr>
    
                   <th  >&nbsp</th>
                    <th>N° CUOTA</th>
                    <th>VENCIMIENTO</th>
                    <th>PRECIO</th>
                    <th>% DE INTERÉS(OPCIONAL)</th>
              
    
                </tr>
            </thead>
            <tbody id="cuerpo_tabla_cuotas">  ';
    
    
    
    
                while($filas=$db->buscar_array($resultado)) {
                    $html .= "<tr>"; 
    
                    $html .= "<td><input type='checkbox' class='col-md-8' id='".$filas['idcuotas']."' onclick='click_check(this.checked,this.id);' ></td>";
    
                    $html .= "<td>".$filas['numero_de_cuota']."</td>";
                    $html .= "<td>".$filas['vencimiento']."</td>";
    
                    $html .= "<td> $".$filas['precio']."</td>";
    
                    $html .= "<td>"."<input class='form-control' type='tel'  id='i".$filas['idcuotas']."' onchange='click_input(this.id)' value='0' >"."</td>";
    
    
                    $html .= "</tr> ";
    
    
                    
                }
            //    <input class='form-control' type='checkbox' name='' id=''>
    
                $html .='</tbody>
                </table>';
                echo $html;
            
            }else{
            echo "0";
            }
                break;
            default:
                # code...
                break;
        }
        
       


            
            break;
            case 'cuotas_mora':
       
            $dni=$_POST['dni'];
            $fecha= date("Y-m-d");

    $db=new ConexionMySQL("agencia_db");
//NOTA: test_input es una funcion creada para validar el campo,no permite nulos ni permite mayusculas ni espacios


 $buscar= "SELECT idventas,ventas.fecha,cuotas.numero_de_cuota,ventas.estado,cuotas.vencimiento,clientes.nombre,clientes.apellido FROM `ventas`
  JOIN clientes on clientes_idclientes=clientes.idclientes
   join cuotas on cuotas.ventas_idventas=ventas.idventas where clientes.dni = $dni and cuotas.vencimiento < '$fecha'";
  //  ";

   $html=' <table id="tabla_cuota_mora" data-toggle="table" data-pagination="true" style="width:80%">

   <thead class="thead-dark">
     <tr>
       <th>N° VENTA</th>
       <th>FECHA DE VENTA</th>
       <th>APELLIDO</th>
       <th>NOMBRE</th>
       <th>ESTADO</th>
       <th>N° CUOTA</th>
       <th>VENCIMIENTO</th>

     </tr>
   </thead>
   <tbody>
    
    
   ';

$resultado=$db->consulta($buscar);
if($db->numero_de_registros($resultado)){

    while($filas=$db->buscar_array($resultado)) {
        $html .= "<tr>"; 
        $html .= "<td>".$filas['idventas']."</td>";
        $html .= "<td>".$filas['fecha']."</td>";
        $html .= "<td>".$filas['apellido']."</td>";
        $html .= "<td>".$filas['nombre']."</td>";
        $html .= "<td>".$filas['estado']."</td>";
        $html .= "<td>".$filas['numero_de_cuota']."</td>";
        $html .= "<td>".$filas['vencimiento']."</td>";
        $html .= "</tr>";
    }

    $html .='</tbody>  </table>
    ';

echo $html;
$db->DesconectaServer();


}else{

echo "0";
$db->DesconectaServer();

}


       



                break;
    
    default:
        # code...
        break;
}





?>

